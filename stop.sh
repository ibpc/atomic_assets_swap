# bin/bash
# 关闭链
set -euo
cd scripts
pwd

echo "======== close chainmaker ========"
./chain_stop.sh

echo "======== close fabric ========"
./fabric_run.sh clear
