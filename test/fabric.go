// coding:utf-8
// fabric测试
package main

import (
	"atomic_assets_swap/chainclient/fabric/contractinvoke"
	fabConfig "atomic_assets_swap/config/fabric"
	"atomic_assets_swap/utils"
	"encoding/json"
	"fmt"
	"os"
	"time"
)

// ./fabric_restart.sh	启动fabric
// go run deploy/main.go	部署链码
// go run fabric.go	执行函数

// 确保通道存在
func testChannel(client *contractinvoke.FabricClient) error {
	// 查询通道，确定有指定的通道
	flag := false
	ls_channel, err := client.QueryChannel()
	if err != nil {
		return fmt.Errorf("query channel error: " + err.Error())
	}
	for _, id := range ls_channel {
		if id == fabConfig.GetChannelInfo().ChannelID {
			flag = true
			break
		}
	}

	// 如果通道不存在就创建并加入通道
	if !flag {
		utils.InfoTips("通道不存在, 创建并加入通道")
		// 创建并加入通道
		if err := client.CreateAndJoinChannel(fabConfig.GetChannelInfo()); err != nil {
			return fmt.Errorf("create channel error:" + err.Error())
		}
	}
	// 通道存在
	fmt.Println("通道存在")
	return nil
}

func newClient(config_path, orgname, username string) *contractinvoke.FabricClient {
	client, err := contractinvoke.NewClient(config_path, orgname, username)
	if err != nil {
		utils.InfoError("newclient", err)
		return nil
	}
	// 查询通道
	if err := testChannel(client); err != nil {
		utils.InfoError("fabric channel", err)
		os.Exit(1)
	}

	if err := client.InitClient(); err != nil {
		utils.InfoError("initclient", err)
		return nil
	}
	return client
}

func assetsFunc(client *contractinvoke.FabricClient, client2 *contractinvoke.FabricClient) error {

	utils.InfoTips("查询用户1账户地址")
	resp, err := client.InvokeFunction("assets", "getAddress", []string{}, EndorserPeer...)
	if err != nil {
		utils.InfoError("getAddress", err)
		return err
	}
	curr_address := string(resp.Payload)
	utils.Info(curr_address)

	utils.InfoTips("查询用户2账户地址")
	resp, err = client2.InvokeFunction("assets", "getAddress", []string{}, EndorserPeer...)
	if err != nil {
		utils.InfoError("getAddress", err)
		return err
	}
	curr_address2 := string(resp.Payload)
	utils.Info(curr_address2)

	utils.InfoTips("查询账户资产")
	resp, err = client.InvokeFunction("assets", "balanceOf", []string{curr_address}, EndorserPeer...)
	if err != nil {
		utils.InfoError("balanceOf", err)
		return err
	}
	utils.Info(string(resp.Payload))

	utils.InfoTips("查询fabric代币信息CrossTokenInfo那个接口")
	resp, err = client.InvokeFunction("assets", "CrossTokenInfo", []string{}, EndorserPeer...)
	if err != nil {
		utils.InfoError("CrossTokenInfo", err)
		return err
	}
	utils.Info(string(resp.Payload))

	utils.InfoTips("用户1账户初始资产")
	resp, err = client.InvokeFunction("assets", "balanceOf", []string{curr_address}, EndorserPeer...)
	if err != nil {
		utils.InfoError("balanceOf", err)
		return err
	}
	utils.Info(string(resp.Payload))

	utils.InfoTips("用户2账户初始资产")
	resp, err = client2.InvokeFunction("assets", "balanceOf", []string{curr_address}, EndorserPeer...)
	if err != nil {
		utils.InfoError("balanceOf", err)
		return err
	}
	utils.Info(string(resp.Payload))

	utils.InfoTips("授权接口approve，用户1授权给用户2可以动用多少资产")
	resp, err = client.InvokeFunction("assets", "approve", []string{curr_address2, "100"}, EndorserPeer...)
	if err != nil {
		utils.InfoError("approve", err)
		return err
	}
	utils.Info(string(resp.Payload))

	utils.InfoTips("transferFrom接口，用户1给用户2转账多少资产")
	resp, err = client2.InvokeFunction(
		"assets",
		"transferFrom",
		[]string{curr_address, curr_address2, curr_address2, "100"},
		EndorserPeer...,
	)
	if err != nil {
		utils.InfoError("transferFrom", err)
		return err
	}
	utils.Info(string(resp.Payload))

	utils.InfoTips("用户1账户转账后资产")
	resp, err = client.InvokeFunction("assets", "balanceOf", []string{curr_address}, EndorserPeer...)
	if err != nil {
		utils.InfoError("balanceOf", err)
		return err
	}
	utils.Info(string(resp.Payload))

	utils.InfoTips("用户2账户转账后资产")
	resp, err = client2.InvokeFunction("assets", "balanceOf", []string{curr_address2}, EndorserPeer...)
	if err != nil {
		utils.InfoError("balanceOf", err)
		return err
	}
	utils.Info(string(resp.Payload))

	utils.InfoTips("============== assets 测试通过 ===============")
	return nil
}

// htlc功能测试
func htlcFunc(
	client,
	peerClient *contractinvoke.FabricClient,
) error {
	// 先查一下当前地址
	resp, err := client.InvokeFunction("assets", "getAddress", []string{}, EndorserPeer...)
	if err != nil {
		utils.InfoError("getAddress", err)
		return err
	}
	curr_address := string(resp.Payload)
	utils.Info("当前地址", curr_address)
	// 对方地址
	resp, err = peerClient.InvokeFunction("assets", "getAddress", []string{}, EndorserPeer...)
	if err != nil {
		utils.InfoError("getAddress", err)
		return err
	}
	peer_address := string(resp.Payload)
	utils.Info("对方地址", peer_address)

	utils.InfoTips("发起htlc Proposal")
	preimage := "hashlock"
	hashlock := utils.GetSha256Hash([]byte(preimage))
	resp, err = client.InvokeFunction(
		"htlc",
		"newProposal",
		[]string{peer_address, "100", hashlock, "600"},
		EndorserPeer...,
	)
	if err != nil {
		utils.InfoError("newProposal", err)
		return err
	}
	proposalID := string(resp.Payload)
	utils.Info(proposalID)
	time.Sleep(1 * time.Second)

	utils.InfoTips("查询刚才发起的htlc Proposal")
	resp, err = client.InvokeFunction("htlc", "getProposal", []string{proposalID}, EndorserPeer...)
	if err != nil {
		utils.InfoError("getProposal", err)
		return err
	}
	utils.Info(string(resp.Payload))
	time.Sleep(1 * time.Second)

	utils.InfoTips("发起用户对自己发起的htlc proposal执行withdraw，应报用户错误")
	resp, err = client.InvokeFunction("htlc", "withdraw", []string{proposalID, preimage}, EndorserPeer...)
	if err == nil {
		info := utils.InfoError("withdraw", fmt.Errorf("应该报withdraw用户错误"))
		return fmt.Errorf(info)
	}
	utils.Info(err)
	time.Sleep(1 * time.Second)

	utils.InfoTips("对手用户用错误原像对发给自己的htlc proposal发起withdraw，应报原像错误")
	resp, err = peerClient.InvokeFunction("htlc", "withdraw", []string{proposalID, "wrongpreimage"}, EndorserPeer...)
	if err == nil {
		info := utils.InfoError("withdraw", fmt.Errorf("应该报withdraw原像错误"))
		return fmt.Errorf(info)
	}
	utils.Info(err)
	time.Sleep(1 * time.Second)

	utils.InfoTips("从这笔htlc Proposal中取款")
	resp, err = peerClient.InvokeFunction("htlc", "withdraw", []string{proposalID, preimage}, EndorserPeer...)
	if err != nil {
		utils.InfoError("withdraw", err)
		return err
	}
	utils.Info(string(resp.Payload))
	time.Sleep(1 * time.Second)

	utils.InfoTips("对手用户对已经取过的htlc proposal再发起withdraw，应报proposal状态错误")
	resp, err = peerClient.InvokeFunction("htlc", "withdraw", []string{proposalID, preimage}, EndorserPeer...)
	if err == nil {
		info := utils.InfoError("withdraw", fmt.Errorf("应该报错proposal状态错误"))
		return fmt.Errorf(info)
	}
	utils.Info(err)

	utils.InfoTips("从一笔htlc Proposal中退款: 首先发起一笔proposal")
	resp, err = client.InvokeFunction("htlc", "newProposal", []string{peer_address, "100", hashlock, "3"}, EndorserPeer...)
	if err != nil {
		utils.InfoError("newProposal", err)
		return err
	}
	proposalID1 := string(resp.Payload)
	utils.Info(proposalID1)

	utils.InfoTips("自己从自己发起的htlc proposal中withdraw，应报用户错误")
	resp, err = client.InvokeFunction("htlc", "withdraw", []string{proposalID, preimage}, EndorserPeer...)
	if err == nil {
		info := utils.InfoError("withdraw", fmt.Errorf("应该报用户错误"))
		return fmt.Errorf(info)
	}
	utils.Info(err)

	if err := htlcFunc1(client, peerClient, proposalID1); err != nil {
		panic(err)
	}

	utils.InfoTips("============== htlc 测试通过 ===============")
	return nil
}

// htlc功能测试的另一部分
func htlcFunc1(client, peerClient *contractinvoke.FabricClient, proposalID1 string) error {
	utils.InfoTips("发起用户尝试对自己发起的时间锁未到期的htlc proposal发起refund，应报proposal状态错误")
	resp, err := client.InvokeFunction("htlc", "refund", []string{proposalID1}, EndorserPeer...)
	if err == nil {
		info := utils.InfoError("refund", fmt.Errorf("应报proposal状态错误"))
		return fmt.Errorf(info)
	}
	utils.Info(err)

	utils.InfoTips("休眠5秒确保超时")
	time.Sleep(5 * time.Second)
	utils.InfoTips("休眠完成进行退款")
	resp, err = client.InvokeFunction("htlc", "refund", []string{proposalID1}, EndorserPeer...)
	if err != nil {
		utils.InfoError("refund", err)
		return err
	}
	utils.Info(string(resp.Payload))

	utils.InfoTips("发起用户对已经refund的htlc proposal再发起refund，应报proposal状态错误")
	resp, err = client.InvokeFunction("htlc", "refund", []string{proposalID1}, EndorserPeer...)
	if err == nil {
		info := utils.InfoError("refund", fmt.Errorf("应报proposal状态错误"))
		return fmt.Errorf(info)
	}
	utils.Info(err)

	utils.InfoTips("对手用户对发起用户发起的htlc proposal发起refund，应报用户错误")
	resp, err = peerClient.InvokeFunction("htlc", "refund", []string{proposalID1}, EndorserPeer...)
	if err == nil {
		info := utils.InfoError("refund", fmt.Errorf("应报用户错误"))
		return fmt.Errorf(info)
	}
	utils.Info(err)
	return nil
}

// 测试lilac功能
func lilacFunc(
	client,
	peerClient *contractinvoke.FabricClient,
) error {
	// 先查一下当前地址
	resp, err := client.InvokeFunction("assets", "getAddress", []string{}, EndorserPeer...)
	if err != nil {
		utils.InfoError("getAddress", err)
		return err
	}
	curr_address := string(resp.Payload)
	utils.Info("当前地址", curr_address)
	// 对方地址
	resp, err = peerClient.InvokeFunction("assets", "getAddress", []string{}, EndorserPeer...)
	if err != nil {
		utils.InfoError("getAddress", err)
		return err
	}
	peer_address := string(resp.Payload)
	utils.Info("对方地址", peer_address)

	utils.InfoTips("发起lilac Proposal")
	preimage_ori := []string{"hashlock", "hashlock1"}
	preimage_byte, err := json.Marshal(preimage_ori)
	if err != nil {
		info := utils.InfoError("json.marshal0", err)
		return fmt.Errorf(info)
	}
	preimage := string(preimage_byte)

	hashlock_ori := []string{}
	for _, p := range preimage_ori {
		hashlock_ori = append(hashlock_ori, utils.GetSha256Hash([]byte(p)))
	}
	hashlock_byte, err := json.Marshal(hashlock_ori)
	if err != nil {
		info := utils.InfoError("json.marshal", err)
		return fmt.Errorf(info)
	}
	hashlock := string(hashlock_byte)

	resp, err = client.InvokeFunction(
		"lilac",
		"newProposal",
		[]string{peer_address, "100", hashlock, "600"},
		EndorserPeer...,
	)
	if err != nil {
		utils.InfoError("newProposal", err)
		return err
	}
	proposalID := string(resp.Payload)
	utils.Info(proposalID)
	time.Sleep(1 * time.Second)

	if err2 := lilacFunc2(client, peerClient, proposalID, preimage); err2 != nil {
		panic(err2)
	}

	utils.InfoTips("从一笔lilac Proposal中退款: 首先发起一笔proposal")
	resp, err = client.InvokeFunction(
		"lilac",
		"newProposal",
		[]string{peer_address, "100", hashlock, "3"},
		EndorserPeer...,
	)
	if err != nil {
		utils.InfoError("newProposal", err)
		return err
	}
	proposalID1 := string(resp.Payload)
	utils.Info(proposalID1)

	// 调用函数
	if err1 := lilacFunc1(client, peerClient, proposalID1); err != nil {
		panic(err1)
	}

	utils.InfoTips("自己从自己发起的lilac proposal中withdraw，应报用户错误")
	resp, err = client.InvokeFunction("lilac", "withdraw", []string{proposalID, preimage}, EndorserPeer...)
	if err == nil {
		info := utils.InfoError("withdraw", fmt.Errorf("应该报用户错误"))
		return fmt.Errorf(info)
	}
	utils.Info(err)

	utils.InfoTips("============== lilac 测试通过 ===============")
	return nil
}

// lilac功能测试拆分出来的第二个函数
func lilacFunc2(client, peerClient *contractinvoke.FabricClient, proposalID, preimage string) error {
	utils.InfoTips("查询刚才发起的lilac Proposal")
	resp, err := client.InvokeFunction("lilac", "getProposal", []string{proposalID}, EndorserPeer...)
	if err != nil {
		utils.InfoError("getProposal", err)
		return err
	}
	utils.Info(string(resp.Payload))
	time.Sleep(1 * time.Second)

	utils.InfoTips("发起用户对自己发起的lilac proposal执行withdraw，应报用户错误")
	resp, err = client.InvokeFunction("lilac", "withdraw", []string{proposalID, preimage}, EndorserPeer...)
	if err == nil {
		info := utils.InfoError("withdraw", fmt.Errorf("应该报withdraw用户错误"))
		return fmt.Errorf(info)
	}
	utils.Info(err)
	time.Sleep(1 * time.Second)

	utils.InfoTips("对手用户用错误原像对发给自己的lilac proposal发起withdraw，应报原像错误")
	resp, err = peerClient.InvokeFunction("lilac", "withdraw", []string{proposalID, "wrongpreimage"}, EndorserPeer...)
	if err == nil {
		info := utils.InfoError("withdraw", fmt.Errorf("应该报withdraw原像错误"))
		return fmt.Errorf(info)
	}
	utils.Info(err)
	time.Sleep(1 * time.Second)

	utils.InfoTips("从这笔lilac Proposal中取款")
	resp, err = peerClient.InvokeFunction("lilac", "withdraw", []string{proposalID, preimage}, EndorserPeer...)
	if err != nil {
		utils.InfoError("withdraw", err)
		return err
	}
	utils.Info(string(resp.Payload))
	time.Sleep(1 * time.Second)

	utils.InfoTips("对手用户对已经取过的lilac proposal再发起withdraw，应报proposal状态错误")
	resp, err = peerClient.InvokeFunction("lilac", "withdraw", []string{proposalID, preimage}, EndorserPeer...)
	if err == nil {
		info := utils.InfoError("withdraw", fmt.Errorf("应该报错proposal状态错误"))
		return fmt.Errorf(info)
	}
	utils.Info(err)
	return nil
}

// lilac功能测试拆分出来的另一个函数
func lilacFunc1(client, peerClient *contractinvoke.FabricClient, proposalID1 string) error {
	utils.InfoTips("发起用户尝试对自己发起的时间锁未到期的lilac proposal发起refund，应报时间锁timelock状态错误")
	resp, err := client.InvokeFunction("lilac", "refund", []string{proposalID1}, EndorserPeer...)
	if err == nil {
		info := utils.InfoError("refund", fmt.Errorf("应报proposal状态错误"))
		return fmt.Errorf(info)
	}
	utils.Info(err)

	utils.InfoTips("休眠5秒确保超时")
	time.Sleep(5 * time.Second)
	utils.InfoTips("休眠完成进行退款")
	resp, err = client.InvokeFunction("lilac", "refund", []string{proposalID1}, EndorserPeer...)
	if err != nil {
		utils.InfoError("refund", err)
		return err
	}
	utils.Info(string(resp.Payload))

	utils.InfoTips("发起用户对已经refund的lilac proposal再发起refund，应报proposal状态错误")
	resp, err = peerClient.InvokeFunction("lilac", "refund", []string{proposalID1}, EndorserPeer...)
	if err == nil {
		info := utils.InfoError("refund", fmt.Errorf("应报proposal状态错误"))
		return fmt.Errorf(info)
	}
	utils.Info(err)

	utils.InfoTips("对手用户对发起用户发起的lilac proposal发起refund，应报用户错误")
	resp, err = client.InvokeFunction("lilac", "refund", []string{proposalID1}, EndorserPeer...)
	if err == nil {
		info := utils.InfoError("refund", fmt.Errorf("应报用户错误"))
		return fmt.Errorf(info)
	}
	utils.Info(err)
	return nil
}

func resetBalance(client *contractinvoke.FabricClient) error {
	resp, err := client.InvokeFunction("assets", "reset", []string{}, EndorserPeer...)
	if err != nil {
		info := utils.InfoError("reset", err)
		return fmt.Errorf(info)
	}
	utils.Info(string(resp.Payload))
	return nil
}

var EndorserPeer = []string{
	"peer1.org1.raw.com",
	"peer1.org2.raw.com",
}

func main() {
	client := newClient("./config_files/fabric/user.yaml", "Org1", "User1")
	if err := resetBalance(client); err != nil {
		utils.InfoError("reset client1", err)
		return
	}
	clientPeer := newClient("./config_files/fabric/user.yaml", "Org2", "User1")
	if err := resetBalance(clientPeer); err != nil {
		utils.InfoError("reset client peer", err)
		return
	}

	if err := assetsFunc(client, clientPeer); err != nil {
		utils.InfoError("assetsfunc", err)
		return
	}

	if err := htlcFunc(client, clientPeer); err != nil {
		utils.InfoError("htlcfunc", err)
		return
	}

	if err := lilacFunc(client, clientPeer); err != nil {
		utils.InfoError("lilacFunc", err)
		return
	}

}
