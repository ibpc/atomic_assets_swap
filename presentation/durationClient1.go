// coding:utf-8
// 客户端1持久性测试
package presentation

import (
	"atomic_assets_swap/client"
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"fmt"
	"strconv"
	"time"
)

// 启动演示客户端1
// @param senderUserKey 发起Proposal的链名及用户名 chainName_userName
// @param sendRate 每秒发送的交易数量
func DurationClient1(mpChain2UserConfigPath map[string]interface{}, senderUserKey string, sendRate int) {
	currClientName := client1_name
	peerClientName := client2_name
	if len(mpChain2UserConfigPath) != 2 {
		panic("当前客户端测试用户必须为2")
	}

	// 保存全部key并判断senderUserKey是否在里面
	bFlag := false
	keys := make([]string, 0, len(mpChain2UserConfigPath))
	for k := range mpChain2UserConfigPath {
		keys = append(keys, k)
		if senderUserKey == k {
			bFlag = true
		}
	}
	if !bFlag {
		info := utils.InfoError("runAutoClient1", fmt.Errorf("输入的用户key必须存在于keys, userKey: %s, keys: %v", senderUserKey, keys))
		panic(info)
	}

	// 获取链名和用户名
	ls_tmp, err := utils.SplitStr(senderUserKey, "_")
	if err != nil {
		info := utils.InfoError("splitStr", err)
		panic(info)
	}
	if len(ls_tmp) != 2 {
		info := utils.InfoError("splitStr", fmt.Errorf("length of splitStr must equal to 2, get %d", len(ls_tmp)))
		panic(info)
	}
	chainName, userName := ls_tmp[0], ls_tmp[1]

	// 获取另一个链名和用户名
	key2 := ""
	for _, key := range keys {
		if key == senderUserKey {
			continue
		}
		key2 = key
		break
	}
	ls_tmp, err = utils.SplitStr(key2, "_")
	if err != nil {
		info := utils.InfoError("splitStr2", err)
		panic(info)
	}
	if len(ls_tmp) != 2 {
		info := utils.InfoError("splitStr2", fmt.Errorf("length of splitStr must equal to 2, get %d", len(ls_tmp)))
		panic(info)
	}

	// 启动cmd客户端
	c, err := client.NewClient("cmdClient", mpChain2UserConfigPath)
	if err != nil {
		info := utils.InfoError("NewClient", err)
		panic(info)
	}
	// 启动订阅以及自动化处理
	utils.InfoTips("开启订阅及自动化处理")
	c.RunAutoFunction()

	// 保存全部用户地址
	utils.InfoTips("保存当前用户全部地址进文件")
	saveAllUserAddress(fmt.Sprintf("%s_chain_user_address.txt", currClientName), c, mpChain2UserConfigPath)

	// 读取对方链的全部地址
	utils.InfoTips("等待读取对方全部地址")
	time.Sleep(10 * time.Second)

	// 读取对方全部地址
	utils.InfoTips("读取对方全部地址")
	ls_ls_peerAddressInfo := readAllUserAddress(fmt.Sprintf("%s_chain_user_address.txt", peerClientName))
	if len(ls_ls_peerAddressInfo) != 2 {
		panic("对端测试地址个数必须为2")
	}
	utils.Info("对端用户以及地址", ls_ls_peerAddressInfo)

	// ========= 查询当前用户的余额
	utils.InfoTips("查询当前余额")
	queryUserBalance(c, keys)

	// ========== 发起htlc proposal
	utils.InfoTips("============ 发起htlc proposal")
	// chainName, userName, address
	lsSameChainPeer := searchSameChainPeer(chainName, ls_ls_peerAddressInfo)
	toAddr := lsSameChainPeer[2]
	lsPeerChainPeer := searchPeerChainPeer(chainName, ls_ls_peerAddressInfo)
	toChainName := lsPeerChainPeer[0]

	// ##################### 正常测试
	for {
		start := time.Now()
		for i := 0; i < sendRate; i++ {
			// 发起一次调用
			go durationFunc(c, chainName, userName, toAddr, toChainName)
		}
		duration := time.Since(start)
		if duration < time.Second {
			time.Sleep(time.Second - duration)
		}
	} // for true
}

// 持久性测试，发起一次交易
// @param c 客户端
// @param chainName 发起的链名
// @param userName 发起的用户名
// @param toAddr 目的地址
// @param toChainName 目的链名
func durationFunc(
	c *client.Client,
	chainName, userName,
	toAddr, toChainName string,
) {
	proposalType := config.PROPOSAL_TYPE_HTLC
	timeLock := ""
	toAmount := "10"
	needAmount := "1"
	funcName := ""
	hashLock := ""
	switch proposalType {
	case config.PROPOSAL_TYPE_HTLC:
		funcName = newHtlcProposal
		hashLock = "preimage" + chainName + userName + strconv.Itoa(int(utils.GetTimestampUs()))
	case config.PROPOSAL_TYPE_LILAC:
		funcName = newLilacProposal
		hashLock = fmt.Sprintf("Preimage%d,preimage%d", utils.GetTimestampUs(), utils.GetTimestampUs())
	default:
		info := utils.InfoError("newProposal", fmt.Errorf("无效的proposal类型 %s", proposalType))
		panic(info)
	}
	if timeLock == config.NIL || timeLock == "" {
		timeLock = "600"
	}

	utils.InfoTips("发起", proposalType, "proposal")
	ls_cmd := []string{
		funcName,
		chainName,
		userName,
		toAddr,
		toAmount,
		hashLock,
		config.NIL,
		timeLock,
		toChainName,
		needAmount,
	}
	proposalID, err := c.RunCommond(ls_cmd)
	if err != nil {
		info := utils.InfoError("run "+proposalType+" commond", err)
		panic(info)
	}
	utils.InfoTips(proposalID)
}
