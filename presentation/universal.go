// coding:utf-8
// 公用函数
package presentation

import (
	"atomic_assets_swap/client"
	"atomic_assets_swap/utils"
	"fmt"
)

// 等待一段时间以供查看结果
var waitTimeToLookResult = 10

// 保存每一个用户的地址到文件
func saveAllUserAddress(fileName string, client *client.Client, mpChain2UserConfigPath map[string]interface{}) {
	ls_content := make([]string, 0)
	for key := range mpChain2UserConfigPath {
		ls_name, err := utils.SplitStr(key, "_")
		if err != nil {
			info := utils.InfoError("saveAllUserAddress 分割字符串", err)
			panic(info)
		}
		if len(ls_name) != 2 {
			info := utils.Info("分割的长度不为2错误, get: ", len(ls_name), ls_name)
			panic(info)
		}
		chainName, userName := ls_name[0], ls_name[1]

		// 获取地址
		commond := []string{
			"getAddress",
			chainName,
			userName,
		}
		addressAny, err := client.RunCommond(commond)
		if err != nil {
			info := utils.InfoError("run Addrss commond", err)
			panic(info)
		}
		address, ok := addressAny.(string)
		if !ok {
			panic("无效的地址类型")
		}
		// 保存地址
		ls_content = append(ls_content, chainName+" "+userName+" "+address)
	}

	// 保存进文件
	utils.RewriteFile(fileName, ls_content)
}

// 从user address里面读取链名字和用户名，用户地址
// @param fileName 文件名
// @return [[chainName, userName, address], ...]
func readAllUserAddress(fileName string) [][]string {
	ls_result := [][]string{}
	for _, line := range utils.ReadFileWithBufio(fileName) {
		ls_content, err := utils.SplitStr(line, " ")
		if err != nil {
			info := utils.InfoError("ReadFileWithBufio", err)
			panic(info)
		}
		if len(ls_content) != 3 {
			info := utils.Info("长度必须等于3", ls_content)
			panic(info)
		}
		ls_result = append(ls_result, ls_content)
	}
	return ls_result
}

// 查询用户余额
// @param c client
// @aram arrKeys 用户配置文件的key
func queryUserBalance(c *client.Client, arrKeys []string) {
	for _, keyName := range arrKeys {
		ls_result, err := utils.SplitStr(keyName, "_")
		if err != nil {
			info := utils.InfoError("queryUserBalance", err)
			panic(info)
		}
		if len(ls_result) != 2 {
			info := utils.InfoError("queryUserBalance splitstr", fmt.Errorf("length of keyName must be equal to 2"))
			panic(info)
		}
		chainName, userName := ls_result[0], ls_result[1]
		ls_cmd := []string{"getBalance", chainName, userName}
		result, err := c.RunCommond(ls_cmd)
		if err != nil {
			info := utils.InfoError("queryUserBalance runCommond", err)
			panic(info)
		}
		utils.Info(chainName, userName, "balance", result.(string))
	}
	utils.InfoTips(fmt.Sprintf("休眠%d秒查看结果", waitTimeToLookResult))
	fmt.Println()
	utils.Sleep(waitTimeToLookResult)
}

// 搜索和对端匹配的chainName, userName, address
// @param chainName 当前链名，也是对端链名
// @param lsPeerInfo 对端用户信息
// @return []string 相同链名的用户信息
func searchSameChainPeer(chainName string, lsPeerInfo [][]string) []string {
	if len(lsPeerInfo) != 2 {
		info := utils.InfoError(
			"searchSameChainPeer",
			fmt.Errorf("链用户个数必须为2, get %d", len(lsPeerInfo)))
		panic(info)
	}
	if lsPeerInfo[0][0] == lsPeerInfo[1][0] {
		info := utils.InfoError(
			"searchSameChainPeer",
			fmt.Errorf("对端用户的两个链名必须不相同, get %s, %s", lsPeerInfo[0][0], lsPeerInfo[1][0]))
		panic(info)
	}
	ls_result := []string{}
	for index := range lsPeerInfo {
		if lsPeerInfo[index][0] == chainName {
			ls_result = lsPeerInfo[index]
			break
		}
	}
	return ls_result
}

// 搜索和对端匹配的chainName, userName, address
// @param chainName 当前链名
// @param lsPeerInfo 对端用户信息
// @return []string 相同链名的用户信息
func searchPeerChainPeer(chainName string, lsPeerInfo [][]string) []string {
	if len(lsPeerInfo) != 2 {
		info := utils.InfoError("searchPeerChainPeer", fmt.Errorf("链用户个数必须为2, get %d", len(lsPeerInfo)))
		panic(info)
	}
	if lsPeerInfo[0][0] == lsPeerInfo[1][0] {
		info := utils.InfoError(
			"searchPeerChainPeer",
			fmt.Errorf("对端用户的两个链名必须不相同, get %s, %s", lsPeerInfo[0][0], lsPeerInfo[1][0]))
		panic(info)
	}
	ls_result := []string{}
	for index := range lsPeerInfo {
		if lsPeerInfo[index][0] != chainName {
			ls_result = lsPeerInfo[index]
			break
		}
	}
	return ls_result
}
