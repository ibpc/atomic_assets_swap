// coding:utf-8
// 常量
package presentation

const (
	// 客户端1的名字
	client1_name = "client1"
	// 客户端2的名字
	client2_name = "client2"
	// 发起新的htlc proposal的首命令
	newHtlcProposal = "newHtlcProposal"
	// 发起新的lilac proposal的首命令
	newLilacProposal = "newLilacProposal"
)
