// coding:utf-8
// 测试单链tps
package presentation

import (
	"atomic_assets_swap/client"
	"atomic_assets_swap/utils"
	"fmt"
	"time"
)

var toAmount = "10"
var needAmount = "1"

// user1 tps client, 发送方(当前函数):用户1与用户2交互, 接收方:用户2与用户1交互
// @param mpChain2UserConfigPath 用户、
// @param tps 发送的速率
func TPSclient1(tps int) {
	chain1UserConfigPath := "./config_files/sdkconfigs/chain1_sdkconfig1.yml"
	mpChain2UserConfigPath := make(map[string]interface{})
	chainName := "chain1"
	userName := "user1"
	mpChain2UserConfigPath[chainName+"_"+userName] = chain1UserConfigPath

	// 启动cmd客户端
	c, err := client.NewClient("cmdClient", mpChain2UserConfigPath)
	if err != nil {
		info := utils.InfoError("NewClient", err)
		panic(info)
	}
	// ========== 发起htlc proposal
	utils.InfoTips("============ 发起htlc proposal")

	for {
		start := time.Now()
		for i := 0; i < tps; i++ {
			toAddr := "aaaaaaaaaaaaaaaaaaaaaaaa"
			toChainName := "chain2"
			go tpsUser1NewProposal(c, "htlc", chainName, userName, toAddr, toAmount, toChainName, needAmount, "")
		}
		duration := time.Since(start)
		if duration < time.Second {
			time.Sleep(time.Second - duration)
		}
	}
}

// 测tps, user1发起一个新的proposal
func tpsUser1NewProposal(
	c *client.Client,
	proposalType,
	chainName,
	userName,
	toAddr,
	toAmount,
	toChainName,
	needAmount,
	timeLock string,
) {
	funcName := ""
	hashLock := ""
	// 防止原像冲突
	number := fmt.Sprintf("%d", utils.GetTimestampUs())
	nonce := utils.GetTimestampUs()
	switch proposalType {
	case "htlc":
		funcName = "newHtlcProposal"
		hashLock = "preimage" + chainName + userName + fmt.Sprintf("%d", nonce) + number + fmt.Sprint(utils.GetTimestampUs())
	case "lilac":
		funcName = "newLilacProposal"
		hashLock = fmt.Sprintf(
			"%sPreimage%s%s,%spreimage%s%s",
			number,
			fmt.Sprintf("%d", nonce),
			fmt.Sprint(utils.GetTimestampMs()),
			number,
			fmt.Sprintf("%d", nonce),
			fmt.Sprint(utils.GetTimestampMs()))
	default:
		info := utils.InfoError("newProposal", fmt.Errorf("无效的proposal类型 %s", proposalType))
		panic(info)
	}
	if timeLock == "nil" || timeLock == "" {
		timeLock = "600"
	}

	utils.InfoTips("发起", proposalType, "proposal")
	ls_cmd := []string{
		funcName,
		chainName,
		userName,
		toAddr,
		toAmount,
		hashLock,
		"nil",
		timeLock,
		toChainName,
		needAmount,
	}
	_, err := c.RunCommond(ls_cmd)
	if err != nil {
		info := utils.InfoError("run commond", err)
		panic(info)
	}
}
