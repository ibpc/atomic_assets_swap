// coding:utf-8
// 用户2客户端的持久性测试
package presentation

import (
	"atomic_assets_swap/client"
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"fmt"
	"time"
)

// 持久性测试
func DurationClient2(mpChain2UserConfigPath map[string]interface{}) {
	currClientName := client2_name
	peerClientName := client1_name
	if len(mpChain2UserConfigPath) != 2 {
		panic("当前客户端测试用户必须为2")
	}

	// 保存全部key
	keys := make([]string, 0, len(mpChain2UserConfigPath))
	for k := range mpChain2UserConfigPath {
		keys = append(keys, k)
	}

	// 启动cmd客户端
	c, err := client.NewClient("cmdClient", mpChain2UserConfigPath)
	if err != nil {
		info := utils.InfoError("NewClient", err)
		panic(info)
	}
	// 启动订阅以及自动化处理
	utils.InfoTips("开启订阅及自动化处理")
	c.RunAutoFunction()

	// 保存全部用户地址
	utils.InfoTips("保存当前用户全部地址进文件")
	saveAllUserAddress(fmt.Sprintf("%s_chain_user_address.txt", currClientName), c, mpChain2UserConfigPath)

	// 读取对方链的全部地址
	utils.InfoTips("等待读取对方全部地址")
	time.Sleep(10 * time.Second)

	// 读取对方全部地址
	utils.InfoTips("读取对方全部地址")
	ls_ls_peerAddressInfo := readAllUserAddress(fmt.Sprintf("%s_chain_user_address.txt", peerClientName))
	if len(ls_ls_peerAddressInfo) != 2 {
		panic("对端测试地址个数必须为2")
	}
	utils.Info("对端用户以及地址", ls_ls_peerAddressInfo)

	// ========= 查询当前用户的余额
	utils.InfoTips("查询当前余额")
	queryUserBalance(c, keys)

	// 保存已经处理过哪些id了
	mpIds := make(map[string]bool)

	// 启动命令行程序
	for {
		// 等待接收到proposal并制作对应的proposal发回去
		for {
			if len(c.GetReceiveProposal()) == 0 {
				utils.InfoTips("等待对方发起proposal ...")
				// 抽空删除
				for k := range mpIds {
					delete(mpIds, k)
				}

				time.Sleep(1 * time.Second)
				continue
			}
			break
		} // for

		for _, receiveeProposal := range c.GetReceiveProposal() {
			if mpIds[receiveeProposal.ProposalID] {
				// 处理过就跳过
				continue
			}
			// fmt.Println("len ", len(c.GetReceiveProposal()))
			// receiveeProposal := c.GetReceiveProposal()[0]
			// 对端链信息
			lsPeerChainPeer := searchPeerChainPeer(receiveeProposal.ChainName, ls_ls_peerAddressInfo)
			senderChainName, toAddr := lsPeerChainPeer[0], lsPeerChainPeer[2]

			var newProposalCmd []string
			toAmount := "1"
			needAmount := "10"
			switch receiveeProposal.ProposalType {
			case config.PROPOSAL_TYPE_HTLC:
				newProposalCmd = []string{
					newHtlcProposal,
					senderChainName,
					"user2",
					toAddr,
					toAmount,
					"nil",
					receiveeProposal.HashLock.(string),
					"300",
					receiveeProposal.ChainName,
					needAmount,
				}
			case config.PROPOSAL_TYPE_LILAC:
				hashLock := ""
				ls_hashlock, err := utils.Any2StringArray(receiveeProposal.HashLock)
				if err != nil {
					info := utils.InfoError("receive proposal hashlock", err)
					panic(info)
				}
				for _, str := range ls_hashlock {
					hashLock += "," + str
				}
				hashLock = hashLock[1:]
				newProposalCmd = []string{
					newLilacProposal,
					senderChainName,
					"user2",
					toAddr,
					toAmount,
					"nil",
					hashLock,
					"300",
					receiveeProposal.ChainName,
					needAmount,
				}
			default:
				panic("无效的type")
			}
			result, err := c.RunCommond(newProposalCmd)
			if err != nil {
				info := utils.InfoError("duration client2", err)
				panic(info)
			}
			utils.InfoTips(result)

			// 标记已经处理过
			mpIds[receiveeProposal.ProposalID] = true
		} // for receiveProposal
	} // for true
}
