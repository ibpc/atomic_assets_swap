# Copyright IBM Corp. All Rights Reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

version: '2'

volumes:
  orderer.example.com:
  # peer1.org.auth.com:
  # peer2.org.auth.com:
  peer1.org1.raw.com:
  peer2.org1.raw.com:
  peer1.org2.raw.com:
  peer2.org2.raw.com:


networks:
  crosschain:

services:

  orderer.example.com:
    container_name: orderer.example.com
    image: hyperledger/fabric-orderer:$IMAGE_TAG
    environment:
      - FABRIC_LOGGING_SPEC=DEBUG
      - ORDERER_GENERAL_LISTENADDRESS=0.0.0.0
      - ORDERER_GENERAL_LISTENPORT=7050
      - ORDERER_GENERAL_GENESISMETHOD=file
      - ORDERER_GENERAL_GENESISFILE=/var/hyperledger/orderer/orderer.genesis.block
      - ORDERER_GENERAL_LOCALMSPID=OrdererMSP
      - ORDERER_GENERAL_LOCALMSPDIR=/var/hyperledger/orderer/msp
      # enabled TLS
      - ORDERER_GENERAL_TLS_ENABLED=true
      - ORDERER_GENERAL_TLS_PRIVATEKEY=/var/hyperledger/orderer/tls/server.key
      - ORDERER_GENERAL_TLS_CERTIFICATE=/var/hyperledger/orderer/tls/server.crt
      - ORDERER_GENERAL_TLS_ROOTCAS=[/var/hyperledger/orderer/tls/ca.crt]
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric
    command: orderer
    volumes:
        # - ./channel-artifacts/genesis.block:/var/hyperledger/orderer/orderer.genesis.block
        - $CHANNEL_SAVE_DIR/genesis.block:/var/hyperledger/orderer/orderer.genesis.block
        - ${CONFIG_ROOT_DIR}/crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/msp:/var/hyperledger/orderer/msp
        - ${CONFIG_ROOT_DIR}/crypto-config/ordererOrganizations/example.com/orderers/orderer.example.com/tls/:/var/hyperledger/orderer/tls
        - orderer.example.com:/var/hyperledger/production/orderer
    ports:
      - 7050:7050
    networks:
      - crosschain

  peer1.org2.raw.com:
    container_name: peer1.org2.raw.com
    image: hyperledger/fabric-peer:$IMAGE_TAG
    environment:
      #Generic peer variables
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      # the following setting starts chaincode containers on the same
      # bridge network as the peers
      # https://docs.docker.com/compose/networking/
      # - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=fixtures_crosschain
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_GOSSIP_USELEADERELECTION=true
      - CORE_PEER_GOSSIP_ORGLEADER=false
      - CORE_PEER_PROFILE_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
      # Peer specific variabes
      - CORE_PEER_ID=peer1.org2.raw.com
      - CORE_PEER_ADDRESS=peer1.org2.raw.com:7051
      - CORE_PEER_LISTENADDRESS=0.0.0.0:7051
      - CORE_PEER_CHAINCODEADDRESS=peer1.org2.raw.com:7052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:7052
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer1.org2.raw.com:7051
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer2.org2.raw.com:8051
      - CORE_PEER_LOCALMSPID=Org2MSP
    volumes:
        - /var/run/:/host/var/run/
        - ${CONFIG_ROOT_DIR}/crypto-config/peerOrganizations/org2.raw.com/peers/peer1.org2.raw.com/msp:/etc/hyperledger/fabric/msp
        - ${CONFIG_ROOT_DIR}/crypto-config/peerOrganizations/org2.raw.com/peers/peer1.org2.raw.com/tls:/etc/hyperledger/fabric/tls
        - peer1.org2.raw.com:/var/hyperledger/production
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: peer node start
    ports:
      - 7051:7051
      - 7052:7052
      #保留端口
      - 7053:7053
      - 7054:7054
    networks:
      - crosschain

  peer2.org2.raw.com:
    container_name: peer2.org2.raw.com
    image: hyperledger/fabric-peer:$IMAGE_TAG
    environment:
      #Generic peer variables
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      # the following setting starts chaincode containers on the same
      # bridge network as the peers
      # https://docs.docker.com/compose/networking/
      # - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=fixtures_crosschain
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_GOSSIP_USELEADERELECTION=true
      - CORE_PEER_GOSSIP_ORGLEADER=false
      - CORE_PEER_PROFILE_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
      # Peer specific variabes
      - CORE_PEER_ID=peer2.org2.raw.com
      - CORE_PEER_ADDRESS=peer2.org2.raw.com:8051
      - CORE_PEER_LISTENADDRESS=0.0.0.0:8051
      - CORE_PEER_CHAINCODEADDRESS=peer2.org2.raw.com:8052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:8052
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer2.org2.raw.com:8051
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer1.org2.raw.com:7051
      - CORE_PEER_LOCALMSPID=Org2MSP
    volumes:
        - /var/run/:/host/var/run/
        - ${CONFIG_ROOT_DIR}/crypto-config/peerOrganizations/org2.raw.com/peers/peer2.org2.raw.com/msp:/etc/hyperledger/fabric/msp
        - ${CONFIG_ROOT_DIR}/crypto-config/peerOrganizations/org2.raw.com/peers/peer2.org2.raw.com/tls:/etc/hyperledger/fabric/tls
        - peer2.org2.raw.com:/var/hyperledger/production
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: peer node start
    ports:
      - 8051:8051
      - 8052:8052
      #保留端口
      - 8053:8053
      - 8054:8054
    networks:
      - crosschain

  peer1.org1.raw.com:
    container_name: peer1.org1.raw.com
    image: hyperledger/fabric-peer:$IMAGE_TAG
    environment:
      #Generic peer variables
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      # the following setting starts chaincode containers on the same
      # bridge network as the peers
      # https://docs.docker.com/compose/networking/
      # - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=fixtures_crosschain
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_GOSSIP_USELEADERELECTION=true
      - CORE_PEER_GOSSIP_ORGLEADER=false
      - CORE_PEER_PROFILE_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
      # Peer specific variabes
      - CORE_PEER_ID=peer1.org1.raw.com
      - CORE_PEER_ADDRESS=peer1.org1.raw.com:9051
      - CORE_PEER_LISTENADDRESS=0.0.0.0:9051
      - CORE_PEER_CHAINCODEADDRESS=peer1.org1.raw.com:9052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:9052
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer1.org1.raw.com:9051
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer1.org1.raw.com:9051
      - CORE_PEER_LOCALMSPID=Org1MSP
    volumes:
        - /var/run/:/host/var/run/
        - ${CONFIG_ROOT_DIR}/crypto-config/peerOrganizations/org1.raw.com/peers/peer1.org1.raw.com/msp:/etc/hyperledger/fabric/msp
        - ${CONFIG_ROOT_DIR}/crypto-config/peerOrganizations/org1.raw.com/peers/peer1.org1.raw.com/tls:/etc/hyperledger/fabric/tls
        - peer1.org1.raw.com:/var/hyperledger/production
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: peer node start
    ports:
      - 9051:9051
      - 9052:9052
      #保留端口
      - 9053:9053
      - 9054:9054
    networks:
      - crosschain

  peer2.org1.raw.com:
    container_name: peer2.org1.raw.com
    image: hyperledger/fabric-peer:$IMAGE_TAG
    environment:
      #Generic peer variables
      - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
      # the following setting starts chaincode containers on the same
      # bridge network as the peers
      # https://docs.docker.com/compose/networking/
      # - CORE_VM_DOCKER_HOSTCONFIG_NETWORKMODE=fixtures_crosschain
      - FABRIC_LOGGING_SPEC=INFO
      - CORE_PEER_TLS_ENABLED=true
      - CORE_PEER_GOSSIP_USELEADERELECTION=true
      - CORE_PEER_GOSSIP_ORGLEADER=false
      - CORE_PEER_PROFILE_ENABLED=true
      - CORE_PEER_TLS_CERT_FILE=/etc/hyperledger/fabric/tls/server.crt
      - CORE_PEER_TLS_KEY_FILE=/etc/hyperledger/fabric/tls/server.key
      - CORE_PEER_TLS_ROOTCERT_FILE=/etc/hyperledger/fabric/tls/ca.crt
      # Peer specific variabes
      - CORE_PEER_ID=peer2.org1.raw.com
      - CORE_PEER_ADDRESS=peer2.org1.raw.com:10051
      - CORE_PEER_LISTENADDRESS=0.0.0.0:10051
      - CORE_PEER_CHAINCODEADDRESS=peer2.org1.raw.com:10052
      - CORE_PEER_CHAINCODELISTENADDRESS=0.0.0.0:10052
      - CORE_PEER_GOSSIP_EXTERNALENDPOINT=peer2.org1.raw.com:10051
      - CORE_PEER_GOSSIP_BOOTSTRAP=peer2.org1.raw.com:10051
      - CORE_PEER_LOCALMSPID=Org1MSP
    volumes:
        - /var/run/:/host/var/run/
        - ${CONFIG_ROOT_DIR}/crypto-config/peerOrganizations/org1.raw.com/peers/peer2.org1.raw.com/msp:/etc/hyperledger/fabric/msp
        - ${CONFIG_ROOT_DIR}/crypto-config/peerOrganizations/org1.raw.com/peers/peer2.org1.raw.com/tls:/etc/hyperledger/fabric/tls
        - peer2.org1.raw.com:/var/hyperledger/production
    working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
    command: peer node start
    ports:
      - 10051:10051
      - 10052:10052
      #保留端口
      - 10053:10053
      - 10054:10054
    networks:
      - crosschain
  
  # cli:
  #   container_name: cli
  #   image: hyperledger/fabric-tools:$IMAGE_TAG
  #   tty: true
  #   stdin_open: true
  #   environment:
  #     - SYS_CHANNEL=$SYS_CHANNEL
  #     - GOPATH=/opt/gopath
  #     - CORE_VM_ENDPOINT=unix:///host/var/run/docker.sock
  #     #- FABRIC_LOGGING_SPEC=DEBUG
  #     - FABRIC_LOGGING_SPEC=INFO
  #     - CORE_PEER_ID=cli
  #     - CORE_PEER_ADDRESS=peer1.org2.raw.com:7051
  #     - CORE_PEER_LOCALMSPID=OrgAuthMSP
  #     - CORE_PEER_TLS_ENABLED=true
  #     - CORE_PEER_TLS_CERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.raw.com/peers/peer1.org2.raw.com/tls/server.crt
  #     - CORE_PEER_TLS_KEY_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.raw.com/peers/peer1.org2.raw.com/tls/server.key
  #     - CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.raw.com/peers/peer1.org2.raw.com/tls/ca.crt
  #     - CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.raw.com/users/Admin@org2.raw.com/msp
  #   # working_dir: /opt/gopath/src/github.com/hyperledger/fabric/peer
  #   working_dir: /opt/gopath/src/
  #   command: /bin/bash
  #   volumes:
  #       - /var/run/:/host/var/run/
  #       # !挂载文件夹到src,因为他找文件是从gopath/src开始找的
  #       - ${CONTRACT_SRC_DIR}:/opt/gopath/src
  #       # !挂载go root到docker的goroot
  #       - ${GOROOT}:/opt/go
  #       # 下面三个路径应该可以不用，或者挂载到本来的目录
  #       - ${CONFIG_ROOT_DIR}/crypto-config:/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/
  #       # - ./scripts:/opt/gopath/src/github.com/hyperledger/fabric/peer/scripts/
  #       # - ./channel-artifacts:/opt/gopath/src/github.com/hyperledger/fabric/peer/channel-artifacts
  #       - ${CHANNEL_SAVE_DIR}:/opt/gopath/src/github.com/hyperledger/fabric/peer/channel-artifacts
  #   ports:
  #     #保留端口
  #     - 15053:15053
  #     - 15054:15054
  #   depends_on:
  #     - orderer.example.com
  #     - peer1.org1.raw.com
  #     - peer2.org1.raw.com
  #     - peer1.org2.raw.com
  #     - peer2.org2.raw.com
  #   networks:
  #     - crosschain
