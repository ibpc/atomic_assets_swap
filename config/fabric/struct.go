/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description fabric相关结构体
*/
package fabric

import (
	mspclient "github.com/hyperledger/fabric-sdk-go/pkg/client/msp"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/resmgmt"
)

// OrgInfo 组织信息
type OrgInfo struct {
	OrgAdminUser  string            // 组织管理员账户
	OrgName       string            // 组织名称
	OrgMspId      string            // 组织MSP
	OrgUser       string            // 组织普通用户
	OrgMspClient  *mspclient.Client // 组织MSP客户端
	OrgResMgmt    *resmgmt.Client   // 组织资源管理客户端,部署
	Peers         []string          // 组织下的节点，安装链码的时候需要
	OrgAnchorFile string            // 组织锚节点信息
}

// OrdererInfo 排序节点信息
type OrdererInfo struct {
	OrdererAdminUser string          //排序节点账户
	OrdererOrgName   string          //排序组织
	OrdererEndpoint  string          //排序节点地址
	ChMgmtClient     *resmgmt.Client //通道资源管理客户端
}

// ChannelInfo 通道信息
type ChannelInfo struct {
	Orgs          []*OrgInfo  //通道内有哪些组织
	Orderer       OrdererInfo //排序节点信息
	ChannelID     string      //通道名称
	ChannelConfig string      //通道配置文件
}

// ChainCodeInfo 链码信息
type ChainCodeInfo struct {
	ChaincodeID      string // 链码ID
	ChannelID        string //在哪个通道上安装链码
	ChaincodeGoPath  string
	ChaincodePath    string
	ChaincodeVersion string
	ChaincodePolicy  string     //背书策略
	Orgs             []*OrgInfo //有哪些组织安装这个链码
}
