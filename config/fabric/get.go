/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 获取fabric常量配置
*/
package fabric

import (
	"fmt"
	"strings"
)

// 根据组织名和用户名获取对应的组织信息
func GetOrgFromNameAndUser(orgName, userName string) *OrgInfo {
	lowerOrgName := strings.ToLower(orgName)
	return &OrgInfo{
		OrgAdminUser:  "Admin",
		OrgName:       orgName,
		OrgMspId:      fmt.Sprintf("%sMSP", orgName),
		OrgUser:       userName,
		Peers:         []string{fmt.Sprintf("peer1.%s.raw.com", lowerOrgName), fmt.Sprintf("peer2.%s.raw.com", lowerOrgName)},
		OrgAnchorFile: CC_PROJ + fmt.Sprintf("/config_files/fabric/channel-artifacts/%sanchors.tx", orgName),
	}
}

// 获取组织信息
func GetOrgs() []*OrgInfo {
	return orgs
}

// 获取特定名字的组织信息
func GetDefaultOrg(orgName string) *OrgInfo {
	// todo:判断组织不存在
	return mpOrg[orgName]
}

// 获取节点信息
func GetOrders() *OrdererInfo {
	return &orderer
}

// 获取通道信息
func GetChannelInfo() *ChannelInfo {
	return &channelInfo
}

// 获取channelID
func GetChannelID() string {
	return CHANNEL_ID
}

// 新建一个链码信息
func NewChaincodeInfo(chaincodeID, chaincodePath string) ChainCodeInfo {
	return ChainCodeInfo{
		ChannelID:        CHANNEL_ID,
		ChaincodeID:      chaincodeID,
		ChaincodeVersion: CC_VERSION,
		ChaincodeGoPath:  CHAINCODE_GO_PATH,
		ChaincodePath:    chaincodePath,
		ChaincodePolicy:  "OR('Org1MSP.member','Org2MSP.member')",
		Orgs:             orgs,
	}
}

// ************ 获取链码信息 ************

// 获取order链码信息
func GetOrderInfo() *ChainCodeInfo {
	return &orderInfo
}
