/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description fabric常量配置
*/
package fabric

import "atomic_assets_swap/utils"

var (
	CC_PROJ           = utils.GetWorkSpace()
	CC_VERSION        = "1.0.0"
	CHAINCODE_GO_PATH = CC_PROJ + "/contracts/fabric"
	CHANNEL_CONFIG    = CC_PROJ + "/config_files/fabric/channel-artifacts/cc-channel.tx"
	CHANNEL_ID        = "cc-channel"
)

// 默认组织信息
var orgs = []*OrgInfo{
	// org1
	{
		OrgAdminUser:  "Admin",
		OrgName:       "Org1",
		OrgMspId:      "Org1MSP",
		OrgUser:       "User1",
		Peers:         []string{"peer1.org1.raw.com", "peer2.org1.raw.com"},
		OrgAnchorFile: CC_PROJ + "/config_files/fabric/channel-artifacts/Org1anchors.tx",
	},
	// org2
	{
		OrgAdminUser:  "Admin",
		OrgName:       "Org2",
		OrgMspId:      "Org2MSP",
		OrgUser:       "User2",
		Peers:         []string{"peer1.org2.raw.com", "peer2.org2.raw.com"},
		OrgAnchorFile: CC_PROJ + "/config_files/fabric/channel-artifacts/Org2anchors.tx",
	},
}

// 组织信息
var mpOrg = map[string]*OrgInfo{
	"Org1": orgs[0],
	"Org2": orgs[1],
}

// 排序节点信息
var orderer = OrdererInfo{
	OrdererAdminUser: "Admin",
	OrdererOrgName:   "OrdererOrg",
	OrdererEndpoint:  "orderer.example.com",
}

// 通道信息
var channelInfo = ChannelInfo{
	Orgs:          orgs,
	Orderer:       orderer,
	ChannelID:     CHANNEL_ID,
	ChannelConfig: CHANNEL_CONFIG,
}

// order
var orderInfo = ChainCodeInfo{
	ChannelID: CHANNEL_ID,
	//链码信息
	ChaincodeID:      "order",
	ChaincodeVersion: CC_VERSION,
	ChaincodeGoPath:  CHAINCODE_GO_PATH,
	ChaincodePath:    "order/",
	ChaincodePolicy:  "OR('OrgAuthMSP.member','Org1MSP.member','Org2MSP.member')",
	Orgs:             orgs,
}
