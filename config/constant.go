/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description chainmaker 常量定义
*/
package config

import (
	"chainmaker.org/chainmaker/pb-go/v2/common"
)

// User 用户基础信息
type User struct {
	TlsKeyPath, TlsCrtPath   string
	SignKeyPath, SignCrtPath string
}

// 合约相关
const (
	HTLC_CONTRACT_NAME      = "htlc"                         // htlc合约名
	HTLC_CONTRACT_PATH      = "./contracts/htlc/htlc.7z"     // htlc合约路径
	HTLC_CONTRACT_VERSION   = "v0"                           // htlc合约版本号
	LILAC_CONTRACT_NAME     = "lilac"                        // htlc合约名
	LILAC_CONTRACT_PATH     = "./contracts/lilac/lilac.7z"   // htlc合约路径
	LILAC_CONTRACT_VERSION  = "v0"                           // htlc合约版本号
	ASSETS_CONTRACT_NAME    = "assets"                       // 资产合约名
	ASSETS_CONTRACT_PATH    = "./contracts/assets/assets.7z" // 资产合约路径
	ASSETS_CONTRACT_VERSION = "v0"                           // 资产合约版本号
)

// 合约调用相关
const (
	// invoke方法
	INVOKE_METHOD = "invoke_contract"
	//合约虚拟机参数
	DOCKER_GO = common.RuntimeType_DOCKER_GO //DOCKER_GO类型虚拟机
	WASM      = common.RuntimeType_WASMER    //WASMER类型虚拟机
	GASM      = common.RuntimeType_GASM      //GASM类型虚拟机
	//合约创建超时时间(单位: s)
	CREATE_CONTRACT_TIMEOUT = 10
)

// lint报错的常量
const (
	// 新proposal事件名
	TOPIC_NEW_PROPOSAL = "newProposal"
	TOPIC_WITHDRAW     = "withdraw"

	// proposal类型
	PROPOSAL_TYPE_HTLC  = "htlc"
	PROPOSAL_TYPE_LILAC = "lilac"

	// nil字符串
	NIL = "nil"
	// exit字符串
	EXIT = "exit"
	// fabric链名
	FABRIC = "fabric"
	// chain1链名
	CHAIN1 = "chain1"
	// chain2链名
	CHAIN2 = "chain2"
	// transfer方法
	TRANSFER = "transfer"
)

// 测试文件用到的常量
const (
	// 测试用户名
	TEST_USER_NAME = "useraaa"
	// 测试用户证书路径
	TEST_USER_CERT_PATH = "../config_files/sdkconfigs/chain1_sdkconfig1.yml"
	// 测试链1地址
	TEST_CHAIN1_ADDRESS = "chain1address"

	// 错误链名
	TEST_ERR_CHAIN_NAME = "chain1_"
	// 错误用户名
	TEST_ERR_USER_NAME = "user1_"
)

// 名称信息
const (
	// 组织信息
	OrgId1 = "wx-org1.chainmaker.org" //组织1名称
	OrgId2 = "wx-org2.chainmaker.org" //组织2名称
	OrgId3 = "wx-org3.chainmaker.org" //组织3名称
	OrgId4 = "wx-org4.chainmaker.org" //组织4名称
	// 组织管理员信息
	Chain1Org1Admin1 = "chain1org1admin1" //链1组织1的管理员名称
	Chain1Org2Admin1 = "chain1org2admin1" //链1组织2的管理员名称
	Chain1Org3Admin1 = "chain1org3admin1" //链1组织3的管理员名称
	Chain1Org4Admin1 = "chain1org4admin1" //链1组织4的管理员名称
	Chain2Org1Admin1 = "chain2org1admin1" //链2组织1的管理员名称
	Chain2Org2Admin1 = "chain2org2admin1" //链2组织2的管理员名称
	Chain2Org3Admin1 = "chain2org3admin1" //链2组织3的管理员名称
	Chain2Org4Admin1 = "chain2org4admin1" //链2组织4的管理员名称
	//客户端信息
	Chain1Org1Client1 = "chain1org1client1" //链1组织1的客户端名称
	Chain1Org2Client1 = "chain1org2client1" //链1组织2的客户端名称
	Chain1Org3Client1 = "chain1org3client1" //链1组织3的客户端名称
	Chain1Org4Client1 = "chain1org4client1" //链1组织4的客户端名称
	Chain2Org1Client1 = "chain2org1client1" //链2组织1的客户端名称
	Chain2Org2Client1 = "chain2org2client1" //链2组织2的客户端名称
	Chain2Org3Client1 = "chain2org3client1" //链2组织3的客户端名称
	Chain2Org4Client1 = "chain2org4client1" //链2组织4的客户端名称
)

// 所有链1上的管理员用户信息
var Chain1Admins = []string{Chain1Org1Admin1, Chain1Org2Admin1, Chain1Org3Admin1, Chain1Org4Admin1}

// 所有链2上的管理员用户信息
var Chain2Admins = []string{Chain2Org1Admin1, Chain2Org2Admin1, Chain2Org3Admin1, Chain2Org4Admin1}
