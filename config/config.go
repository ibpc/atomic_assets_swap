/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description chainmaker基本配置信息
*/
package config

import (
	"path/filepath"
	"runtime"
)

// 获取固定路径下管理员证书信息
func GetAdmins() map[string]*User {
	var config_path string
	_, filename, _, _ := runtime.Caller(0)
	config_path = filepath.Dir(filepath.Dir(filename)) + "/config_files/"
	var admins = map[string]*User{
		Chain1Org1Admin1: {
			config_path + "chain1/crypto-config/wx-org1.chainmaker.org/user/admin1/admin1.tls.key",
			config_path + "chain1/crypto-config/wx-org1.chainmaker.org/user/admin1/admin1.tls.crt",
			config_path + "chain1/crypto-config/wx-org1.chainmaker.org/user/admin1/admin1.sign.key",
			config_path + "chain1/crypto-config/wx-org1.chainmaker.org/user/admin1/admin1.sign.crt",
		},
		Chain1Org2Admin1: {
			config_path + "chain1/crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.tls.key",
			config_path + "chain1/crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.tls.crt",
			config_path + "chain1/crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.sign.key",
			config_path + "chain1/crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.sign.crt",
		},
		Chain1Org3Admin1: {
			config_path + "chain1/crypto-config/wx-org3.chainmaker.org/user/admin1/admin1.tls.key",
			config_path + "chain1/crypto-config/wx-org3.chainmaker.org/user/admin1/admin1.tls.crt",
			config_path + "chain1/crypto-config/wx-org3.chainmaker.org/user/admin1/admin1.sign.key",
			config_path + "chain1/crypto-config/wx-org3.chainmaker.org/user/admin1/admin1.sign.crt",
		},
		Chain1Org4Admin1: {
			config_path + "chain1/crypto-config/wx-org4.chainmaker.org/user/admin1/admin1.tls.key",
			config_path + "chain1/crypto-config/wx-org4.chainmaker.org/user/admin1/admin1.tls.crt",
			config_path + "chain1/crypto-config/wx-org4.chainmaker.org/user/admin1/admin1.sign.key",
			config_path + "chain1/crypto-config/wx-org4.chainmaker.org/user/admin1/admin1.sign.crt",
		},
		Chain2Org1Admin1: {
			config_path + "chain2/crypto-config/wx-org1.chainmaker.org/user/admin1/admin1.tls.key",
			config_path + "chain2/crypto-config/wx-org1.chainmaker.org/user/admin1/admin1.tls.crt",
			config_path + "chain2/crypto-config/wx-org1.chainmaker.org/user/admin1/admin1.sign.key",
			config_path + "chain2/crypto-config/wx-org1.chainmaker.org/user/admin1/admin1.sign.crt",
		},
		Chain2Org2Admin1: {
			config_path + "chain2/crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.tls.key",
			config_path + "chain2/crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.tls.crt",
			config_path + "chain2/crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.sign.key",
			config_path + "chain2/crypto-config/wx-org2.chainmaker.org/user/admin1/admin1.sign.crt",
		},
		Chain2Org3Admin1: {
			config_path + "chain2/crypto-config/wx-org3.chainmaker.org/user/admin1/admin1.tls.key",
			config_path + "chain2/crypto-config/wx-org3.chainmaker.org/user/admin1/admin1.tls.crt",
			config_path + "chain2/crypto-config/wx-org3.chainmaker.org/user/admin1/admin1.sign.key",
			config_path + "chain2/crypto-config/wx-org3.chainmaker.org/user/admin1/admin1.sign.crt",
		},
		Chain2Org4Admin1: {
			config_path + "chain2/crypto-config/wx-org4.chainmaker.org/user/admin1/admin1.tls.key",
			config_path + "chain2/crypto-config/wx-org4.chainmaker.org/user/admin1/admin1.tls.crt",
			config_path + "chain2/crypto-config/wx-org4.chainmaker.org/user/admin1/admin1.sign.key",
			config_path + "chain2/crypto-config/wx-org4.chainmaker.org/user/admin1/admin1.sign.crt",
		},
	}
	return admins
}
