# /bin/bash
# 起链
# set -euo pipefail
set -euo
echo "=========== restart chainmaker =========="
./chainmaker_restart.sh
echo "=========== restart fabric =========="
./fabric_restart.sh
