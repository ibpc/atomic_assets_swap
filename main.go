/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 主函数
*/
package main

import (
	"atomic_assets_swap/client"
	"atomic_assets_swap/presentation"
	"atomic_assets_swap/utils"
	"fmt"
	"os"
)

// 启动客户端1
func runClient1() {
	// 每条链上的用户配置文件
	mpChain2UserConfigPath := make(map[string]interface{})

	mpChain2UserConfigPath["chain1_user1"] = chain1UserConfigPath
	mpChain2UserConfigPath["chain2_user1"] = chain2UserConfigPath
	// fabric客户端
	mpChain2UserConfigPath["fabric_user1"] = fabricUser1ConfigPath

	// 启动cmd客户端
	c, err := client.NewClient("cmdClient", mpChain2UserConfigPath)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	c.Run()
}

// 启动客户端2
func runClient2() {
	// 每条链上的用户配置文件
	mpChain2UserConfigPath := make(map[string]interface{})

	mpChain2UserConfigPath["chain1_user2"] = chain1User2ConfigPath
	mpChain2UserConfigPath["chain2_user2"] = chain2User2ConfigPath
	mpChain2UserConfigPath["fabric_user2"] = fabricUser2ConfigPath

	// 启动cmd客户端
	c, err := client.NewClient("cmdClient", mpChain2UserConfigPath)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	c.Run()
}

// 用户配置文件路径, 用于确定用户
var (
	chain1UserConfigPath  = "./config_files/sdkconfigs/chain1_sdkconfig1.yml"
	chain2UserConfigPath  = "./config_files/sdkconfigs/chain2_sdkconfig2.yml"
	chain1User2ConfigPath = "./config_files/sdkconfigs/chain1_sdkconfig3.yml"
	chain2User2ConfigPath = "./config_files/sdkconfigs/chain2_sdkconfig4.yml"
	fabricUser1ConfigPath = utils.StringArray2Any([]string{"./config_files/fabric/user.yaml", "Org1"})
	fabricUser2ConfigPath = utils.StringArray2Any([]string{"./config_files/fabric/user.yaml", "Org2"})
)

func main() {
	args := os.Args
	if len(args) != 2 {
		utils.InfoTips("===== usage ===== ./main client1 / client2")
		utils.InfoTips("main client1\t启动测试客户端1")
		utils.InfoTips("main client2\t启动测试客户端2")
		return
	}
	currClientName := args[1]

	switch currClientName {
	case "client1":
		runClient1()
	case "client2":
		runClient2()
	case "tps":
		// 3,5,10,15能抗住, 30,20,18,17就不行
		// 16刚好抗住
		presentation.TPSclient1(16)
	case "duration1":
		mpChain2UserConfigPath := make(map[string]interface{})
		mpChain2UserConfigPath["chain1_user1"] = chain1UserConfigPath
		mpChain2UserConfigPath["chain2_user1"] = chain2UserConfigPath
		presentation.DurationClient1(mpChain2UserConfigPath, "chain1_user1", 5)
	case "duration2":
		mpChain2UserConfigPath := make(map[string]interface{})
		mpChain2UserConfigPath["chain1_user2"] = chain1User2ConfigPath
		mpChain2UserConfigPath["chain2_user2"] = chain2User2ConfigPath
		presentation.DurationClient2(mpChain2UserConfigPath)
	default:
		utils.InfoError("main", fmt.Errorf("无效客户端指令"))
	}
}
