/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 链SDK实现的接口
*/
// Package projectinterface 项目涉及到的接口
package projectinterface

import "atomic_assets_swap/chainclient/proposalstruct"

// ChainClientInterface 客户端实现这个接口, 定义功能
type ChainClientInterface interface {
	// 在链chainName发起一个proposalType的proposal, 参数为params
	// @param proposalType 发起的proposal类型
	// @param toAddr 接收者地址
	// @param amount 发送的数量
	// @param hashlock 哈希锁
	// @param timelock 时间锁, 单位秒
	// @return string proposalID
	// @return error 错误信息
	NewProposal(propsalType, toAddr, amount string, hashlock, timelock interface{}) (string, error)

	// 撤销一个proposal
	// @param proposalType proposal类型"htlc","lilac"
	// @param proposalID 被撤销的id
	Refund(proposalType, proposalID string) error

	// 取钱
	// @param proposalType proposal类型"htlc","lilac"
	// @param proposalID 取这个proposal的钱
	// @param preimage 原像
	Withdraw(proposalType, proposlID string, preimage interface{}) error

	// 监听一个链的事件
	// @param contractName 被监听的合约名
	// @param eventName 被监听的事件名称
	// @param resultChain 获取监听结果的通道
	SubscribeEvent(contractName, eventName string, resultChan chan<- interface{})

	// 解析订阅事件为一个proposal结构体
	// @param eventInfo 每个链对应的事件信息
	// @return proposal 解析出来的proposal
	// @return error 解析失败
	ParseEventToProposal(eventInfo interface{}) (proposalstruct.Proposal, error)

	// 获取余额
	// @param address 地址,如果为""则返回自己的余额
	// @return string 余额
	// @return error err
	GetBalance(address string) (string, error)

	// 获取proposal
	// @param proposalID proposalID
	// @return proposal 这个proposalID对应的proposal
	// @return error err
	GetProposal(proposalType, proposalID string) (proposalstruct.Proposal, error)

	// 获取地址
	// @return string 地址
	// @return error err
	GetAddress() (string, error)

	// 获取chainID
	// @return string id
	// @return error err
	GetChainID() (string, error)

	// 关闭客户端
	// 最后 defer
	Close()
}
