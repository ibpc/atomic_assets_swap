module atomic_assets_swap

go 1.16

require (
	bou.ke/monkey v1.0.2
	chainmaker.org/chainmaker/contract-sdk-go/v2 v2.3.3
	chainmaker.org/chainmaker/pb-go/v2 v2.3.2
	chainmaker.org/chainmaker/sdk-go/v2 v2.3.2
	github.com/hyperledger/fabric-chaincode-go v0.0.0-20220920210243-7bc6fa0dd58b
	github.com/hyperledger/fabric-protos-go v0.0.0-20220613214546-bf864f01d75e
	github.com/hyperledger/fabric-sdk-go v1.0.0
)

replace (
	github.com/go-kit/kit v0.10.0 => github.com/go-kit/kit v0.8.0
	github.com/hyperledger/fabric-protos-go v0.0.0-20220613214546-bf864f01d75e => github.com/hyperledger/fabric-protos-go v0.0.0-20200707132912-fee30f3ccd23
)

require (
	github.com/Knetic/govaluate v3.0.1-0.20171022003610-9aa49832a739+incompatible // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/mattn/go-isatty v0.0.17 // indirect
	github.com/rogpeppe/go-internal v1.8.0 // indirect
	github.com/stretchr/testify v1.8.1 // indirect
	github.com/tidwall/gjson v1.14.4 // indirect
	go.uber.org/zap v1.24.0 // indirect
	golang.org/x/crypto v0.5.0 // indirect
	golang.org/x/net v0.7.0 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.2.1 // indirect
)
