# 长安链部分合约测试脚本
# 用户配置文件
CHAIN1_USER1_SDKCONFIG=./config_files/sdkconfigs/chain1_sdkconfig1.yml
CHAIN1_USER2_SDKCONFIG=./config_files/sdkconfigs/chain1_sdkconfig3.yml
CHAIN2_USER1_SDKCONFIG=./config_files/sdkconfigs/chain2_sdkconfig2.yml
CHAIN2_USER2_SDKCONFIG=./config_files/sdkconfigs/chain2_sdkconfig4.yml

# Set{User Account Address}
CHAIN1_USER1_ADDRESS=c8b96d06d43d8d558e3d3f16f176d08025a51f18
CHAIN1_USER2_ADDRESS=0170f57420beb1a828894099a9127c65192b4893
CHAIN2_USER1_ADDRESS=a47bd5f0704b6c2cb89210c50d732e239916612e
CHAIN2_USER2_ADDRESS=37e907def0dd060483bd2987fd188c756178c7a6

# # bin/bash
# #! 首先初始化余额
# echo "初始化用户资产"
# for chainid in 1 2;
# do
#     for index in {1..4}
#     # for index in {1..2}
#     do
#         echo "初始化 链${chainid} 用户${index} 链上资产"
#         ./bin/cmc client contract user invoke --contract-name=assets --method=reset --sdk-conf-path=./config_files/sdkconfigs/chain${chainid}_sdkconfig${index}.yml --params="{}" --sync-result=true --result-to-string=true
#     done
# done

# echo "开始测试合约功能..."

# echo "查询用户账户地址"
# index=1
# for userid in 1 2
# do
#     echo "查询 链1 用户${userid} 账户地址"
#     ./bin/cmc client contract user get --contract-name=assets --method=getAddress --sdk-conf-path=./config_files/sdkconfigs/chain1_sdkconfig${index}.yml --params="{}" --result-to-string=true
#     index=$[$index+1]
#     echo "查询 链2 用户${userid} 账户地址"
#     ./bin/cmc client contract user get --contract-name=assets --method=getAddress --sdk-conf-path=./config_files/sdkconfigs/chain2_sdkconfig${index}.yml --params="{}" --result-to-string=true
#     index=$[$index+1]
# done

# echo "查询用户账户初始跨链资产状况"
# ./bin/cmc client contract user get \
# --contract-name=assets \
# --method=balanceOf \
# --sdk-conf-path=${CHAIN1_USER1_SDKCONFIG} \
# --params="{\"address\":\"${CHAIN1_USER1_ADDRESS}\"}" \
# --result-to-string=true

# echo "向长安链chain1发起htlc跨链资产互换提案"
# ./bin/cmc client contract user invoke \
# --contract-name=htlc \
# --method=newProposal \
# --sdk-conf-path=${CHAIN1_USER1_SDKCONFIG} \
# --params="{\"receiver\":\"${CHAIN1_USER2_ADDRESS}\",\"amount\":\"10\",\"hashlock\":\"107661134f21fc7c02223d50ab9eb3600bc3ffc3712423a1e47bb1f9a9dbf55f\",\"timelock\":\"600\"}" \
# --sync-result=true \
# --result-to-string=true

# echo "根据上一笔交易信息提取proposalID并查询长安链chain1上的该笔htlc提案"
# ./bin/cmc client contract user get \
# --contract-name=htlc \
# --method=getProposal \
# --sdk-conf-path=${CHAIN1_USER2_SDKCONFIG} \
# --params="{\"proposalID\":\"04eaab5f44679ceeb1ed826c73ce510a2dc220ce64a1a095c1235b81fe1da830\"}" \
# --result-to-string=true

# echo "chain1user2从长安链chain1的htlc跨链代币兑换提案中取款"
# ./bin/cmc client contract user invoke \
# --contract-name=htlc \
# --method=withdraw \
# --sdk-conf-path=${CHAIN1_USER2_SDKCONFIG} \
# --params="{\"proposalID\":\"04eaab5f44679ceeb1ed826c73ce510a2dc220ce64a1a095c1235b81fe1da830\",\"preimage\":\"preimage\"}" \
# --sync-result=true \
# --result-to-string=true

# echo "chain1user2尝试从chain1user1发起的htlc跨链代币兑换提案中退款"
# ./bin/cmc client contract user invoke \
# --contract-name=htlc \
# --method=refund \
# --sdk-conf-path=${CHAIN1_USER2_SDKCONFIG} \
# --params="{\"proposalID\":\"05694f362b682178860ac219da7426baa43a7a46fb8da96ca2ecfefc6e8fe237\"}" \
# --sync-result=true \
# --result-to-string=true

# echo "chain1user1从长安链chain1的htlc跨链代币兑换提案中退款"
# ./bin/cmc client contract user invoke \
# --contract-name=htlc \
# --method=refund \
# --sdk-conf-path=${CHAIN1_USER1_SDKCONFIG} \
# --params="{\"proposalID\":\"05694f362b682178860ac219da7426baa43a7a46fb8da96ca2ecfefc6e8fe237\"}" \
# --sync-result=true \
# --result-to-string=true

# ---------------------------测试lilac合约---------------------------
# echo "向长安链chain1发起lilac跨链资产互换提案"
# ./bin/cmc client contract user invoke \
# --contract-name=lilac \
# --method=newProposal \
# --sdk-conf-path=${CHAIN1_USER1_SDKCONFIG} \
# --params="{\"receiver\":\"${CHAIN1_USER2_ADDRESS}\",\"amount\":\"10\",\"hashlock\":[\"5144f34d1b8edc707120dc254bf56e00bd9f5a6f5e6892f72103408f43ee9ded\", \"7a869987c84cbd23568dab928f0229581c2658e61a1ee37214c83ea16ba9cba7\"],\"timelock\":\"600\"}" \
# --sync-result=true \
# --result-to-string=true

# echo "chain1user1尝试从自己发的lilac跨链代币兑换提案中取款"
# ./bin/cmc client contract user invoke \
# --contract-name=lilac \
# --method=withdraw \
# --sdk-conf-path=${CHAIN1_USER1_SDKCONFIG} \
# --params="{\"proposalID\":\"7395706bdae494980149bfb5709ed585d9401d8bcc59f2ad4b3a80b6dca786c9\",\"preimage\":[\"preimage1\",\"preimage2\"]}" \
# --sync-result=true \
# --result-to-string=true

# echo "chain1user2尝试用错误原像从lilac跨链代币兑换提案中取款"
# ./bin/cmc client contract user invoke \
# --contract-name=lilac \
# --method=withdraw \
# --sdk-conf-path=${CHAIN1_USER2_SDKCONFIG} \
# --params="{\"proposalID\":\"7395706bdae494980149bfb5709ed585d9401d8bcc59f2ad4b3a80b6dca786c9\",\"preimage\":[\"xxx\",\"yyy\"]}" \
# --sync-result=true \
# --result-to-string=true

# echo "chain1user2从lilac跨链代币兑换提案中取款"
# ./bin/cmc client contract user invoke \
# --contract-name=lilac \
# --method=withdraw \
# --sdk-conf-path=${CHAIN1_USER2_SDKCONFIG} \
# --params="{\"proposalID\":\"7395706bdae494980149bfb5709ed585d9401d8bcc59f2ad4b3a80b6dca786c9\",\"preimage\":[\"preimage1\",\"preimage2\"]}" \
# --sync-result=true \
# --result-to-string=true

# echo "chain1user2尝试重复从lilac跨链代币兑换提案中取款"
# ./bin/cmc client contract user invoke \
# --contract-name=lilac \
# --method=withdraw \
# --sdk-conf-path=${CHAIN1_USER2_SDKCONFIG} \
# --params="{\"proposalID\":\"7395706bdae494980149bfb5709ed585d9401d8bcc59f2ad4b3a80b6dca786c9\",\"preimage\":[\"preimage1\",\"preimage2\"]}" \
# --sync-result=true \
# --result-to-string=true

# echo "根据proposalID查询长安链chain1上的该笔lilac提案"
# ./bin/cmc client contract user get \
# --contract-name=lilac \
# --method=getProposal \
# --sdk-conf-path=${CHAIN1_USER2_SDKCONFIG} \
# --params="{\"proposalID\":\"7395706bdae494980149bfb5709ed585d9401d8bcc59f2ad4b3a80b6dca786c9\"}" \
# --result-to-string=true

# echo "chain1user1向长安链chain1发起一笔短时lilac跨链资产互换提案"
# ./bin/cmc client contract user invoke \
# --contract-name=lilac \
# --method=newProposal \
# --sdk-conf-path=${CHAIN1_USER1_SDKCONFIG} \
# --params="{\"receiver\":\"${CHAIN1_USER2_ADDRESS}\",\"amount\":\"10\",\"hashlock\":[\"5144f34d1b8edc707120dc254bf56e00bd9f5a6f5e6892f72103408f43ee9ded\", \"7a869987c84cbd23568dab928f0229581c2658e61a1ee37214c83ea16ba9cba7\"],\"timelock\":\"600\"}" \
# --sync-result=true \
# --result-to-string=true

echo "chain1user1尝试在时间锁未到期前向lilac跨链代币兑换提案发起退款"
./bin/cmc client contract user invoke \
--contract-name=lilac \
--method=refund \
--sdk-conf-path=${CHAIN1_USER1_SDKCONFIG} \
--params="{\"proposalID\":\"145427f6c09adbf8eb16dedb8c7a289ef3004c180840904bfa91bc69da775eb8\"}" \
--sync-result=true \
--result-to-string=true

echo "chain1user2尝试对别人发起的lilac跨链代币兑换提案退款"
./bin/cmc client contract user invoke \
--contract-name=lilac \
--method=refund \
--sdk-conf-path=${CHAIN1_USER2_SDKCONFIG} \
--params="{\"proposalID\":\"145427f6c09adbf8eb16dedb8c7a289ef3004c180840904bfa91bc69da775eb8\",\"preimage\":[\"preimage1\",\"preimage2\"]}" \
--sync-result=true \
--result-to-string=true

# echo "chain1user1在时间锁到期后从lilac跨链代币兑换提案中退款"
# ./bin/cmc client contract user invoke \
# --contract-name=lilac \
# --method=refund \
# --sdk-conf-path=${CHAIN1_USER1_SDKCONFIG} \
# --params="{\"proposalID\":\"2b2d1f87f6a4ddf2ed20e3644751d2d91ed0d9ef6bfbb3311ee665eb242e2045\",\"preimage\":[\"preimage1\",\"preimage2\"]}" \
# --sync-result=true \
# --result-to-string=true

# echo "chain1user1尝试从已退款lilac跨链代币兑换提案中重复退款"
# ./bin/cmc client contract user invoke \
# --contract-name=lilac \
# --method=refund \
# --sdk-conf-path=${CHAIN1_USER1_SDKCONFIG} \
# --params="{\"proposalID\":\"2b2d1f87f6a4ddf2ed20e3644751d2d91ed0d9ef6bfbb3311ee665eb242e2045\",\"preimage\":[\"preimage1\",\"preimage2\"]}" \
# --sync-result=true \
# --result-to-string=true