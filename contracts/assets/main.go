/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author longbo,jch
@Description 该文件中实现了资产合约,基于erc20标准接口实现的资产合约功能
*/
package main

import (
	"encoding/json"
	"log"
	"strconv"

	pb "chainmaker.org/chainmaker/contract-sdk-go/v2/pb/protogo"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sandbox"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
)

type Assets struct {
}

// 跨链资产数据结构
type CrossToken struct {
	TotalSupply int64  `json:"total_supply"` //token总量
	Name        string `json:"name"`         //token名称
	Version     string `json:"version"`      //token版本
}

// 生成新的跨链资产
func NewCrossToken(totalSupply int64, name, version string) *CrossToken {
	return &CrossToken{
		TotalSupply: totalSupply,
		Name:        name,
		Version:     version,
	}
}

// 初始化合约
// @param totalSupply token总量
// @param name token名称
// @param version token版本
// @param users 初始化用户资产
func (a *Assets) InitContract() pb.Response {
	params := sdk.Instance.GetArgs()
	totalSupplyString := string(params["totalSupply"])
	name, version := string(params["name"]), string(params["version"])
	// users := string(params["users"])
	if len(totalSupplyString) == 0 {
		return sdk.Error("need totalSupply")
	}
	totalSupply, err := strconv.ParseInt(totalSupplyString, 10, 64)
	//token总量不能 < 0
	if totalSupply < 0 {
		return sdk.Error("totalsupply < 0")
	}
	if err != nil {
		return sdk.Error("parse totalsupply error")
	}
	crossToken := NewCrossToken(totalSupply, name, version)
	crossTokenJson, err := json.Marshal(crossToken)
	if err != nil {
		return sdk.Error("CrossToken json error")
	}
	//更新跨链资产信息
	if err := sdk.Instance.PutStateFromKeyByte("globalCrossToken", crossTokenJson); err != nil {
		return sdk.Error("put state error")
	}
	sdk.Instance.EmitEvent("CrossTokenInit", []string{totalSupplyString, name, version})
	//初始化用户资产
	// userList := strings.Split(users, ",")
	//每人初始化1000
	// for _, user := range userList {
	// 	sdk.Instance.PutStateFromKey(user, "1000")
	// 	// if err := sdk.Instance.PutStateFromKey(user, "1000"); err != nil {
	// 	// 	return sdk.Error("put state error")
	// 	// }
	// }
	//抛出CrossTokenInit事件
	// sdk.Instance.EmitEvent("InitUser", userList)
	return sdk.Success([]byte("Init Success"))
}

// 升级合约
func (a *Assets) UpgradeContract() pb.Response {
	return sdk.Success([]byte("Upgrade success"))
}

func (a *Assets) InvokeContract(method string) pb.Response {
	// according method segment to select contract functions
	switch method {
	case "CrossTokenInfo":
		return a.CrossTokenInfo()
	case "approve":
		return a.approve()
	case "allowance":
		return a.allowance()
	case "transfer":
		return a.transfer()
	case "balanceOf":
		return a.balanceOf()
	case "transferFrom":
		return a.transferFrom()
	case "mint":
		return a.mint()
	case "burn":
		return a.burn()
	case "getAddress":
		return a.getAddress()
	case "registContractAddress":
		return a.registContractAddress()
	case "reset":
		return a.reset()
	default:
		return sdk.Error("invalid method")
	}
}

// 返回整个跨链资产信息
// @return CrossToken的json序列化结果
func (a *Assets) CrossTokenInfo() pb.Response {
	tf, err := sdk.Instance.GetStateFromKeyByte("globalCrossToken")
	if err != nil {
		return sdk.Error("get CrossToken info error")
	}
	return sdk.Success(tf)
}

// 获取授权key
// 绑定owner和spender关系
func makeAllowanceKey(owner, spender string) string {
	return "allow__" + owner + "__to__" + spender
}

// 授权相关函数，当前账户允许从指定账户转账
// @param spenderAddress 消费者
// @param value 授权金额
func (a *Assets) approve() pb.Response {
	params := sdk.Instance.GetArgs()
	owner, _ := sdk.Instance.Origin()
	valueString := string(params["value"])
	value, err := strconv.ParseInt(valueString, 10, 64)
	if value < 0 || err != nil {
		return sdk.Error("parse int error")
	}
	spender := string(params["spenderAddress"])
	key := makeAllowanceKey(owner, spender)
	//更新授权关系
	if err := sdk.Instance.PutStateFromKey(key, valueString); err != nil {
		return sdk.Error("put state error")
	}
	// 抛出approve事件
	sdk.Instance.EmitEvent("approve", []string{key + " : " + valueString})
	return sdk.Success(nil)
}

// 授权相关函数，当前账户允许从指定账户转账的金额数
// @param ownerAddress 原token持有者
// @param spenderAddress 授权消费者
// @return 授权金额余量
func (a *Assets) allowance() pb.Response {
	params := sdk.Instance.GetArgs()
	owner, spender := string(params["ownerAddress"]), string(params["spenderAddress"])
	key := makeAllowanceKey(owner, spender)
	valueString, err := sdk.Instance.GetStateFromKey(key)
	if err != nil {
		return sdk.Error("allowance error")
	}
	if len(valueString) == 0 {
		valueString = "0"
	}
	return sdk.Success([]byte(key + ":" + valueString))
}

// 向指定地址发送一定数量的CrossToken
// @param toAddress 目的地址
// @param value 金额数量
func (a *Assets) transfer() pb.Response {
	//参数检查
	params := sdk.Instance.GetArgs()
	senderpk, _ := sdk.Instance.Sender()
	toAddress := string(params["toAddress"])
	if len(toAddress) == 0 {
		return sdk.Error("get toAddress error")
	}
	valueString := string(params["value"])
	value, err := strconv.ParseInt(valueString, 10, 64)
	if value < 0 || err != nil {
		return sdk.Error("value error")
	}
	//如果目的地址也是自己,直接返回
	if senderpk == toAddress {
		sdk.Instance.EmitEvent("transfer", []string{senderpk, toAddress})
	}
	senderBalanceString, err := sdk.Instance.GetStateFromKey(senderpk)
	if err != nil || len(senderBalanceString) == 0 {
		return sdk.Error("get state error")
	}
	senderBalance, err := strconv.ParseInt(senderBalanceString, 10, 64)
	if senderBalance < 0 || err != nil {
		return sdk.Error("get sender balance error")
	}
	toBalanceString, err := sdk.Instance.GetStateFromKey(toAddress)
	if err != nil {
		return sdk.Error("get state error")
	}
	toBalance := int64(0)
	if len(toBalanceString) != 0 {
		toBalance, _ = strconv.ParseInt(toBalanceString, 10, 64)
	}

	if senderBalance < value {
		return sdk.Error("sender balance not enough")
	}
	senderBalance -= value
	toBalance += value
	senderBalanceS := strconv.FormatInt(senderBalance, 10)
	toBalanceS := strconv.FormatInt(toBalance, 10)
	// 更新各账户余额
	if err := sdk.Instance.PutStateFromKey(senderpk, senderBalanceS); err != nil {
		return sdk.Error("put sender balance error")
	}
	if err := sdk.Instance.PutStateFromKey(toAddress, toBalanceS); err != nil {
		return sdk.Error("put sender balance error")
	}
	// 抛出transfer事件
	sdk.Instance.EmitEvent("transfer", []string{senderpk + " to " + toAddress + " : " + valueString})
	return sdk.Success(nil)
}

// 由指定发送地址向指定地址发送一定数量CrossToken
// @param senderAddress 源地址
// @param toAddress 目的地址
// @param value 金额数量
func (a *Assets) transferFrom() pb.Response {
	//参数检查
	params := sdk.Instance.GetArgs()
	owner, to := string(params["senderAddress"]), string(params["toAddress"])
	valueString := string(params["value"])
	value, err := strconv.ParseInt(valueString, 10, 64)
	if err != nil || value < 0 {
		return sdk.Error("parse value error")
	}
	sender, _ := sdk.Instance.Sender()
	if owner == to {
		sdk.Instance.EmitEvent("transferFrom", []string{owner, to})
		return sdk.Success(nil)
	}
	//检查授权情况
	key := makeAllowanceKey(owner, sender)
	ownerBalanceString, err := sdk.Instance.GetStateFromKey(owner)
	if err != nil {
		return sdk.Error("get state error")
	}
	ownerBalance, err := strconv.ParseInt(ownerBalanceString, 10, 64)
	if ownerBalance < 0 || err != nil {
		return sdk.Error("ownerBalance need > 0")
	}
	allowanceValueString, err := sdk.Instance.GetStateFromKey(key)
	if err != nil || len(allowanceValueString) == 0 {
		return sdk.Error("not allowance")
	}
	allowanceValue, err := strconv.ParseInt(allowanceValueString, 10, 64)
	if allowanceValue < 0 || err != nil {
		return sdk.Error("allowanceValue error")
	}
	if allowanceValue < value {
		return sdk.Error("allowanceValue should > value")
	}
	toBalanceString, err := sdk.Instance.GetStateFromKey(to)
	if err != nil {
		return sdk.Error("get state error")
	}
	toBalance := int64(0)
	if len(toBalanceString) != 0 {
		toBalance, _ = strconv.ParseInt(toBalanceString, 10, 64)
	}
	if ownerBalance < value {
		return sdk.Error("ownerBalance should > value")
	}
	//转账操作
	ownerBalance -= value
	toBalance += value
	allowanceValue -= value
	ownerBalanceS := strconv.FormatInt(ownerBalance, 10)
	toBalanceS := strconv.FormatInt(toBalance, 10)
	allowanceValueS := strconv.FormatInt(allowanceValue, 10)
	//更新代理转账结果
	if err := sdk.Instance.PutStateFromKey(owner, ownerBalanceS); err != nil {
		return sdk.Error("put state error")
	}
	if err := sdk.Instance.PutStateFromKey(to, toBalanceS); err != nil {
		return sdk.Error("put state error")
	}
	if err := sdk.Instance.PutStateFromKey(key, allowanceValueS); err != nil {
		return sdk.Error("put state error")
	}
	//抛出transferfrom事件
	sdk.Instance.EmitEvent("transferFrom", []string{sender + " from " + owner + " to " + to + " : " + valueString})
	return sdk.Success(nil)
}

// 获取指定地址的余额
// @params address 指定地址
func (a *Assets) balanceOf() pb.Response {
	params := sdk.Instance.GetArgs()
	address := string(params["address"])
	//查询链上指定地址的状态信息
	balance, err := sdk.Instance.GetStateFromKey(address)
	if err != nil {
		return sdk.Error("get balance error")
	}
	if len(balance) == 0 {
		return sdk.Success([]byte("0"))
	}
	return sdk.Success([]byte(balance))
}

// 发行货币到指定地址
// 只有合约和creator有权调用
// @params:address
// @params:value
func (a *Assets) mint() pb.Response {
	senderPK, _ := sdk.Instance.GetSenderPk()
	if !__checkValidtAddress(senderPK) {
		return sdk.Error("have no privillege to invoke")
	}
	params := sdk.Instance.GetArgs()
	address, valueString := string(params["address"]), string(params["value"])
	value, err := strconv.ParseInt(valueString, 10, 64)
	if err != nil || value < 0 {
		return sdk.Error("parse value error")
	}
	//获取address的余额
	balanceString, err := sdk.Instance.GetStateFromKey(address)
	if err != nil {
		return sdk.Error("get balance error")
	}
	balance := int64(0)
	if len(balanceString) != 0 {
		balance, _ = strconv.ParseInt(balanceString, 10, 64)
	}
	//更新余额
	newBalance := balance + value
	newBalances := strconv.FormatInt(newBalance, 10)
	if err1 := sdk.Instance.PutStateFromKey(address, newBalances); err1 != nil {
		return sdk.Error("put state error")
	}
	//更新token总量
	tf, err := sdk.Instance.GetStateFromKeyByte("globalCrossToken")
	if err != nil {
		return sdk.Error("getstate error")
	}
	var gCrossToken CrossToken
	err = json.Unmarshal(tf, &gCrossToken)
	if err != nil {
		return sdk.Error("unmarshal CrossToken error")
	}
	gCrossToken.TotalSupply += value
	gCrossTokenJson, err := json.Marshal(gCrossToken)
	if err != nil {
		return sdk.Error("CrossToken json error")
	}
	//更新总跨链资产信息
	if err := sdk.Instance.PutStateFromKeyByte("globalCrossToken", gCrossTokenJson); err != nil {
		return sdk.Error("put state error")
	}
	sdk.Instance.EmitEvent("mint", []string{"mint to address:" + address + " " + valueString})
	return sdk.Success(nil)
}

// 从指定地址销毁货币
// 只有合约和creator有权调用
// @params:address
// @params:value
func (a *Assets) burn() pb.Response {
	senderPK, _ := sdk.Instance.GetSenderPk()
	if !__checkValidtAddress(senderPK) {
		return sdk.Error("have no privillege to invoke")
	}
	params := sdk.Instance.GetArgs()
	address, valueString := string(params["address"]), string(params["value"])
	value, err := strconv.ParseInt(valueString, 10, 64)
	if err != nil || value < 0 {
		return sdk.Error("parse value error")
	}
	//获取address的余额
	balanceString, err := sdk.Instance.GetStateFromKey(address)
	if err != nil {
		return sdk.Error("get balance error")
	}
	balance := int64(0)
	if len(balanceString) != 0 {
		balance, _ = strconv.ParseInt(balanceString, 10, 64)
	}
	//余额检查
	if balance < value {
		return sdk.Error("not enough CrossToken to burn")
	}
	//更新余额
	newBalance := balance - value
	newBalances := strconv.FormatInt(newBalance, 10)
	if err1 := sdk.Instance.PutStateFromKey(address, newBalances); err1 != nil {
		return sdk.Error("put state error")
	}
	//更新token总量
	tf, err := sdk.Instance.GetStateFromKeyByte("globalCrossToken")
	if err != nil {
		return sdk.Error("getstate error")
	}
	var gCrossToken CrossToken
	err = json.Unmarshal(tf, &gCrossToken)
	if err != nil {
		return sdk.Error("unmarshal CrossToken error")
	}
	gCrossToken.TotalSupply -= value
	gCrossTokenJson, err := json.Marshal(gCrossToken)
	if err != nil {
		return sdk.Error("CrossToken json error")
	}
	//更新总跨链资产信息
	if err := sdk.Instance.PutStateFromKeyByte("globalCrossToken", gCrossTokenJson); err != nil {
		return sdk.Error("put state error")
	}
	sdk.Instance.EmitEvent("burn", []string{"burn from address:" + address + " " + valueString})
	return sdk.Success(nil)
}

// 获取客户端地址
func (a *Assets) getAddress() pb.Response {
	senderpk, _ := sdk.Instance.Sender()
	return sdk.Success([]byte(senderpk))
}

// 检查是否是合法地址调用burn和mint方法
func __checkValidtAddress(address string) bool {
	creator, _ := sdk.Instance.GetCreatorPk()
	if creator == address {
		return true
	}
	sender, _ := sdk.Instance.Sender()
	key := "contract:" + sender
	// 从链上找指定合约地址信息
	res, err := sdk.Instance.GetStateFromKey(key)
	if err != nil {
		return false
	}
	if len(res) == 0 {
		return false
	}
	return res == "true"
}

// 注册合约地址，只有他们有权限调用mint和burn
func (a *Assets) registContractAddress() pb.Response {
	contractAddress, _ := sdk.Instance.Sender()
	key := "contract:" + contractAddress
	// 将注册的合约地址存到链上
	if err := sdk.Instance.PutStateFromKey(key, "true"); err != nil {
		return sdk.Error("put state error")
	}
	return sdk.Success(nil)
}

// 测试函数，给每个调用的用户100000000000000
func (a *Assets) reset() pb.Response {
	sender, err := sdk.Instance.Origin()
	if err != nil {
		return sdk.Error("get Origin error: " + err.Error())
	}
	if err := sdk.Instance.PutStateFromKey(sender, "100000000000000"); err != nil {
		return sdk.Error("putStateFromKey error: " + err.Error())
	}
	return sdk.Success([]byte("reset success"))
}

// main
func main() {
	err := sandbox.Start(new(Assets))
	if err != nil {
		log.Fatal(err)
	}
}
