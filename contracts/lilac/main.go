/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author longbo,jch
@Description 该文件中实现了Lilac合约,基于哈希时间锁完优化方案Lilac成原子交换
*/

package main

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"log"
	"strconv"

	pb "chainmaker.org/chainmaker/contract-sdk-go/v2/pb/protogo"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sandbox"
	"chainmaker.org/chainmaker/contract-sdk-go/v2/sdk"
)

// Proposal defines lilac proposal segments
type Proposal struct {
	Sender    string   `json:"sender"`    // sender account Address
	Receiver  string   `json:"receiver"`  // receiver account Address
	Amount    int64    `json:"amount"`    // transfer/lock token amounts
	PosalTime int64    `json:"posaltime"` // proposal generation time
	TimeLock  int64    `json:"timelock"`  // timelock
	HashLock  []string `json:"hashlock"`  // hashlock
	Preimage  []string `json:"preimage"`  // key to make hashmatch and unlock hashlock
	// BlockDepth int64  `json:"blockdepth"`
	Locked     bool `json:"locked"`     // proposal locked status
	Unlocked   bool `json:"unlocked"`   // proposal unlocked status
	Rolledback bool `json:"rolledback"` // propsoal rolledback status
}

// Lilac defines operate object
type LILAC struct {
}

// InitContract use to init contract
func (l *LILAC) InitContract() pb.Response {
	return sdk.Success([]byte("Init success"))
}

// UpgradeContract use to upgrade contract
func (l *LILAC) UpgradeContract() pb.Response {
	return sdk.Success([]byte("Upgrade success"))
}

// InvokeContract use to select specific method
func (l *LILAC) InvokeContract(method string) pb.Response {
	// according method segment to select contract functions
	switch method {
	case "newProposal":
		return l.newProposal()
	case "getProposal":
		return l.getProposal()
	case "withdraw":
		return l.withdraw()
	case "refund":
		return l.refund()
	default:
		return sdk.Error("invalid method")
	}
}

// newProposal initiate a proposal with receiver, amount, hashlock list
func (l *LILAC) newProposal() pb.Response {
	params := sdk.Instance.GetArgs()
	sender, _ := sdk.Instance.Sender()
	receiver := string(params["receiver"])
	amountString := string(params["amount"])
	amount, err := strconv.ParseInt(amountString, 10, 64)
	// amount must be greater than or equal to zero
	if amount < 0 {
		return sdk.Error("invalid amount")
	}
	// parse amount segement from client fail
	if err != nil {
		return sdk.Error("amount error")
	}
	// get current chainmaker timestamp
	currTimeStamp, err := sdk.Instance.GetTxTimeStamp()
	if err != nil {
		return sdk.Error("getTxTimeStamp error")
	}
	currTime, err := strconv.ParseInt(currTimeStamp, 10, 64)
	if err != nil {
		return sdk.Error("timelock error")
	}
	var hashlock []string
	hashlockjson := params["hashlock"]
	err = json.Unmarshal(hashlockjson, &hashlock)
	if err != nil {
		return sdk.Error("hashlock error")
	}
	timelockString := string(params["timelock"])
	timelock, err := strconv.ParseInt(timelockString, 10, 64)
	if err != nil {
		return sdk.Error("getTimeLock error")
	}

	// initiate a new proposal object
	var senderProposal = &Proposal{
		Sender:     sender,
		Receiver:   receiver,
		Amount:     amount,
		PosalTime:  currTime,
		HashLock:   hashlock,
		TimeLock:   timelock,
		Preimage:   []string{},
		Locked:     false,
		Unlocked:   false,
		Rolledback: false,
	}

	// cross contract call "assets" function "getAddress" to get lilac contract account address
	args := map[string][]byte{}
	resp := sdk.Instance.CallContract("assets", "getAddress", args)
	// cross contract call fail, return error info
	if resp.Status == sdk.ERROR {
		return sdk.Error(resp.GetMessage())
	}
	// extract contract account Address
	lilacAddress := resp.Payload
	// throw an event of topic:"lilacAddress"
	sdk.Instance.EmitEvent("lilacAddress", []string{string(lilacAddress)})

	// cross contract call "assets" function "approve" to approve lilac account use sender account amount tokens
	args = map[string][]byte{
		"spenderAddress": []byte(lilacAddress),
		"value":          []byte(amountString),
	}
	resp = sdk.Instance.CallContract("assets", "approve", args)
	// cross contract call "approve" function fail, return error info
	if resp.Status == sdk.ERROR {
		return sdk.Error(resp.GetMessage())
	}
	// throw an event of topic:"approve"
	sdk.Instance.EmitEvent("approve", []string{string(resp.Payload)})

	// cross contract call "assets" function "transferfrom" to transfer amount tokens from sender account to lilac account
	args = map[string][]byte{
		"senderAddress": []byte(sender),
		"toAddress":     lilacAddress,
		"value":         []byte(amountString),
	}
	resp = sdk.Instance.CallContract("assets", "transferFrom", args)
	// cross contract call "transferFrom" function fail, return error info
	if resp.Status == sdk.ERROR {
		return sdk.Error(resp.GetMessage())
	}
	// throw an event of topic:"transferfrom"
	sdk.Instance.EmitEvent("transferfrom", []string{string(resp.Payload)})

	// update senderproposal's locked state
	senderProposal.Locked = true

	// generate only proposalID by proposal content and putstate to chain
	proposalJson, err := json.Marshal(senderProposal)
	if err != nil {
		return sdk.Error("senderProposal json error")
	}
	proposalID := mySha256(proposalJson)
	// update chain state by key-value:<propsalID, proposalData>
	if err := sdk.Instance.PutStateFromKeyByte(proposalID, proposalJson); err != nil {
		// update chain state error
		return sdk.Error("put newProposal error")
	}
	// throw an event of topic:"newProposal"
	sdk.Instance.EmitEvent("newProposal", []string{proposalID, string(proposalJson)})
	return sdk.Success([]byte(proposalID))
}

// getProposal gets proposal data by proposalID
func (l *LILAC) getProposal() pb.Response {
	params := sdk.Instance.GetArgs()
	proposalID := string(params["proposalID"])
	proposalJson, err := sdk.Instance.GetStateFromKeyByte(proposalID)
	// get proposal content with proposalID from client error
	if err != nil {
		return sdk.Error("get Proposal error")
	}
	// throw an event of topic:"getProposal"
	sdk.Instance.EmitEvent("getProposal", []string{string(proposalJson)})
	return sdk.Success(proposalJson)
}

// withdraw transfer target proposal's token to proposal's receiver by proposalID, preimage list
func (l *LILAC) withdraw() pb.Response {
	params := sdk.Instance.GetArgs()
	proposalID := string(params["proposalID"])
	proposalJson, err := sdk.Instance.GetStateFromKeyByte(proposalID)
	// get proposal content with proposalID from client error
	if err != nil {
		return sdk.Error("refer" + proposalID + "error")
	}
	var proposalData Proposal
	err = json.Unmarshal(proposalJson, &proposalData)
	if err != nil {
		return sdk.Error("proposal unmarshal error")
	}

	// check sender euqal to receiver
	sender, err := sdk.Instance.Sender()
	if err != nil {
		return sdk.Error("get sender error: " + err.Error())
	}
	if proposalData.Receiver != sender {
		return sdk.Error("permission deny, only receiver")
	}
	// check proposal locked state
	if !proposalData.Locked {
		return sdk.Error("proposal \"locked\" state error")
	}
	// check proposal unlocked state
	if proposalData.Unlocked {
		return sdk.Error("proposal \"unlocked\" state error")
	}
	// check proposal rolledback state
	if proposalData.Rolledback {
		return sdk.Error("proposal \"rolledback\" state error")
	}
	// check timelock state
	// get current chainmaker timestamp
	currTimeStamp, err := sdk.Instance.GetTxTimeStamp()
	if err != nil {
		return sdk.Error("getTxTimeStamp error")
	}
	currTime, err := strconv.ParseInt(currTimeStamp, 10, 64)
	if err != nil {
		return sdk.Error("timelock error")
	}
	proposalTime := proposalData.PosalTime
	timelock := proposalData.TimeLock
	// check if timestamp out of timelock
	if (currTime - proposalTime) > timelock {
		return sdk.Error("timelock out of date")
	}
	// unlock hashlock
	proposalHash := proposalData.HashLock
	preimagejson := params["preimage"]
	var preimage []string
	err = json.Unmarshal(preimagejson, &preimage)
	if err != nil {
		return sdk.Error("preimage unmarshal error")
	}
	// check if preimage list correct
	hashsize := len(proposalHash)
	if hashsize != len(preimage) {
		return sdk.Error("preimage length error")
	}
	// traverse proposal hash list sequentially with preimage list
	for i := 0; i < hashsize; i++ {
		// check that each preimage matches the corresponding hash
		if !hashMatch(proposalHash[i], preimage[i]) {
			return sdk.Error("hash not matched")
		}
	}
	// cross contract call "assets" function "transfer" to transfer amount tokens from lilac account to receiver account
	receiverAddress := proposalData.Receiver
	amount := proposalData.Amount
	amountString := strconv.FormatInt(amount, 10)
	args := map[string][]byte{
		"toAddress": []byte(receiverAddress),
		"value":     []byte(amountString),
	}
	resp := sdk.Instance.CallContract("assets", "transfer", args)
	// cross contract call fail, return error info
	if resp.Status == sdk.ERROR {
		return sdk.Error(resp.GetMessage())
	}
	// update proposal's state
	proposalData.Locked = false
	proposalData.Unlocked = true
	proposalData.Amount = 0
	proposalData.Preimage = preimage
	proposalJson, err = json.Marshal(proposalData)
	if err != nil {
		return sdk.Error("proposal marshal error")
	}
	// update chain state by key-value:<propsalID, proposalData>
	if err := sdk.Instance.PutStateFromKeyByte(proposalID, proposalJson); err != nil {
		// update chain state error
		return sdk.Error("update proposal state error")
	}
	// throw an event of topic:"withdraw"
	// edit by rdc for subscribe
	// sdk.Instance.EmitEvent("withdraw", []string{string(resp.Payload)})
	sdk.Instance.EmitEvent("withdraw", []string{proposalID, string(proposalJson)})
	return sdk.Success(proposalJson)
}

// refund proposal's token to sender account by proposalID
func (l *LILAC) refund() pb.Response {
	params := sdk.Instance.GetArgs()
	proposalID := string(params["proposalID"])
	proposalJson, err := sdk.Instance.GetStateFromKeyByte(proposalID)
	// get proposal content with proposalID from client error
	if err != nil {
		return sdk.Error("refer" + proposalID + "error")
	}
	var proposalData Proposal
	err = json.Unmarshal(proposalJson, &proposalData)
	if err != nil {
		return sdk.Error("proposal unmarshal error")
	}

	// check sender euqal to receiver
	sender, err := sdk.Instance.Sender()
	if err != nil {
		return sdk.Error("get sender error: " + err.Error())
	}
	if proposalData.Sender != sender {
		return sdk.Error("permission deny, only sender")
	}
	// check proposal locked state
	if !proposalData.Locked {
		return sdk.Error("proposal \"locked\" state error")
	}
	// check proposal unlocked state
	if proposalData.Unlocked {
		return sdk.Error("proposal \"unlocked\" state error")
	}
	// check proposal rolledback state
	if proposalData.Rolledback {
		return sdk.Error("proposal's amount has transferred")
	}
	// check timelock state
	// get current chainmaker timestamp
	currTimeStamp, err := sdk.Instance.GetTxTimeStamp()
	if err != nil {
		return sdk.Error("getTxTimeStamp error")
	}
	currTime, err := strconv.ParseInt(currTimeStamp, 10, 64)
	if err != nil {
		return sdk.Error("timelock error")
	}
	proposalTime := proposalData.PosalTime
	timelock := proposalData.TimeLock
	// check if timestamp out of timelock
	if (currTime - proposalTime) < timelock {
		return sdk.Error("timelock is still valid")
	}
	// cross contract call "assets" function "transfer" to transfer amount tokens from lilac account to sender account
	contractName := "assets"
	method := "transfer"
	toAddress := "toAddress"
	value := "value"
	receiverAddress := proposalData.Sender
	amount := proposalData.Amount
	amountString := strconv.FormatInt(amount, 10)
	args := map[string][]byte{
		toAddress: []byte(receiverAddress),
		value:     []byte(amountString),
	}
	resp := sdk.Instance.CallContract(contractName, method, args)
	// cross contract call fail, return error info
	if resp.Status == sdk.ERROR {
		return sdk.Error(resp.GetMessage())
	}
	// update proposal's state
	proposalData.Locked = false
	proposalData.Unlocked = true
	proposalData.Rolledback = true
	proposalData.Amount = 0
	proposalJson, err = json.Marshal(proposalData)
	if err != nil {
		return sdk.Error("proposal marshal error")
	}
	// update chain state by key-value:<propsalID, proposalData>
	if err := sdk.Instance.PutStateFromKeyByte(proposalID, proposalJson); err != nil {
		// update chain state error
		return sdk.Error("update proposal state error")
	}
	// throw an event of topic:"refund"
	sdk.Instance.EmitEvent("refund", []string{string(resp.Payload)})
	return sdk.Success(proposalJson)
}

// contract built-in function to generate proposalID with proposalData
func mySha256(input []byte) string {
	// generate sha256 object
	hash := sha256.New()
	hash.Write(input)
	return fmt.Sprintf("%x", hash.Sum(nil))
}

// contract built-in function to test the realtionship of hashlock & preimage
func hashMatch(hash, preimage string) bool {
	// check if preimage is hash's key
	return hash == string(mySha256([]byte(preimage)))
}

// main
func main() {
	err := sandbox.Start(new(LILAC))
	if err != nil {
		log.Fatal(err)
	}
}
