/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 基于erc20标准接口实现的资产合约
*/
package main

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"strings"

	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/hyperledger/fabric-protos-go/peer"
)

// 临时保存当前跨链approve资产
var mpApprove = map[string]string{}

type Assets struct {
	msID string // 合约唯一ID
}

// 跨链资产数据结构
type CrossToken struct {
	TotalSupply int64  `json:"total_supply"` //token总量
	Name        string `json:"name"`         //token名称
	Version     string `json:"version"`      //token版本
}

// 生成新的跨链资产
func NewCrossToken(totalSupply int64, name, version string) *CrossToken {
	return &CrossToken{
		TotalSupply: totalSupply,
		Name:        name,
		Version:     version,
	}
}

// 初始化合约
// @param totalSupply token总量
// @param name token名称
// @param version token版本
// @param users 初始化用户资产，每个用户初始化1000
func (a *Assets) Init(stub shim.ChaincodeStubInterface) peer.Response {
	args := stub.GetStringArgs()
	if len(args) != 4 {
		return shim.Error(fmt.Sprintf("length of args error, expect 4, get %d", len(args)))
	}

	totalSupplyString := args[0]
	name, version := args[1], args[2]
	// users是使用','分割的字符串
	users := args[3]
	if len(totalSupplyString) == 0 {
		return shim.Error("need totalSupply")
	}
	totalSupply, err := strconv.ParseInt(totalSupplyString, 10, 64)
	//token总量不能 < 0
	if totalSupply < 0 {
		return shim.Error("totalsupply < 0")
	}
	if err != nil {
		return shim.Error("parse totalsupply error")
	}
	CrossToken := NewCrossToken(totalSupply, name, version)
	CrossTokenJson, err := json.Marshal(CrossToken)
	if err != nil {
		return shim.Error("CrossToken json error")
	}
	//更新跨链资产信息
	if err := stub.PutState("globalCrossToken", CrossTokenJson); err != nil {
		return shim.Error("put state error")
	}

	// 抛出事件
	eventData, err := json.Marshal([]string{totalSupplyString, name, version})
	if err != nil {
		return shim.Error("json.Marshal eventData error")
	}
	if err := stub.SetEvent("CrossTokenInit", eventData); err != nil {
		return shim.Error(fmt.Sprintf("unable set CrossTokenInit event, error: %v", err))
	}

	//初始化用户资产
	userList := strings.Split(users, ",")
	//每人初始化1000
	for _, user := range userList {
		stub.PutState(user, []byte("1000"))
	}
	//抛出CrossTokenInit事件
	eventData, err = json.Marshal(userList)
	if err != nil {
		return shim.Error("json.Marshal eventData error")
	}
	if err := stub.SetEvent("InitUser", eventData); err != nil {
		return shim.Error(fmt.Sprintf("unable set InitUser event, error: %v", err))
	}

	// 设置当前合约ID
	a.getID()

	return shim.Success([]byte("Init Success"))
}

func (a *Assets) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	functionName, args := stub.GetFunctionAndParameters()
	switch functionName {
	case "CrossTokenInfo":
		return a.CrossTokenInfo(stub, args)
	case "approve":
		return a.approve(stub, args)
	case "allowance":
		return a.allowance(stub, args)
	case "transfer":
		return a.transfer(stub, args)
	case "balanceOf":
		return a.balanceOf(stub, args)
	case "transferFrom":
		return a.transferFrom(stub, args)
	// case "mint":
	// 	return a.mint(stub, args)
	// case "burn":
	// 	return a.burn(stub, args)
	case "getAddress":
		return a.getAddress(stub, args)
	// case "registContractAddress":
	// 	return a.registContractAddress(stub, args)
	case "reset":
		return a.reset(stub, args)
	default:
		return shim.Error("invalid method")
	}
}

// get htlc chaincode id
func (h *Assets) getID() string {
	if h.msID == "" {
		// 设置当前合约ID
		h.msID = mySha256([]byte("asstes"))
	}
	return h.msID
}

// 返回整个跨链资产信息
// @return CrossToken的json序列化结果
func (a *Assets) CrossTokenInfo(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	tf, err := stub.GetState("globalCrossToken")
	if err != nil {
		return shim.Error("get CrossToken info error")
	}
	return shim.Success(tf)
}

// 获取授权key
// 绑定owner和spender关系
func makeAllowanceKey(owner, spender string) string {
	return "allow__" + owner + "__to__" + spender
}

// 授权相关函数，当前账户允许从指定账户转账
// @param spenderAddress 消费者
// @param value 授权金额
func (a *Assets) approve(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	if len(args) != 2 {
		return shim.Error(fmt.Sprintf("length of args error, expect 2, get %d", len(args)))
	}

	ownerByte, err := getSenderCertificateHash(stub)
	if err != nil {
		return shim.Error("GetCreator error: " + err.Error())
	}
	owner := string(ownerByte)

	spender := args[0]

	key := makeAllowanceKey(owner, spender)

	valueString := args[1]
	value, err := strconv.ParseInt(valueString, 10, 64)
	if value < 0 || err != nil {
		return shim.Error("parse valueString error")
	}

	// 临时保存
	mpApprove[key] = valueString
	//更新授权关系进块
	if err := stub.PutState(key, []byte(valueString)); err != nil {
		return shim.Error("put state error")
	}
	// 抛出approve事件
	stub.SetEvent("approve", []byte(key+":"+valueString))
	return shim.Success([]byte(key + " " + valueString))
}

// 授权相关函数，当前账户允许从指定账户转账的金额数
// @param ownerAddress 原token持有者
// @param spenderAddress 授权消费者
// @return 授权金额余量
func (a *Assets) allowance(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	if len(args) != 2 {
		return shim.Error(fmt.Sprintf("length of args error, expect 2, get %d", len(args)))
	}

	owner, spender := args[0], args[1]
	key := makeAllowanceKey(owner, spender)
	valueByte, err := stub.GetState(key)
	if err != nil {
		return shim.Error(fmt.Sprintf("allowance error: %v", err))
	}
	valueString := string(valueByte)
	if len(valueString) == 0 {
		valueString = "0"
	}
	return shim.Success([]byte(key + ":" + valueString))
}

// 向指定地址发送一定数量的CrossToken
// @param toAddress 目的地址
// @param value 金额数量
func (a *Assets) transfer(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	//参数检查
	if len(args) != 2 {
		return shim.Error(fmt.Sprintf("length of args error, expect 2, get %d", len(args)))
	}

	senderByte, err := getSenderCertificateHash(stub)
	if err != nil {
		return shim.Error(fmt.Sprintf("GetCreator error: %v", err))
	}
	senderpk := string(senderByte)

	toAddress := args[0]
	if len(toAddress) == 0 {
		return shim.Error("get toAddress error")
	}

	valueString := args[1]
	value, err := strconv.ParseInt(valueString, 10, 64)
	if value < 0 || err != nil {
		return shim.Error("value error, get " + valueString)
	}

	//如果目的地址也是自己,直接返回
	if senderpk == toAddress {
		// 抛出transfer事件
		if err := stub.SetEvent("transfer", []byte(senderpk+" send to self "+valueString)); err != nil {
			return shim.Error("unable set transfer to self envent, error: " + err.Error())
		}
	}

	senderBalanceByte, err := stub.GetState(senderpk)
	if err != nil || len(senderBalanceByte) == 0 {
		return shim.Error("get state error")
	}
	senderBalanceString := string(senderBalanceByte)

	senderBalance, err := strconv.ParseInt(senderBalanceString, 10, 64)
	if senderBalance < 0 || err != nil {
		return shim.Error("get sender balance error")
	}

	toBalanceByte, err := stub.GetState(toAddress)
	if err != nil {
		return shim.Error("get state error")
	}
	toBalanceString := string(toBalanceByte)

	toBalance := int64(0)
	if len(toBalanceString) != 0 {
		toBalance, err = strconv.ParseInt(toBalanceString, 10, 64)
		if err != nil {
			return shim.Error("parseInt toBalanceString: " + toBalanceString + " error: " + err.Error())
		}
	}

	if senderBalance < value {
		return shim.Error("sender balance not enough")
	}

	senderBalance -= value
	toBalance += value
	senderBalanceS := strconv.FormatInt(senderBalance, 10)
	toBalanceS := strconv.FormatInt(toBalance, 10)
	// 更新各账户余额
	if err := stub.PutState(senderpk, []byte(senderBalanceS)); err != nil {
		return shim.Error("put sender balance error")
	}
	if err := stub.PutState(toAddress, []byte(toBalanceS)); err != nil {
		return shim.Error("put sender balance error")
	}

	// 抛出transfer事件
	if err := stub.SetEvent("transfer", []byte(senderpk+" to "+toAddress+" : "+valueString)); err != nil {
		return shim.Error("unable set transfer envent, error: " + err.Error())
	}

	return shim.Success(nil)
}

// 由指定发送地址向指定地址发送一定数量CrossToken
// @param owner token本来的拥有者
// @param senderAddress 被授权的人地址
// @param toAddress 发给目的地址
// @param value 金额数量
func (a *Assets) transferFrom(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	//参数检查
	if len(args) != 4 {
		return shim.Error(fmt.Sprintf("length of args error, expect 4, get %d", len(args)))
	}

	// owner授权sender发送valueString给to这个人
	owner, sender, to, valueString := args[0], args[1], args[2], args[3]

	value, err := strconv.ParseInt(valueString, 10, 64)
	if err != nil || value < 0 {
		return shim.Error("parse value error, get " + valueString)
	}
	if owner == to {
		if err := stub.SetEvent("transferFrom", []byte(owner+":"+to)); err != nil {
			return shim.Error("set transferFrom event error: " + err.Error())
		}
		return shim.Success(nil)
	}

	//检查授权情况
	key := makeAllowanceKey(owner, sender)
	ownerBalanceByte, err := stub.GetState(owner)
	if err != nil {
		return shim.Error("getOwnerBalance state error")
	}
	ownerBalanceString := string(ownerBalanceByte)
	ownerBalance, err := strconv.ParseInt(ownerBalanceString, 10, 64)
	if ownerBalance < 0 || err != nil {
		return shim.Error("ownerBalance need > 0")
	}
	allowanceValueByte, err := stub.GetState(key)
	if err != nil {
		return shim.Error("get state: " + key + ", error: " + err.Error())
	}

	allowanceValueString := string(allowanceValueByte)
	if len(allowanceValueString) == 0 || allowanceValueString == "0" {
		allowanceValueString = mpApprove[key]
		if allowanceValueString == "0" {
			return shim.Error("not allowance: " + key + " value: " + allowanceValueString)
		}
	}
	allowanceValue, err := strconv.ParseInt(allowanceValueString, 10, 64)
	if allowanceValue < 0 || err != nil {
		return shim.Error("allowanceValue error: " + err.Error())
	}
	if allowanceValue < value {
		return shim.Error("allowanceValue " + allowanceValueString + " should >= value " + valueString)
	}

	toBalanceByte, err := stub.GetState(to)
	if err != nil {
		return shim.Error("get state error")
	}
	toBalanceString := string(toBalanceByte)
	toBalance := int64(0)
	if len(toBalanceString) != 0 {
		toBalance, err = strconv.ParseInt(toBalanceString, 10, 64)
		if err != nil {
			return shim.Error("parseInt toBalanceString error: " + err.Error() + " " + toBalanceString)
		}
	}
	if ownerBalance < value {
		return shim.Error("ownerBalance should > value")
	}
	//转账操作
	ownerBalance -= value
	toBalance += value
	allowanceValue -= value
	ownerBalanceS := strconv.FormatInt(ownerBalance, 10)
	toBalanceS := strconv.FormatInt(toBalance, 10)
	allowanceValueS := strconv.FormatInt(allowanceValue, 10)

	//更新代理转账结果
	stub.PutState(owner, []byte(ownerBalanceS))
	stub.PutState(to, []byte(toBalanceS))
	stub.PutState(key, []byte(allowanceValueS))

	// 清除临时转账
	mpApprove[key] = ""

	//抛出transferfrom事件
	if err := stub.SetEvent("transferFrom", []byte(sender+" from "+owner+" to "+to+" : "+valueString)); err != nil {
		return shim.Error("setEvent transferFrom error: " + err.Error())
	}
	return shim.Success(nil)
}

// 获取指定地址的余额
// @params address 指定地址
func (a *Assets) balanceOf(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	//参数检查
	if len(args) != 1 {
		return shim.Error(fmt.Sprintf("length of args error, expect 1, get %d", len(args)))
	}

	address := args[0]
	//查询链上指定地址的状态信息
	balanceByte, err := stub.GetState(address)
	if err != nil {
		return shim.Error("get balance error: " + err.Error())
	}
	return shim.Success(balanceByte)
}

// // 发行货币到指定地址
// // 只有合约和creator有权调用
// // @params:address
// // @params:value
// func (a *Assets) mint(stub shim.ChaincodeStubInterface, args []string) peer.Response {
// 	//参数检查
// 	if len(args) != 2 {
// 		return shim.Error(fmt.Sprintf("length of args error, expect 2, get %d", len(args)))
// 	}

// 	senderPK, _ := getSenderCertificateHash(stub)
// 	if !__checkValidtAddress(senderPK) {
// 		return sdk.Error("have no privillege to invoke")
// 	}
// 	params := sdk.Instance.GetArgs()
// 	address, valueString := string(params["address"]), string(params["value"])
// 	value, err := strconv.ParseInt(valueString, 10, 64)
// 	if err != nil || value < 0 {
// 		return sdk.Error("parse value error")
// 	}
// 	//获取address的余额
// 	balanceString, err := sdk.Instance.GetStateFromKey(address)
// 	if err != nil {
// 		return sdk.Error("get balance error")
// 	}
// 	balance := int64(0)
// 	if len(balanceString) != 0 {
// 		balance, _ = strconv.ParseInt(balanceString, 10, 64)
// 	}
// 	//更新余额
// 	newBalance := balance + value
// 	newBalances := strconv.FormatInt(newBalance, 10)
// 	sdk.Instance.PutStateFromKey(address, newBalances)
// 	//更新token总量
// 	tf, err := sdk.Instance.GetStateFromKeyByte("globalCrossToken")
// 	if err != nil {
// 		return sdk.Error("getstate error")
// 	}
// 	var gCrossToken CrossToken
// 	err = json.Unmarshal(tf, &gCrossToken)
// 	if err != nil {
// 		return sdk.Error("unmarshal CrossToken error")
// 	}
// 	gCrossToken.TotalSupply += value
// 	gCrossTokenJson, err := json.Marshal(gCrossToken)
// 	if err != nil {
// 		return sdk.Error("CrossToken json error")
// 	}
// 	//更新总跨链资产信息
// 	if err := sdk.Instance.PutStateFromKeyByte("globalCrossToken", gCrossTokenJson); err != nil {
// 		return sdk.Error("put state error")
// 	}
// 	sdk.Instance.EmitEvent("mint", []string{"mint to address:" + address + " " + valueString})
// 	return sdk.Success(nil)
// }

// // 从指定地址销毁货币
// // 只有合约和creator有权调用
// // @params:address
// // @params:value
// func (a *Assets) burn(stub shim.ChaincodeStubInterface, args []string) peer.Response {
// 	senderPK, _ := sdk.Instance.GetSenderPk()
// 	if !__checkValidtAddress(senderPK) {
// 		return sdk.Error("have no privillege to invoke")
// 	}
// 	params := sdk.Instance.GetArgs()
// 	address, valueString := string(params["address"]), string(params["value"])
// 	value, err := strconv.ParseInt(valueString, 10, 64)
// 	if err != nil || value < 0 {
// 		return sdk.Error("parse value error")
// 	}
// 	//获取address的余额
// 	balanceString, err := sdk.Instance.GetStateFromKey(address)
// 	if err != nil {
// 		return sdk.Error("get balance error")
// 	}
// 	balance := int64(0)
// 	if len(balanceString) != 0 {
// 		balance, _ = strconv.ParseInt(balanceString, 10, 64)
// 	}
// 	//余额检查
// 	if balance < value {
// 		return sdk.Error("not enough CrossToken to burn")
// 	}
// 	//更新余额
// 	newBalance := balance - value
// 	newBalances := strconv.FormatInt(newBalance, 10)
// 	sdk.Instance.PutStateFromKey(address, newBalances)
// 	//更新token总量
// 	tf, err := sdk.Instance.GetStateFromKeyByte("globalCrossToken")
// 	if err != nil {
// 		return sdk.Error("getstate error")
// 	}
// 	var gCrossToken CrossToken
// 	err = json.Unmarshal(tf, &gCrossToken)
// 	if err != nil {
// 		return sdk.Error("unmarshal CrossToken error")
// 	}
// 	gCrossToken.TotalSupply -= value
// 	gCrossTokenJson, err := json.Marshal(gCrossToken)
// 	if err != nil {
// 		return sdk.Error("CrossToken json error")
// 	}
// 	//更新总跨链资产信息
// 	if err := sdk.Instance.PutStateFromKeyByte("globalCrossToken", gCrossTokenJson); err != nil {
// 		return sdk.Error("put state error")
// 	}
// 	sdk.Instance.EmitEvent("burn", []string{"burn from address:" + address + " " + valueString})
// 	return sdk.Success(nil)
// }

// 计算hash
func mySha256(input []byte) string {
	// generate sha256 object
	hash := sha256.New()
	hash.Write(input)
	return fmt.Sprintf("%x", hash.Sum(nil))
}

// 获取发起人地址
func getSenderCertificateHash(stub shim.ChaincodeStubInterface) ([]byte, error) {
	cert, err := stub.GetCreator()
	if err != nil {
		return nil, fmt.Errorf("GetCreator error: " + err.Error())
	}
	return []byte(mySha256(cert)), nil
}

// 获取客户端地址
func (a *Assets) getAddress(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	senderpk, err := getSenderCertificateHash(stub)
	if err != nil {
		return shim.Error("GetCreator error: " + err.Error())
	}
	return shim.Success(senderpk)
}

// // 检查是否是合法地址调用burn和mint方法
// func __checkValidtAddress(address string) bool {
// 	creator, _ := sdk.Instance.GetCreatorPk()
// 	if creator == address {
// 		return true
// 	}
// 	sender, _ := sdk.Instance.Sender()
// 	key := "contract:" + sender
// 	// 从链上找指定合约地址信息
// 	res, err := sdk.Instance.GetStateFromKey(key)
// 	if err != nil {
// 		return false
// 	}
// 	if len(res) == 0 {
// 		return false
// 	}
// 	return res == "true"
// }

// // 注册合约地址，只有他们有权限调用mint和burn
// func (a *Assets) registContractAddress(stub shim.ChaincodeStubInterface, args []string) peer.Response {
// 	contractAddress, _ := sdk.Instance.Sender()
// 	key := "contract:" + contractAddress
// 	// 将注册的合约地址存到链上
// 	if err := sdk.Instance.PutStateFromKey(key, "true"); err != nil {
// 		return sdk.Error("put state error")
// 	}
// 	return sdk.Success(nil)
// }

//测试函数，给每个调用的用户100000000000000
func (a *Assets) reset(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	senderByte, err := getSenderCertificateHash(stub)
	if err != nil {
		return shim.Error("GetCreator error: " + err.Error())
	}
	sender := string(senderByte)
	if err := stub.PutState(sender, []byte("100000000000000")); err != nil {
		return shim.Error("putStateFromKey error: " + err.Error())
	}
	return shim.Success([]byte("reset success"))
}

// main
func main() {
	err := shim.Start(new(Assets))
	if err != nil {
		log.Fatal("start assets contracts error: ", err)
	}
}
