/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 基于哈希时间锁的原子互换合约
*/
package main

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"log"
	"strconv"

	"github.com/hyperledger/fabric-chaincode-go/shim"
	"github.com/hyperledger/fabric-protos-go/peer"
)

// Proposal defines htlc proposal segments
type Proposal struct {
	Sender    string `json:"sender"`    // sender account Address
	Receiver  string `json:"receiver"`  // receiver account Address
	Amount    int64  `json:"amount"`    // transfer/lock token amounts
	PosalTime int64  `json:"posaltime"` // proposal generation time
	TimeLock  int64  `json:"timelock"`  // timelock
	HashLock  string `json:"hashlock"`  // hashlock
	Preimage  string `json:"preimage"`  // key to make hashmatch and unlock hashlock
	// BlockDepth int64  `json:"blockdepth"`
	Locked     bool `json:"locked"`     // proposal locked status
	Unlocked   bool `json:"unlocked"`   // proposal unlocked status
	Rolledback bool `json:"rolledback"` // propsoal rolledback status
}

// HTLC defines operate object
type HTLC struct {
	msID string
}

// InitContract use to init contract
func (h *HTLC) Init(stub shim.ChaincodeStubInterface) peer.Response {
	return shim.Success([]byte("Init success"))
}

// InvokeContract use to select specific method
func (h *HTLC) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	functionName, args := stub.GetFunctionAndParameters()
	// according method segment to select contract functions
	switch functionName {
	case "newProposal":
		return h.newProposal(stub, args)
	case "getProposal":
		return h.getProposal(stub, args)
	case "withdraw":
		return h.withdraw(stub, args)
	case "refund":
		return h.refund(stub, args)
	default:
		return shim.Error("invalid method")
	}
}

// get htlc chaincode id
func (h *HTLC) getID() string {
	if h.msID == "" {
		// 设置当前合约ID
		h.msID = mySha256([]byte("htlc"))
	}
	return h.msID
}

// newProposal initiate a proposal with receiver, amount, hashlock
//@params receiver:接收方地址
//@params amount:锁定金额
//@params hashlock:哈希锁
//@params timelock:时间锁(锁定时间)
func (h *HTLC) newProposal(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	if len(args) != 4 {
		return shim.Error(fmt.Sprintf("length of args error, expect 4, get %d", len(args)))
	}

	senderByte, err := getSenderCertificateHash(stub)
	if err != nil {
		return shim.Error("GetCreator error: " + err.Error())
	}
	sender := string(senderByte)

	receiver := args[0]
	amountString := args[1]
	amount, err := strconv.ParseInt(amountString, 10, 64)
	// parse amount segement from client fail
	if err != nil {
		return shim.Error("amount error: " + amountString)
	}
	// amount must be greater than or equal to zero
	if amount < 0 {
		return shim.Error("invalid amount: " + amountString)
	}
	// get current timestamp
	currTimeStamp, err := stub.GetTxTimestamp()
	if err != nil {
		shim.Error("getTxTimeStamp error: " + err.Error())
	}

	currTime := currTimeStamp.GetSeconds()

	hashlock := args[2]
	timelockString := args[3]
	timelock, err := strconv.ParseInt(timelockString, 10, 64)
	if err != nil {
		shim.Error("timelockString error: " + timelockString)
	}

	// initiate a new proposal object
	var senderProposal = &Proposal{
		Sender:     sender,
		Receiver:   receiver,
		Amount:     amount,
		PosalTime:  currTime,
		HashLock:   hashlock,
		TimeLock:   timelock,
		Preimage:   "null",
		Locked:     false,
		Unlocked:   false,
		Rolledback: false,
	}

	// extract contract account Address
	htlcAddress := []byte(h.getID())
	// throw an event of topic:"htlcAddress"
	if err := stub.SetEvent("htlcAddress", htlcAddress); err != nil {
		return shim.Error("setEvent htlcAddress error: " + err.Error())
	}

	// cross contract call "assets" function "approve" to approve htlc account use sender account amount tokens
	contractName := "assets"
	method := "approve"
	assets_args := [][]byte{[]byte(method), htlcAddress, []byte(amountString)}
	resp := stub.InvokeChaincode(contractName, assets_args, stub.GetChannelID())
	// cross contract call "approve" function fail, return error info
	if resp.Status == shim.ERROR {
		return shim.Error("invoke approve error: " + string(resp.GetMessage()) + " htlcAddress:" + h.msID)
	}

	approve_result := string(resp.Payload)

	// throw an event of topic:"approve"
	if err := stub.SetEvent("approve", resp.Payload); err != nil {
		return shim.Error("setEvent approve error: " + err.Error())
	}

	// cross contract call "assets" function "transferfrom" to transfer amount tokens from sender account to htlc account
	contractName = "assets"
	method = "transferFrom"
	assets_args = [][]byte{[]byte(method), []byte(sender), htlcAddress, htlcAddress, []byte(amountString)}

	resp = stub.InvokeChaincode(contractName, assets_args, stub.GetChannelID())
	// cross contract call "transferFrom" function fail, return error info
	if resp.Status == shim.ERROR {
		return shim.Error("invoke assets.tranferFrom error:" + resp.Message + ",approve:" + approve_result)
	}
	// throw an event of topic:"transferfrom"
	if err := stub.SetEvent("transferfrom", resp.Payload); err != nil {
		return shim.Error("setEvent transferfrom error: " + err.Error())
	}

	// update senderproposal's locked state
	senderProposal.Locked = true

	// generate only proposalID by proposal content and putstate to chain
	proposalJson, err := json.Marshal(senderProposal)
	if err != nil {
		return shim.Error("senderProposal json error: " + err.Error())
	}
	proposalID := mySha256(proposalJson)
	// update chain state by key-value:<propsalID, proposalData>
	if err := stub.PutState(proposalID, proposalJson); err != nil {
		// update chain state error
		return shim.Error("put newProposal error: " + err.Error())
	}
	// throw an event of topic:"newProposal"
	eventData := []string{
		proposalID,
		string(proposalJson),
	}
	eventBytes, err := json.Marshal(eventData)
	if err != nil {
		return shim.Error("json.Marshal eventData error: " + err.Error())
	}
	if err := stub.SetEvent("newProposal", eventBytes); err != nil {
		return shim.Error("setEvent newProposal error: " + err.Error())
	}
	return shim.Success([]byte(proposalID))
}

// getProposal gets proposal data by proposalID
// @param proposalID proposalID
func (h *HTLC) getProposal(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	//参数检查
	if len(args) != 1 {
		return shim.Error(fmt.Sprintf("length of args error, expect 1, get %d", len(args)))
	}

	proposalID := args[0]
	proposalJson, err := stub.GetState(proposalID)
	// get proposal content with proposalID from client error
	if err != nil {
		return shim.Error("get Proposal error: " + err.Error())
	}

	// throw an event of topic:"getProposal"
	if err := stub.SetEvent("getProposal", proposalJson); err != nil {
		return shim.Error("setEvent getProposal error: " + err.Error())
	}
	return shim.Success(proposalJson)
}

// withdraw transfer target proposal's token to proposal's receiver by proposalID, preimage
// @param proposalID proposalID
// @param preimage 原像
func (h *HTLC) withdraw(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	//参数检查
	if len(args) != 2 {
		return shim.Error(fmt.Sprintf("length of args error, expect 2, get %d", len(args)))
	}

	proposalID := args[0]
	proposalJson, err := stub.GetState(proposalID)
	// get proposal content with proposalID from client error
	if err != nil {
		return shim.Error("refer" + proposalID + "error: " + err.Error())
	}

	var proposalData Proposal
	err = json.Unmarshal(proposalJson, &proposalData)
	if err != nil {
		return shim.Error("proposal unmarshal error: " + err.Error() + " json:" + string(proposalJson))
	}

	// check sender euqal to receiver
	sender, err := getSenderCertificateHash(stub)
	if err != nil {
		return shim.Error("get sender error: " + err.Error())
	}
	if proposalData.Receiver != string(sender) {
		return shim.Error("permission deny, only receiver")
	}
	// check proposal locked state
	if !proposalData.Locked {
		return shim.Error("proposal \"locked\" state error")
	}
	// check proposal unlocked state
	if proposalData.Unlocked {
		return shim.Error("proposal \"unlocked\" state error")
	}
	// check proposal rolledback state
	if proposalData.Rolledback {
		return shim.Error("proposal \"rolledback\" state error")
	}
	// check timelock state
	// get current chainmaker timestamp
	currTimeStamp, err := stub.GetTxTimestamp()
	if err != nil {
		shim.Error("getTxTimeStamp error: " + err.Error())
	}

	currTime := currTimeStamp.Seconds
	proposalTime := proposalData.PosalTime
	timelock := proposalData.TimeLock
	// check if timestamp out of timelock
	if (currTime - proposalTime) > timelock {
		return shim.Error("timelock out of date")
	}
	// unlock hashlock
	proposalHash := proposalData.HashLock
	preimage := args[1]
	// check if preimage is hash's key
	if !hashMatch(proposalHash, preimage) {
		return shim.Error("hash not matched")
	}

	// cross contract call "assets" function "transfer" to transfer amount tokens from htlc account to receiver account
	contractName := "assets"
	method := "transfer"
	receiverAddress := proposalData.Receiver
	amount := proposalData.Amount
	amountString := strconv.FormatInt(amount, 10)
	assets_args := [][]byte{[]byte(method), []byte(receiverAddress), []byte(amountString)}
	resp := stub.InvokeChaincode(contractName, assets_args, stub.GetChannelID())
	// cross contract call fail, return error info
	if resp.Status == shim.ERROR {
		return shim.Error(resp.GetMessage())
	}

	// update proposal's state
	proposalData.Locked = false
	proposalData.Unlocked = true
	proposalData.Amount = 0
	proposalData.Preimage = preimage
	proposalJson, err = json.Marshal(proposalData)
	if err != nil {
		return shim.Error("proposal marshal error: " + err.Error())
	}
	// update chain state by key-value:<propsalID, proposalData>
	if err := stub.PutState(proposalID, proposalJson); err != nil {
		// update chain state error
		return shim.Error("update proposal state error: " + err.Error())
	}
	// throw an event of topic:"withdraw"
	eventData := []string{
		proposalID,
		string(proposalJson),
	}
	eventBytes, err := json.Marshal(eventData)
	if err != nil {
		return shim.Error("json.Marshal eventData error: " + err.Error())
	}
	if err := stub.SetEvent("withdraw", eventBytes); err != nil {
		return shim.Error("setEvent withdraw error: " + err.Error())
	}
	return shim.Success(proposalJson)
}

// refund proposal's token to sender account by proposalID
// @param proposalID proposalID
func (h *HTLC) refund(stub shim.ChaincodeStubInterface, args []string) peer.Response {
	//参数检查
	if len(args) != 1 {
		return shim.Error(fmt.Sprintf("length of args error, expect 1, get %d", len(args)))
	}

	proposalID := args[0]
	proposalJson, err := stub.GetState(proposalID)
	// get proposal content with proposalID from client error
	if err != nil {
		return shim.Error("refer" + proposalID + "error: " + err.Error())
	}
	var proposalData Proposal
	err = json.Unmarshal(proposalJson, &proposalData)
	if err != nil {
		return shim.Error("proposal unmarshal error: " + err.Error())
	}

	// check sender euqal to receiver
	sender, err := getSenderCertificateHash(stub)
	if err != nil {
		return shim.Error("get sender error: " + err.Error())
	}
	if proposalData.Sender != string(sender) {
		return shim.Error("permission deny, only Sender")
	}
	// check proposal locked state
	if !proposalData.Locked {
		return shim.Error("proposal \"locked\" state error")
	}
	// check proposal unlocked state
	if proposalData.Unlocked {
		return shim.Error("proposal \"unlocked\" state error")
	}
	// check proposal rolledback state
	if proposalData.Rolledback {
		return shim.Error("proposal's amount has transfered")
	}

	// check timelock state
	// get current chainmaker timestamp
	currTimeStamp, err := stub.GetTxTimestamp()
	if err != nil {
		shim.Error("getTxTimeStamp error: " + err.Error())
	}

	currTime := currTimeStamp.Seconds
	proposalTime := proposalData.PosalTime
	timelock := proposalData.TimeLock
	// check if timestamp out of timelock
	if (currTime - proposalTime) < timelock {
		return shim.Error("timelock is still valid: " + strconv.FormatInt(currTime, 10))
	}
	// cross contract call "assets" function "transfer" to transfer amount tokens from htlc account to sender account
	contractName := "assets"
	method := "transfer"
	receiverAddress := proposalData.Sender
	amount := proposalData.Amount
	amountString := strconv.FormatInt(amount, 10)

	assets_args := [][]byte{[]byte(method), []byte(receiverAddress), []byte(amountString)}
	resp := stub.InvokeChaincode(contractName, assets_args, stub.GetChannelID())
	// cross contract call fail, return error info
	if resp.Status == shim.ERROR {
		return shim.Error("invokeChaincode transfer error: " + resp.GetMessage())
	}

	// update proposal's state
	proposalData.Locked = false
	proposalData.Unlocked = true
	proposalData.Rolledback = true
	proposalData.Amount = 0
	proposalJson, err = json.Marshal(proposalData)
	if err != nil {
		return shim.Error("proposal marshal error")
	}
	// update chain state by key-value:<propsalID, proposalData>
	if err := stub.PutState(proposalID, proposalJson); err != nil {
		// update chain state error
		return shim.Error("update proposal state error: " + err.Error())
	}
	// throw an event of topic:"refund"
	if err := stub.SetEvent("refund", resp.Payload); err != nil {
		return shim.Error("setEvent refund error: " + err.Error())
	}
	return shim.Success(proposalJson)
}

// 获取发起人地址
func getSenderCertificateHash(stub shim.ChaincodeStubInterface) ([]byte, error) {
	senderpk, err := stub.GetCreator()
	if err != nil {
		return nil, fmt.Errorf("GetCreator error: " + err.Error())
	}
	hash := mySha256(senderpk)
	return []byte(hash), nil
}

// contract built-in function to generate proposalID with proposalData
func mySha256(input []byte) string {
	// generate sha256 object
	hash := sha256.New()
	hash.Write(input)
	return fmt.Sprintf("%x", hash.Sum(nil))
}

// contract built-in function to test the realtionship of hashlock & preimage
func hashMatch(hash, preimage string) bool {
	// check if preimage is hash's key
	return hash == string(mySha256([]byte(preimage)))
}

// main
func main() {
	err := shim.Start(new(HTLC))
	if err != nil {
		log.Fatal(err)
	}
}
