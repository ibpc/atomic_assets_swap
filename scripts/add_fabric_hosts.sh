# bin/bash
# 添加fabric的hosts名字
FILE_NAME="/etc/hosts"
HOSTS1="\n# add by rdc for fabric\n127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4 orderer.example.com peer1.org.auth.com peer2.org.auth.com peer1.org1.raw.com peer2.org1.raw.com peer1.org2.raw.com peer2.org2.raw.com\n::1         localhost localhost.localdomain localhost6 localhost6.localdomain6 orderer.example.com peer1.org.auth.com peer2.org.auth.com peer1.org1.raw.com peer2.org1.raw.com peer1.org2.raw.com peer2.org2.raw.com\n"
result=`cat ${FILE_NAME} | grep "peer1.org.auth.com peer2.org.auth.com peer1.org1.raw.com peer2.org1.raw.com peer1.org2.raw.com peer2.org2.raw.com"`
if [[ "$result" == "" ]]
then 
    echo -e ${HOSTS1} | sudo tee -a ${FILE_NAME}
else 
    echo "hosts already exists."
fi