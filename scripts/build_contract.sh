# bin/bash
# 编译合约
set -euo

INFO(){
    local content=$*
    local DATE_NOW=`date "+%Y-%m-%d %H:%M:%S"`
    echo -e "\033[32m[INFO ${DATE_NOW}][fabric] ${content} \033[0m"
}

CURR_PATH="$(pwd)"
INFO "编译assets合约"
cd $CURR_PATH/contracts/assets && ./build.sh
INFO "编译htlc合约"
cd $CURR_PATH/contracts/htlc && ./build.sh
INFO "编译lilac合约"
cd $CURR_PATH/contracts/lilac && ./build.sh