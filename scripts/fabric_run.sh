# /bin/bash
# fabric起链并加载文件
# set -euo pipefail
set -euo

INFO(){
    local content=$*
    local DATE_NOW=`date "+%Y-%m-%d %H:%M:%S"`
    echo -e "\033[32m[INFO ${DATE_NOW}][fabric] ${content} \033[0m"
}

ERROR(){
    local content=${1}
    local DATE_NOW=`date "+%Y-%m-%d %H:%M:%S"`
    echo -e "\033[31m[ERROR ${DATE_NOW}][fabric] ${content} \033[0m"
}

# !设置需要更改的变量
# fabric binary目录
FABRIC_BIN_PATH="$(dirname $(pwd))/bin"
# 配置文件根目录
CONFIG_ROOT_DIR=$(dirname $(pwd))/config_files/fabric
# 配置fabric src目录
CONTRACT_SRC_DIR=$(dirname $(pwd))/contracts/fabric/src
# 打印出来看看
INFO "fabric binary path: ${FABRIC_BIN_PATH}"
INFO "config root dir: ${CONFIG_ROOT_DIR}"

# 导入版本环境变量
# export IMAGE_TAG="latest"
export IMAGE_TAG="1.4"
export SYS_CHANNEL="syschannel"
# 配置文件根目录
export CONFIG_ROOT_DIR=${CONFIG_ROOT_DIR}
# 通道信息保存的目录
export CHANNEL_SAVE_DIR="${CONFIG_ROOT_DIR}/channel-artifacts"
# fabric src目录
export CONTRACT_SRC_DIR=${CONTRACT_SRC_DIR}
# 通知
INFO "IMAGE_TAG: ${IMAGE_TAG}"
INFO "CHANNE_SAVE_DIR: ${CHANNEL_SAVE_DIR}"

# 设置通道名
CHANNEL_TEST="test-channel"
CHANNEL_CROSSCHAIN="cc-channel"
CHANNEL_MYCC="mycc"

# 设置fabric二进制文件路径
CRYPTOGEN_BIN="${FABRIC_BIN_PATH}/cryptogen"
CONFIGTXGEN_BIN="${FABRIC_BIN_PATH}/configtxgen"
# 通知
INFO "cryptogen binary path: ${CRYPTOGEN_BIN}"
INFO "configtxgen binary path: ${CONFIGTXGEN_BIN}"

# 组织文件，根据组织生成证书
CRYPTO_CONFIG_YAML="${CONFIG_ROOT_DIR}/crypto-config.yaml"

# 创建目录
if [ ! -d ${CHANNEL_SAVE_DIR} ]; then
    mkdir ${CHANNEL_SAVE_DIR}
fi

# 使用方法
function USAGE(){
    echo "====== usage ======"
    echo "启动网络          ./fabric_run.sh start"
    echo "启动网络生成证书   ./fabric_run.sh start -f"
    echo "关闭网络          ./fabric_run.sh clear"
    echo "关闭网络清理证书   ./fabric_run.sh clear -f"
    echo "====== end ======"
    echo ""
}

# 启动容器
function START(){
    # 启动容器
    INFO "启动容器"
    # docker-compose -f ${CONFIG_ROOT_DIR}/docker-compose.yaml up -d
    docker-compose -f ${CONFIG_ROOT_DIR}/docker-compose.yaml up
    # docker-compose -f docker-compose.yaml up
}

# 关闭并清理容器
function CLEAR(){
    #关闭容器
    INFO "关闭并清理容器"
    container=`docker ps -a | grep hyperledger | awk '{print $1}'`
    chaincode_container=`docker ps -a | grep dev | awk '{print $1}'`
    # echo ${container}
    if [ -n "${container}" ];then
        docker-compose -f ${CONFIG_ROOT_DIR}/docker-compose.yaml down
        if [ -n "${chaincode_container}" ];then
            docker container stop $(docker ps -a | grep dev | awk '{print $1}')
        fi
        echo y | docker container prune
        INFO "容器删除完毕..."
        # docker volume  rm $(docker volume ls | grep fixtures | awk '{print $2}')
        echo y|docker volume prune
        if [ -n "`docker images|grep dev | awk '{print $1}'`" ];then
            docker image rm $(docker images|grep dev | awk '{print $1}')
        fi
        INFO "volume删除完毕..."
    fi
}

# 启动容器并重新生成证书
function START_F(){
    INFO "生成证书文件"
    # 注意输出路径要有crypto-config，这个目录是固定的
    ${CRYPTOGEN_BIN} generate --config=${CRYPTO_CONFIG_YAML} --output=${CONFIG_ROOT_DIR}/crypto-config
    
    # 整个fabric的创世区块
    INFO "生成系统通道文件"
    # echo ${CONFIGTXGEN_BIN} -configPath ${CONFIG_ROOT_DIR} -profile GenGenesis -channelID ${SYS_CHANNEL} -outputBlock ${CHANNEL_SAVE_DIR}/genesis.block
    ${CONFIGTXGEN_BIN} -configPath ${CONFIG_ROOT_DIR} -profile GenGenesis -channelID ${SYS_CHANNEL} -outputBlock ${CHANNEL_SAVE_DIR}/genesis.block

    # 生成通道，保存到一笔特殊交易里面
    INFO "生成应用通道文件..."
    ${CONFIGTXGEN_BIN} -configPath ${CONFIG_ROOT_DIR} -profile GenTwoOrgsChannel -channelID ${CHANNEL_CROSSCHAIN} -outputCreateChannelTx ${CHANNEL_SAVE_DIR}/${CHANNEL_CROSSCHAIN}.tx

    # 锚节点是不同组织之间进行通信的东西，如果没有通信就不太需要
    INFO "生成锚节点"
    orgs=`cat ${CONFIG_ROOT_DIR}/configtx.yaml | grep Name | sed 's/Name: //g' | awk '{print $1}'`
    for name in ${orgs}
    do
        if [ $name != "OrdererOrg" ]; then
            ${CONFIGTXGEN_BIN} -configPath ${CONFIG_ROOT_DIR} -profile GenTwoOrgsChannel -channelID ${CHANNEL_CROSSCHAIN} -outputAnchorPeersUpdate ${CHANNEL_SAVE_DIR}/${name}anchors.tx -asOrg ${name}
        fi
    done
    
    # 证书生成完毕，开始启动镜像
    START
}

# 关闭容器并清除证书
function CLEAR_F(){
    # 关闭并清理容器
    CLEAR

    INFO "删除配置..."
    rm -rf ${CONFIG_ROOT_DIR}/channel-artifacts
    rm -rf ${CONFIG_ROOT_DIR}/crypto-config
}

if [ $# -eq 1 ]; then
    # start
    if [ "$1" = "start" ]; then
        START
    elif [ $1 = "clear" ]; then
        CLEAR
    else
        ERROR "无效参数"
        USAGE
        exit 1
    fi
elif [ $# -eq 2 ]; then
    # start -f
    if [ "$1" == "start" ] && [ "$2" == "-f" ]; then
        START_F
    elif [ $1 == "clear" ] && [ $2 == "-f" ]; then
        CLEAR_F
    else
        ERROR "无效参数"
        USAGE
        exit 1
    fi
else
    ERROR "无效参数"
    USAGE
    exit 2
fi

INFO "finish"

# 查看config,排查compose挂载错误
# docker-compose -f ${CONFIG_ROOT_DIR}/docker-compose.yaml config