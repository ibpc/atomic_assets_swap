/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 针对chainmaker的log函数
*/
package utils

import (
	"fmt"

	"chainmaker.org/chainmaker/pb-go/v2/common"
)

// 自定义的log函数
// @param funcName 是哪一个函数出来的log
// @param resp 合约返回的response
// @param err 其他错误
// @param return 错误信息
func MyLogRespErr(funcName string, resp *common.TxResponse, err error) string {
	info := "=== " + funcName + " ==="
	if resp != nil {
		info += "\nresp.Message->" +
			resp.Message + "\nresp.ContractsResult->" +
			resp.ContractResult.String()
	}
	if err != nil {
		info += ("\nerror->" + err.Error())
	}
	info += "\n"
	fmt.Println(info)
	return info
}

// 自定义的log函数
// @param funcName 是哪一个函数出来的log
// @param resp 合约返回的response
// @param err 其他错误
// @param return 错误信息
func MyLogPayloadErr(funcName string, payload *common.Payload, err error) string {
	info := "=== " + funcName + " ==="
	if payload != nil {
		info += "\npayload.string->" +
			payload.String()
	}
	if err != nil {
		info += ("\nerror->" + err.Error())
	}
	info += "\n"
	fmt.Println(info)
	return info
}
