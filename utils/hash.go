/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 计算哈希以及匹配哈希值
*/
package utils

import (
	"crypto/sha256"
	"fmt"
)

// 计算sha256哈希
// @param abData 字节数组
// @return string 计算的哈希
func GetSha256Hash(abData []byte) string {
	hasher := sha256.New()
	hasher.Write(abData)
	return fmt.Sprintf("%x", hasher.Sum(nil))
}
