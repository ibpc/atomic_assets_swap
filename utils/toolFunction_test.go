/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 测试函数
*/
package utils

import (
	"atomic_assets_swap/chainclient/proposalstruct"
	"fmt"
	"testing"
)

func TestFileExist(t *testing.T) {
	if FileExist("file.notexit") {
		t.Error("file exist error")
	}
	if !FileExist("file.go") {
		t.Error("file exist error2")
	}
}

func TestSha256Hash(t *testing.T) {
	h := GetSha256Hash([]byte("hash"))
	if len(h) <= 1 {
		t.Error("gesha256 error")
	}
}

func TestChainmakerLog(t *testing.T) {
	if len(MyLogRespErr("name", nil, nil)) <= 1 {
		t.Error("log resp error")
	}
}

func TestChainmakerPayloadLog(t *testing.T) {
	if len(MyLogPayloadErr("name", nil, nil)) <= 1 {
		t.Error("log payload error")
	}
}

func TestFabricChainnelRespError(t *testing.T) {
	if len(InfoFabricChannelRespError("name", nil, nil)) <= 1 {
		t.Error("log fabric error")
	}
}

func TestLogger(t *testing.T) {
	if len(InfoError("name", fmt.Errorf("error"))) <= 1 {
		t.Error("infoError error")
	}
	if len(Info("info")) <= 1 {
		t.Error("info error")
	}
	if len(InfoWarning("warning")) <= 1 {
		t.Error("infoWarning error")
	}
	if len(InfoTips("tips")) <= 1 {
		t.Error("infoTips error")
	}
}

func TestEqualProposal(t *testing.T) {
	p1 := proposalstruct.Proposal{}
	p1.ProposalType = "htlc"

	p2 := proposalstruct.Proposal{}
	p2.ProposalType = "lilac"
	p2.Preimage = StringArray2Any([]string{"a", "b"})
	p2.HashLock = StringArray2Any([]string{"h1", "h2"})

	p3 := proposalstruct.Proposal{}

	if EqualProposal(p1, p2) {
		t.Error("p1, p2 not equal")
	}

	if EqualProposal(p1, p3) {
		t.Error("p1, p3 not equal")
	}

	if EqualProposal(p2, p3) {
		t.Error("p2, p3 not equal")
	}

	if !EqualProposal(p1, p1) {
		t.Error("p1 equal")
	}

	if !EqualProposal(p2, p2) {
		t.Error("p1 equal")
	}

	if EqualProposal(p3, p3) {
		t.Error("p3 not equal")
	}
}

func TestIsNumber(t *testing.T) {
	if IsNumber("a") {
		t.Error("a is not number")
	}
	if !IsNumber("123456789") {
		t.Error("is number")
	}
}

func TestSplitStr(t *testing.T) {
	ls_result, err := SplitStr(" aa _  bb \n", "_")
	fmt.Println(ls_result)
	if err != nil {
		t.Error(err)
	}
	if len(ls_result) != 2 {
		t.Error("length is 2")
	}
	if ls_result[0] != "aa" {
		t.Error("first str is not aa")
	}
	if ls_result[1] != "bb" {
		t.Error("second str is not bb")
	}
}

func TestCwd(t *testing.T) {
	if len(GetWorkSpace()) <= 1 {
		t.Error("workspace error")
	}
}

func TestTime(t *testing.T) {
	if len(GetTime()) < 10 {
		t.Error("time length error")
	}
	if GetTimestamp() < 1681374068 {
		t.Error("time number error")
	}
	if GetTimestampMs() < 1681374068040 {
		t.Error("time number ms error")
	}
}
