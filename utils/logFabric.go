/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 针对fabric的log函数
*/
package utils

import (
	"fmt"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
)

// 打印fabric的log
func InfoFabricChannelRespError(funcName string, resp *channel.Response, err error) string {
	info := "=== " + funcName + " ==="
	if resp != nil {
		info += "\nresp.STATE->" + string(resp.ChaincodeStatus) +
			"\nresp.Payload->" + string(resp.Payload)
	}
	if err != nil {
		info += ("\nerror->" + err.Error())
	}
	info += "\n"
	fmt.Println(info)
	return info
}
