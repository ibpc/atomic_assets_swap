/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 针对不同类型的日志打印不同的颜色并输出
*/
package utils

import (
	"fmt"
)

// 在控制台打印出不同颜色的信息
// 0 - 黑色
// 1 - 红色
// 2 - 绿色
// 3 - 黄色
// 4 - 蓝色
// 5 - 紫红色
// 6 - 青蓝色;
func ColorPrint(color int, message string) {
	fmt.Printf("\033[0;%dm%s\033[0m\n", color+30, message)
}

// 打印输出一条错误消息
// @param funcName 哪一个函数名报错, 自己指定
// @param err 错误信息
func InfoError(funcName string, err error) string {
	info := fmt.Sprintln("funcName->" + funcName + " error->" + err.Error())
	ColorPrint(1, info)
	return info
}

// 打印输出消息
// @param info 任意消息
// @return 合并后的消息
func Info(info ...interface{}) string {
	result := fmt.Sprint("info: ", info)
	ColorPrint(2, result)
	return result
}

// 打印警告消息
// @param info 任意消息
// @return 合并后的消息
func InfoWarning(info ...interface{}) string {
	result := fmt.Sprint("warning: ", info)
	ColorPrint(3, result)
	return result
}

// 打印提示消息
// @param info 任意消息
// @return 合并后的消息
func InfoTips(info ...interface{}) string {
	result := fmt.Sprint("tips: ", info)
	ColorPrint(4, result)
	return result
}
