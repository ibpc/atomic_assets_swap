/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 获取当前工作目录
*/
package utils

import (
	"os"
)

// 获取当前工作目录
func GetWorkSpace() string {
	myDir, err := os.Getwd()
	if err != nil {
		InfoError("os.Getwd", err)
		os.Exit(1)
	}
	InfoTips("当前工作空间: ", myDir)
	return myDir
}
