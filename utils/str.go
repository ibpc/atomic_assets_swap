/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 字符串处理与字符串转换
*/
package utils

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

// 读取输入并转化为字符串数组
func ReadInputs() ([]string, error) {
	// 输入包含空格的命令行，func_name arg1 arg2 ...
	fmt.Printf("Please input command line :")
	reader := bufio.NewReader(os.Stdin)
	cmdLine, err := reader.ReadString('\n')
	if err != nil {
		log.Fatalf("err when read inputs in function \"InputCmdLine\":%v", err)
	}
	// 需要处理最后一个\n
	cmdLine = cmdLine[:len(cmdLine)-1]
	// 处理首尾的空格，\n也会给处理掉，所以上一句可以不用
	return SplitStr(cmdLine, " ")
}

// 判断是不是数字
// @param numStr 数字字符串
func IsNumber(numStr string) bool {
	if _, err := strconv.ParseInt(numStr, 10, 64); err != nil {
		return false
	}
	return true
}

// 分割字符串
func SplitStr(str, subStr string) ([]string, error) {
	// 处理首尾的空格，\n也会给处理掉
	line := strings.TrimSpace(str)
	if len(line) == 0 {
		return nil, fmt.Errorf("empty line error")
	}
	// 分割
	vLine_ori := strings.Split(line, subStr)
	// fmt.Println(len(vCmd_ori))
	if len(vLine_ori) == 0 {
		return nil, fmt.Errorf("line is nil")
	}
	// 清除中间的空格
	vLine := make([]string, 0)
	for _, value := range vLine_ori {
		value = strings.TrimSpace(value)
		if len(value) == 0 {
			// 是空格就跳过
			continue
		}
		vLine = append(vLine, value)
	}
	return vLine, nil
}

// 比较两个字符串数组是否相等
// @param a1 第一个字符串数组
// @param a2 第二个字符串数组
// @return true 长度和每一个字符串都相等
// @return false 长度或者有一个字符串不相等
func EqualStringArray(a1, a2 []string) bool {
	if len(a1) != len(a2) {
		return false
	}
	for i, val := range a1 {
		if val != a2[i] {
			return false
		}
	}
	return true
}

// any类型转化为字符串数组
// @param a any类型的字符串数组
// @return []string 字符串数组
func Any2StringArray(a interface{}) ([]string, error) {
	result := []string{}
	arrInterface, ok := a.([]interface{})
	if !ok {
		return result, fmt.Errorf("not []interface{} error")
	}
	for _, curr := range arrInterface {
		str, ok := curr.(string)
		if !ok {
			return result, fmt.Errorf("不是string错误")
		}
		result = append(result, str)
	}
	return result, nil
}

// string类型的数组转为any类型的数组
// @param []string
// @return []any any数组
func StringArray2Any(a []string) interface{} {
	var result []interface{}
	for _, str := range a {
		result = append(result, interface{}(str))
	}
	return result
}

// 字符串数组转为byte数组
// @param strArr 字符串数组
func StrArray2ByteArray(strArr []string) [][]byte {
	var result [][]byte
	for _, k := range strArr {
		result = append(result, []byte(k))
	}
	return result
}
