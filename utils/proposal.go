/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 判断两个proposal是否是同一个
*/
package utils

import (
	"atomic_assets_swap/chainclient/proposalstruct"
	"atomic_assets_swap/config"
)

// 判断两个proposal是否相等
// @param p1 第一个proposal
// @param p2 第二个proposal
// @return true 相等
// @return false 不相等
func EqualProposal(p1, p2 proposalstruct.Proposal) bool {
	if !proposalDataEqual(p1, p2) {
		return false
	}
	switch p1.ProposalType {
	case config.PROPOSAL_TYPE_HTLC:
		if p1.Preimage != p2.Preimage || p1.HashLock != p2.HashLock {
			return false
		}
	case config.PROPOSAL_TYPE_LILAC:
		preimage1, err := Any2StringArray(p1.Preimage)
		if err != nil {
			InfoError("p1 preimage", err)
			return false
		}
		preimage2, err := Any2StringArray(p2.Preimage)
		if err != nil {
			InfoError("p2 preimage", err)
			return false
		}

		hash1, err := Any2StringArray(p1.HashLock)
		if err != nil {
			InfoError("p1 hashlock", err)
			return false
		}
		hash2, err := Any2StringArray(p2.HashLock)
		if err != nil {
			InfoError("p2 hshlock", err)
			return false
		}

		if !EqualStringArray(hash1, hash2) || !EqualStringArray(preimage1, preimage2) {
			return false
		}
	default:
		Info("invalid proposalType:" + p1.ProposalType)
		return false
	}
	return true
}

// proposal的其他属性相等
// @param p1 第一个proposal
// @param p2 第二个proposal
// @return true 相等
// @return false 不相等
func proposalDataEqual(p1, p2 proposalstruct.Proposal) bool {
	if p1.Amount != p2.Amount || p1.ChainName != p2.ChainName {
		return false
	}
	if p1.UserName != p2.UserName || p1.ContractName != p2.ContractName {
		return false
	}
	if p1.CorrespondingAmount != p2.CorrespondingAmount {
		return false
	}
	if p1.CorrespondingChainName != p2.CorrespondingChainName {
		return false
	}
	if p1.Locked != p2.Locked || p1.PosalTime != p2.PosalTime {
		return false
	}
	if p1.ProposalID != p2.ProposalID || p1.ProposalType != p2.ProposalType {
		return false
	}
	if p1.Receiver != p2.Receiver || p1.Rolledback != p2.Rolledback {
		return false
	}
	if p1.Sender != p2.Sender || p1.TimeLock != p2.TimeLock || p1.Unlocked != p2.Unlocked {
		return false
	}
	return true
}
