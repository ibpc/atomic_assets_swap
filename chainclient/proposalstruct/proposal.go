/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description proposal结构定义
*/
// Package proposalstruct proposal本地结构体定义
package proposalstruct

// Proposal 提案结构, 用于本地存储
type Proposal struct {
	Sender    string `json:"sender"`    // sender account Address
	Receiver  string `json:"receiver"`  // receiver account Address
	Amount    int64  `json:"amount"`    // transfer/lock token amounts
	PosalTime int64  `json:"posaltime"` // proposal generation time
	TimeLock  int64  `json:"timelock"`  // timelock
	// hashlock, 在htlc中是string, 在lilac中是[]string, 需要先转换成any/[]any
	HashLock interface{} `json:"hashlock"`
	// 原像, 在htlc中是string, 在Lilac中是[]string, 需要先转换成any/[]any
	Preimage interface{} `json:"preimage"`
	// BlockDepth int64  `json:"blockdepth"`
	Locked     bool `json:"locked"`     // proposal locked status
	Unlocked   bool `json:"unlocked"`   // proposal unlocked status
	Rolledback bool `json:"rolledback"` // propsoal rolledback status

	// 以下是为了实现proposal匹配而额外增加的属性

	ProposalID             string `json:"proposalID"`     // proposal id
	ChainName              string `json:"chainName"`      // proposal chain name
	UserName               string `json:"userName"`       // proposal user name
	ContractName           string `json:"contractName"`   // 这个proposal合约名
	ProposalType           string `json:"proposalType"`   //proposal type such as 'htlc' 'lilac'
	CorrespondingChainName string `json:"correChainName"` // 对端的名字
	CorrespondingAmount    int64  `json:"correAmount"`    // 对端应该转给我多少钱
}
