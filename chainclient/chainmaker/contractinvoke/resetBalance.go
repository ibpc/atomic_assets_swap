/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 重置用户余额为默认值
*/
package contractinvoke

import (
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"fmt"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
)

// 取钱
// @param client 通过哪一个客户端来调用这个函数
// @param proposalID 取哪一个proposal的钱
// @param key 原像
// @return 取钱以后的proposal字符串, error
func resetBalance(client *sdk.ChainClient) (string, error) {
	args := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("reset"),
		},
	}
	// 链上执行withdraw方法
	resp, err := InvokeContract(client, config.ASSETS_CONTRACT_NAME, "", args, true)
	if err != nil {
		info := utils.MyLogRespErr("InvokeContract", resp, err)
		return "", fmt.Errorf(info)
	}
	return string(resp.ContractResult.Result), nil
}
