/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 部署assets合约
*/
package contractinvoke

import (
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"fmt"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
)

// 部署资产合约
func deployAssetsContract(client *sdk.ChainClient) error {
	// 初始化资产
	args := []*common.KeyValuePair{
		{
			Key:   "totalSupply",
			Value: []byte("1000000"),
		},
		{
			Key:   "name",
			Value: []byte("ict-token"),
		},
		{
			Key:   "version",
			Value: []byte("ict"),
		},
		{
			Key:   "users",
			Value: []byte(""),
		},
	}
	resp, err := DeployContract(client,
		config.ASSETS_CONTRACT_NAME, config.ASSETS_CONTRACT_VERSION,
		config.ASSETS_CONTRACT_PATH, config.DOCKER_GO,
		args,
		true,
		config.CREATE_CONTRACT_TIMEOUT,
		config.Chain1Admins...)
	if err != nil {
		info := utils.MyLogRespErr("DeployContract", resp, err)
		return fmt.Errorf(info)
	}
	return nil
}

// 升级资产合约
func upgradeAssetsContract(client *sdk.ChainClient) error {
	// 初始化一个提案的参数列表
	args := []*common.KeyValuePair{}
	resp, err := UpgradeContract(client,
		config.ASSETS_CONTRACT_NAME, config.ASSETS_CONTRACT_VERSION,
		config.ASSETS_CONTRACT_PATH, config.DOCKER_GO,
		args,
		true,
		config.CREATE_CONTRACT_TIMEOUT,
		config.Chain1Admins...)
	if err != nil {
		utils.MyLogRespErr("UpgradeContract", resp, err)
		return err
	}
	return nil
}

// 查询address地址的资产余额balance
func getBalance(client *sdk.ChainClient, address string) (string, error) {
	args := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("balanceOf"),
		},
		{
			Key:   "address",
			Value: []byte(address),
		},
	}
	// 查询链上指定账户资产余额
	resp, err := QueryContract(client, config.ASSETS_CONTRACT_NAME, args)
	if err != nil {
		info := utils.MyLogRespErr("QueryContract", resp, err)
		return "", fmt.Errorf(info)
	}
	return string(resp.ContractResult.Result), nil
}

// 查询当前用户地址
// @param client 被查询地址的链客户端
func getAddress(client *sdk.ChainClient) (string, error) {
	args := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("getAddress"),
		},
	}
	// 查询链上指定账户资产余额
	resp, err := QueryContract(client, config.ASSETS_CONTRACT_NAME, args)
	if err != nil {
		info := utils.MyLogRespErr("QueryContract", resp, err)
		return "", fmt.Errorf(info)
	}
	return string(resp.ContractResult.Result), nil
}
