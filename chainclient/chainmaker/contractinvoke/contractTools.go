/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 与链上交互的工具函数
*/
package contractinvoke

import (
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"errors"
	"fmt"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
	sdkutils "chainmaker.org/chainmaker/sdk-go/v2/utils"
)

// 部署合约
// @param client: 客户端
// @param contractName: 合约名称
// @param version: 合约版本
// @param byteCodePath: 编译好的合约文件路径
// @param runtime: 运行方式WASM,DOCKER_GO,GASM等
// @param kvs: 合约初始化所需要的参数，以key-value形式传入
// @param withSyncResult: 是否需要同步获取交易结果
// @param usernames: 链节点数(需要节点背书同意)
func DeployContract(
	client *sdk.ChainClient,
	contractName string,
	version string,
	byteCodePath string,
	runtime common.RuntimeType,
	kvs []*common.KeyValuePair,
	withSyncResult bool,
	createContractTimeout int64,
	usernames ...string,
) (*common.TxResponse, error) {
	payload, err := client.CreateContractCreatePayload(contractName, version, byteCodePath, runtime, kvs)
	if err != nil {
		info := utils.MyLogPayloadErr("client.CreateContractCreatePayload", payload, err)
		return nil, fmt.Errorf(info)
	}
	// 获取背书节点
	endorsers, err := getEndorsers(payload, usernames...)
	if err != nil {
		return nil, fmt.Errorf("getEndorsers error: " + err.Error())
	}
	resp, err := client.SendContractManageRequest(payload, endorsers, createContractTimeout, withSyncResult)
	if err != nil {
		return resp, err
	}
	// 检查回执
	err = checkProposalRequestResp(resp, true)
	if err != nil {
		return resp, err
	}
	return resp, nil
}

// 检查回执
func checkProposalRequestResp(resp *common.TxResponse, needContractResult bool) error {
	if resp.Code != common.TxStatusCode_SUCCESS {
		if resp.Message == "" {
			resp.Message = resp.Code.String()
		}
		return fmt.Errorf(resp.Message)
	}
	if needContractResult && resp.ContractResult == nil {
		return fmt.Errorf("contract result is nil")
	}
	// 0是成功代码
	if resp.ContractResult != nil && resp.ContractResult.Code != 0 {
		return fmt.Errorf(resp.ContractResult.Message)
	}
	return nil
}

// 升级合约
// @param client: 客户端
// @param contractName: 合约名称
// @param upgradeVersion: 合约版本
// @param upgradeByteCodePath: 编译好的合约文件路径
// @param runtime: 运行方式WASM,DOCKER_GO,GASM等
// @param kvs: 合约初始化所需要的参数，以key-value形式传入
// @param withSyncResult: 是否需要同步获取交易结果
// @param usernames: 链节点数(需要节点背书同意)
func UpgradeContract(
	client *sdk.ChainClient,
	contractName string,
	upgradeVersion string,
	upgradeByteCodePath string,
	runtime common.RuntimeType,
	kvs []*common.KeyValuePair,
	withSyncResult bool,
	createContractTimeout int64,
	usernames ...string,
) (*common.TxResponse, error) {
	// 和部署合约的操作基本一样
	payload, err := client.CreateContractUpgradePayload(contractName, upgradeVersion, upgradeByteCodePath, runtime, kvs)
	if err != nil {
		fmt.Println(err)
		return nil, nil
	}

	// 获取背书节点
	endorsers, err := getEndorsers(payload, usernames...)
	if err != nil {
		fmt.Println(err)
		return nil, nil
	}

	resp, err := client.SendContractManageRequest(payload, endorsers, createContractTimeout, withSyncResult)
	if err != nil {
		fmt.Println(err)
		return nil, nil
	}

	err = checkProposalRequestResp(resp, false)
	if err != nil {
		fmt.Println(err)
		return nil, nil
	}
	return resp, nil
}

// 查询合约信息
// @param client: 客户端
// @param contractName: 合约名称
// @param method: 调用或者查询，这里一般是设置成invoke_contract
// @param txId: 可以置空
// @param kvs: 参数列表
// @param withSyncResult: 是否同步获取结果
func QueryContract(
	client *sdk.ChainClient,
	contractName string,
	kvs []*common.KeyValuePair,
) (*common.TxResponse, error) {
	resp, err := client.QueryContract(contractName, config.INVOKE_METHOD, kvs, -1)
	if err != nil {
		return resp, err
	}

	if resp.Code != common.TxStatusCode_SUCCESS {
		return resp, fmt.Errorf("invoke %s contract failed. [code]:%d\t[msg]:%s", contractName, resp.Code, resp.Message)
	}
	return resp, nil
}

// 调用合约方法
// @param client: 客户端
// @param contractName: 合约名称
// @param method: 调用或者查询，这里一般是设置成invoke_contract
// @param txId: 可以置空
// @param kvs: 参数列表
// @param withSyncResult: 是否同步获取结果
func InvokeContract(
	client *sdk.ChainClient,
	contractName,
	txId string,
	kvs []*common.KeyValuePair,
	withSyncResult bool,
) (*common.TxResponse, error) {
	resp, err := client.InvokeContract(contractName, config.INVOKE_METHOD, txId, kvs, -1, withSyncResult)
	if err != nil {
		info := utils.MyLogRespErr("client.InvokeContract", resp, err)
		return resp, fmt.Errorf(info)
	}

	if resp.Code != common.TxStatusCode_SUCCESS {
		info := utils.MyLogRespErr("client.InvokeContract", resp, err)
		return resp, fmt.Errorf(info)
	}

	return resp, nil
}

// 获取背书节点
func getEndorsers(payload *common.Payload, usernames ...string) ([]*common.EndorsementEntry, error) {
	var endorsers []*common.EndorsementEntry

	for _, name := range usernames {
		// 获取管理员证书路径
		users := config.GetAdmins()
		u, ok := users[name]
		if !ok {
			return nil, errors.New("user not found")
		}

		var err error
		var entry *common.EndorsementEntry
		p11Handle := sdk.GetP11Handle()
		if p11Handle != nil {
			entry, err = sdkutils.MakeEndorserWithPathAndP11Handle(u.SignKeyPath, u.SignCrtPath, p11Handle, payload)
			if err != nil {
				return nil, err
			}
		} else {
			entry, err = sdkutils.MakeEndorserWithPath(u.SignKeyPath, u.SignCrtPath, payload)
			if err != nil {
				return nil, err
			}
		}

		endorsers = append(endorsers, entry)
	}

	return endorsers, nil
}
