/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 测试
*/
package contractinvoke

import (
	"atomic_assets_swap/chainclient/proposalstruct"
	"atomic_assets_swap/utils"
	"context"
	"encoding/json"
	"reflect"
	"testing"

	"bou.ke/monkey"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/config"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
)

// 链SDK
var client = &sdk.ChainClient{}

// 初始化, 打桩
func initFunc() {
	// 给sdk.NewCainClient打桩
	monkey.Patch(sdk.NewChainClient, func(...sdk.ChainClientOption) (*sdk.ChainClient, error) {
		result := sdk.ChainClient{}
		return &result, nil
	})

	// 给enablecerthash打桩
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&sdk.ChainClient{}),
		"EnableCertHash",
		func(*sdk.ChainClient) error {
			return nil
		})

	// 给InvokeContract打桩
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&sdk.ChainClient{}),
		"InvokeContract",
		func(*sdk.ChainClient,
			string, string, string, []*common.KeyValuePair, int64, bool,
		) (*common.TxResponse, error) {
			resp := common.TxResponse{}
			resp.Code = common.TxStatusCode_SUCCESS
			resp.ContractResult = &common.ContractResult{}
			resp.ContractResult.Result = []byte("InvokeContractResult")
			return &resp, nil
		})

	// invoke contract打桩补丁
	monkey.PatchInstanceMethod(reflect.TypeOf(&sdk.ChainClient{}),
		"InvokeContractWithLimit",
		func(*sdk.ChainClient,
			string, string, string, []*common.KeyValuePair, int64, bool,
			*common.Limit) (*common.TxResponse, error) {
			resp := common.TxResponse{}
			resp.Code = common.TxStatusCode_SUCCESS
			resp.ContractResult = &common.ContractResult{}
			resp.ContractResult.Result = []byte("InvokeContractResult")
			return &resp, nil
		})

	// 给QueryContract打桩
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&sdk.ChainClient{}),
		"QueryContract",
		func(*sdk.ChainClient,
			string, string, []*common.KeyValuePair, int64,
		) (*common.TxResponse, error) {
			resp := common.TxResponse{}
			resp.Code = common.TxStatusCode_SUCCESS
			resp.ContractResult = &common.ContractResult{}
			info, _ := json.Marshal(proposalstruct.Proposal{})
			resp.ContractResult.Result = info
			return &resp, nil
		})

	// GetContractInfo
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&sdk.ChainClient{}),
		"GetContractInfo",
		func(*sdk.ChainClient, string) (*common.Contract, error) {
			return &common.Contract{}, nil
		},
	)

	// GetChainConfig
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&sdk.ChainClient{}),
		"GetChainConfig",
		func(*sdk.ChainClient) (*config.ChainConfig, error) {
			result := config.ChainConfig{}
			result.ChainId = "chainid"
			return &result, nil
		},
	)

	// CreateContractCreatePayload
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&sdk.ChainClient{}),
		"CreateContractCreatePayload",
		func(*sdk.ChainClient, string, string, string,
			common.RuntimeType, []*common.KeyValuePair) (*common.Payload, error) {
			result := common.Payload{}
			return &result, nil
		},
	)

	// CreateContractUpgradePayload
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&sdk.ChainClient{}),
		"CreateContractUpgradePayload",
		func(*sdk.ChainClient, string, string, string,
			common.RuntimeType, []*common.KeyValuePair) (*common.Payload, error) {
			result := common.Payload{}
			return &result, nil
		},
	)

	// SendContractManageRequest
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&sdk.ChainClient{}),
		"SendContractManageRequest",
		func(*sdk.ChainClient, *common.Payload, []*common.EndorsementEntry,
			int64, bool) (*common.TxResponse, error) {
			result := common.TxResponse{}
			result.Code = common.TxStatusCode_SUCCESS
			result.ContractResult = &common.ContractResult{}
			result.ContractResult.Result = []byte("result")
			result.ContractResult.Code = 0
			return &result, nil
		},
	)

	// SubscribeContractEvent
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&sdk.ChainClient{}),
		"SubscribeContractEvent",
		func(*sdk.ChainClient, context.Context, int64, int64,
			string, string) (<-chan interface{}, error) {
			result := make(<-chan interface{}, 100)
			return result, nil
		},
	)
}

// 测试合约部署、更新
func TestDelpoyUpgrade(t *testing.T) {
	// 部署assets
	if err := DeployAssetsContract(client); err != nil {
		panic(err)
	}
	// 部署htlc
	if err := DeployHTLCContract(client); err != nil {
		panic(err)
	}
	// 部署Lilac
	if err := DeployLilacContract(client); err != nil {
		panic(err)
	}
}

// 测试更新
func TestUpgrade(t *testing.T) {
	// 更新asets
	if err := UpgradeAssetsContract(client); err != nil {
		panic(err)
	}
	// 更新htlc
	if err := UpgradeHTLCContract(client); err != nil {
		panic(err)
	}
	// 更新lilac
	if err := UpgradeLilacContract(client); err != nil {
		panic(err)
	}
}

// 测试HTLC
func TestHTLCProposal(t *testing.T) {
	// 获取proposal
	if _, err := GetHTLCProposal(client, "proposalid"); err != nil {
		panic(err)
	}

	// 发起 proposal
	amount := "1"
	hashlock := "hashlock"
	timelock := "10000"
	toaddr := "toaddrhtlc"
	// amount 错误
	if _, err := NewHTLCProposal(client, toaddr, "amount", hashlock, timelock); err == nil {
		panic("new htlc proposal amount error: err == nil error")
	}
	// timelock 错误
	if _, err := NewHTLCProposal(client, toaddr, amount, hashlock, "timelock"); err == nil {
		panic("new htlc proposal timelock error: err == nil error")
	}
	// 正常proposal
	if _, err := NewHTLCProposal(client, toaddr, amount, hashlock, timelock); err != nil {
		panic(err)
	}
}

// 测试LILAC
func TestLilacProposal(t *testing.T) {
	// 获取lilac
	if _, err := GetLilacProposal(client, "proposalid"); err != nil {
		panic(err)
	}

	// 发起 proposal
	amount := "1"
	hashlock := []string{"hash1", "hash2"}
	timelock := "10000"
	toaddr := "toaddrlilac"
	// amount 错误
	if _, err := NewLilacProposal(client, toaddr, "amount", hashlock, timelock); err == nil {
		panic("new lilac proposal amount error: err == nil error")
	}
	// timelock 错误
	if _, err := NewLilacProposal(client, toaddr, amount, hashlock, "timelock"); err == nil {
		panic("new lilac proposal timelock error: err == nil error")
	}
	// 正常proposal
	if _, err := NewLilacProposal(client, toaddr, amount, hashlock, timelock); err != nil {
		panic(err)
	}
}

// 测试withdraw
func TestWithdraw(t *testing.T) {
	if _, err := WithdrawHTLC(client, "proposalid", "preimage"); err != nil {
		panic(err)
	}
	if _, err := WithdrawLilac(client, "proposalid", []string{"key1", "key2"}); err != nil {
		panic(err)
	}
}

// 测试refund
func TestRefund(t *testing.T) {
	if _, err := RefundHTLC(client, "proposalid"); err != nil {
		panic(err)
	}
	if _, err := RefundLilac(client, "proposalid"); err != nil {
		panic(err)
	}
}

// 测试其他功能
func TestOtherFunc(t *testing.T) {
	// 测试获取余额
	if _, err := GetBalance(client, "address"); err != nil {
		panic(err)
	}

	// 重置资产测试
	if _, err := ResetBalance(client); err != nil {
		panic(err)
	}

	// 获得地址
	if _, err := GetAddress(client); err != nil {
		panic(err)
	}
}

func TestMain(m *testing.M) {
	utils.InfoTips("开始测试chainmaker合约调用功能")
	// 初始化
	initFunc()
	// 执行
	m.Run()
	utils.InfoTips("chainmaker合约调用功能 测试完成")
}
