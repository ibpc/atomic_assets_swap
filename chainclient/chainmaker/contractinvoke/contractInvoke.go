/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 与合约交互，对外暴露的全部功能
*/
// Package contractinvoke 与合约交互用到的基本功能
package contractinvoke

import (
	"atomic_assets_swap/utils"
	"fmt"

	sdk "chainmaker.org/chainmaker/sdk-go/v2"
)

// 部署Asset合约
// @param client 执行客户端
// @return error
func DeployAssetsContract(client *sdk.ChainClient) error {
	return deployAssetsContract(client)
}

// 部署HTLC合约
// @param client 执行客户端
// @return error
func DeployHTLCContract(client *sdk.ChainClient) error {
	return deployHTLC(client)
}

// 部署Lilac合约
// @param client 执行客户端
// @return error
func DeployLilacContract(client *sdk.ChainClient) error {
	return deployLilac(client)
}

// 更新资产合约
// @param client 执行客户端
// @return error
func UpgradeAssetsContract(client *sdk.ChainClient) error {
	return upgradeAssetsContract(client)
}

// 更新htlc合约
// @param client 执行客户端
// @return error
func UpgradeHTLCContract(client *sdk.ChainClient) error {
	return upgradeHTLCContract(client)
}

// 更新lilac合约
// @param client 执行客户端
// @return error
func UpgradeLilacContract(client *sdk.ChainClient) error {
	return upgradeLilacContract(client)
}

// 查询资产余额
// @param client
// @proposalID 提案id
// @return 余额,error
func GetBalance(client *sdk.ChainClient, address string) (string, error) {
	return getBalance(client, address)
}

// 重置资产
// @param client
func ResetBalance(client *sdk.ChainClient) (string, error) {
	return resetBalance(client)
}

// 查询当前用户地址
// @param client 被查询地址的链客户端
func GetAddress(client *sdk.ChainClient) (string, error) {
	return getAddress(client)
}

// 查询这个proposal
// @param client
// @proposalID 提案id
// @return proposal字符串, error
func GetHTLCProposal(client *sdk.ChainClient, proposalID string) (string, error) {
	return getHTLCProposal(client, proposalID)
}

// 查询这个proposal
// @param client
// @proposalID 提案id
// @return proposal字符串, error
func GetLilacProposal(client *sdk.ChainClient, proposalID string) (string, error) {
	return getLilacProposal(client, proposalID)
}

// 发起一个HTLCproposal
// @param client 这个客户端发起
// @param toAddress 发钱给这个人
// @param amount 发这么多钱过去
// @param hashlock 哈希锁
// @param timelock 时间锁
// @return proposalID, error
func NewHTLCProposal(
	client *sdk.ChainClient,
	toAddress,
	amount,
	hashlock,
	timelock string,
) (string, error) {
	// 必须是整数
	if !utils.IsNumber(amount) {
		info := utils.Info("function NewHTLCProposal params \"amount\" is not a number: " + amount)
		return "", fmt.Errorf(info)
	}
	if !utils.IsNumber(timelock) {
		info := utils.Info("function NewHTLCProposal params \"timelock\" is not a number: " + timelock)
		return "", fmt.Errorf(info)
	}
	return newHTLCProposal(client, toAddress, amount, hashlock, timelock)
}

// 取钱HTLC
// @param client 通过哪一个客户端来调用这个函数
// @param proposalID 取哪一个proposal的钱
// @param key 原像
// @return 取钱以后的proposal字符串, error
func WithdrawHTLC(client *sdk.ChainClient, proposalID, key string) (string, error) {
	return withdrawHTLC(client, proposalID, key)
}

// 退钱HTLC
// @param client 客户端
// @param proposalID 从这个proposal退钱
func RefundHTLC(client *sdk.ChainClient, proposalID string) (string, error) {
	return refundHTLC(client, proposalID)
}

// 发起一个Lilacproposal
// @param client 这个客户端发起
// @param toAddress 发钱给这个人
// @param amount 发这么多钱过去
// @param hashlock 哈希锁
// @param timelock 时间锁
// @return proposalID, error
func NewLilacProposal(
	client *sdk.ChainClient,
	toAddress,
	amount string,
	hashlock []string,
	timelock string,
) (string, error) {
	if !utils.IsNumber(amount) {
		info := utils.Info("function NewLilacProposal params \"amount\" is not a number: " + amount)
		return "", fmt.Errorf(info)
	}
	if !utils.IsNumber(timelock) {
		info := utils.Info("function NewLilacProposal params \"timelock\" is not a number: " + timelock)
		return "", fmt.Errorf(info)
	}
	return newLilacProposal(client, toAddress, amount, hashlock, timelock)
}

// 取钱Lilac
// @param client 通过哪一个客户端来调用这个函数
// @param proposalID 取哪一个proposal的钱
// @param key 原像
// @return 取钱以后的proposal字符串, error
func WithdrawLilac(client *sdk.ChainClient, proposalID string, key []string) (string, error) {
	return withdrawLilac(client, proposalID, key)
}

// 退钱Lilac
// @param client 客户端
// @param proposalID 从这个proposal退钱
// @return string 退钱的结果
// @return error err
func RefundLilac(client *sdk.ChainClient, proposalID string) (string, error) {
	return refundLilac(client, proposalID)
}

// 监听合约事件
// @param cc 发起监听的客户端
// @param contractName 合约名
// @param eventName 事件名
// @param resultChan 保存结果的通道
func SubscribeEvent(cc *sdk.ChainClient, contractName, eventName string, resultChan chan<- interface{}) {
	subscribeEvent(cc, contractName, eventName, resultChan)
}
