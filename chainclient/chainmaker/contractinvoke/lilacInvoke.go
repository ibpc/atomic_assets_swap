/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description lilac合约调用相关函数，包括：
	1. 部署lilac合约
	2. 更新lilac合约
	3. 发起一个新的lilac proposal
	4. 根据proposalID获得这个proposal信息
	5. 根据proposalID对其进行退款操作
	6. 根据proposalID对其进行取款操作
*/
// lilac合约调用相关函数
package contractinvoke

import (
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"encoding/json"
	"fmt"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
)

// 部署Lilac合约
func deployLilac(client *sdk.ChainClient) error {
	// 初始化一个提案的参数列表
	aProposalArgs := []*common.KeyValuePair{}
	resp, err := DeployContract(client,
		config.LILAC_CONTRACT_NAME, config.LILAC_CONTRACT_VERSION,
		config.LILAC_CONTRACT_PATH, config.DOCKER_GO,
		aProposalArgs,
		true,
		config.CREATE_CONTRACT_TIMEOUT,
		config.Chain1Admins...)
	if err != nil {
		info := utils.MyLogRespErr("DeployCOntract", resp, err)
		return fmt.Errorf(info)
	}
	return nil
}

// 更新htlc合约
func upgradeLilacContract(client *sdk.ChainClient) error {
	// 初始化一个提案的参数列表
	aProposalArgs := []*common.KeyValuePair{}
	_, err := UpgradeContract(client,
		config.LILAC_CONTRACT_NAME, config.LILAC_CONTRACT_VERSION,
		config.LILAC_CONTRACT_PATH, config.DOCKER_GO,
		aProposalArgs,
		true,
		config.CREATE_CONTRACT_TIMEOUT,
		config.Chain1Admins...)
	if err != nil {
		return err
	}
	return nil
}

// 查询这个proposal
// @param client
// @proposalID 提案id
// @return proposal字符串, error
func getLilacProposal(client *sdk.ChainClient, proposalID string) (string, error) {
	args := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("getProposal"),
		},
		{
			Key:   "proposalID",
			Value: []byte(proposalID),
		},
	}
	// 查询链上proposal信息
	resp, err := QueryContract(client, config.LILAC_CONTRACT_NAME, args)
	if err != nil {
		info := utils.MyLogRespErr("QueryContract", resp, err)
		return "", fmt.Errorf(info)
	}
	return string(resp.ContractResult.Result), nil
}

// 发起一个Lilacproposal
// @param client 这个客户端发起
// @param toAddress 发钱给这个人
// @param amount 发这么多钱过去
// @param hashlock 哈希锁
// @param timelock 时间锁
// @return proposalID, error
func newLilacProposal(
	client *sdk.ChainClient,
	toAddress,
	amount string,
	hashlock []string,
	timelock string,
) (string, error) {
	hashlockList, err := json.Marshal(hashlock)
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	args := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("newProposal"),
		},
		{
			Key:   "receiver",
			Value: []byte(toAddress),
		},
		{
			Key:   "amount",
			Value: []byte(amount),
		},
		{
			Key:   "hashlock",
			Value: []byte(hashlockList),
		},
		{
			Key:   "timelock",
			Value: []byte(timelock),
		},
	}
	// 链上执行newProposal方法
	resp, err := InvokeContract(client, config.LILAC_CONTRACT_NAME, "", args, true)
	if err != nil {
		info := utils.MyLogRespErr("newLilacProposal", resp, err)
		return "", fmt.Errorf(info)
	}
	return string(resp.ContractResult.Result), nil
}

// 取钱
// @param client 通过哪一个客户端来调用这个函数
// @param proposalID 取哪一个proposal的钱
// @param key 原像
// @return 取钱以后的proposal字符串, error
func withdrawLilac(client *sdk.ChainClient, proposalID string, key []string) (string, error) {
	keyList, err := json.Marshal(key)
	if err != nil {
		fmt.Println(err)
		return "", err
	}
	args := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("withdraw"),
		},
		{
			Key:   "proposalID",
			Value: []byte(proposalID),
		},
		{
			Key:   "preimage",
			Value: []byte(keyList),
		},
	}
	// 链上执行withdraw方法
	resp, err := InvokeContract(client, config.LILAC_CONTRACT_NAME, "", args, true)
	if err != nil {
		info := utils.MyLogRespErr("InvokeContract", resp, err)
		return "", fmt.Errorf(info)
	}
	return string(resp.ContractResult.Result), nil
}

// 退钱
// @param client 客户端
// @param proposalID 从这个proposal退钱
func refundLilac(client *sdk.ChainClient, proposalID string) (string, error) {
	args := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("refund"),
		},
		{
			Key:   "proposalID",
			Value: []byte(proposalID),
		},
	}
	// 链上执行refund方法
	resp, err := InvokeContract(client, config.LILAC_CONTRACT_NAME, "", args, true)
	if err != nil {
		info := utils.MyLogRespErr("IncokeContract", resp, err)
		return "", fmt.Errorf(info)
	}
	return string(resp.ContractResult.Result), nil
}
