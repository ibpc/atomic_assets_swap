/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 订阅合约上的事件并通过通道返回结果
*/
package contractinvoke

import (
	"atomic_assets_swap/utils"
	"context"
	"fmt"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
)

// HTLC监听事件
// @param cc 发起监听的客户端
// @param contractName 合约名
// @param eventName 事件名
// @param resultChan 保存事件的通道
func subscribeEvent(cc *sdk.ChainClient, contractName, eventName string, resultChan chan<- interface{}) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	// 订阅并获取通道
	ec, err := cc.SubscribeContractEvent(ctx, -1, -1, contractName, eventName)
	if err != nil {
		utils.InfoError("subscribeHtlcProposal.cc.SubscribeContractEvent", err)
		return
	}
	for {
		select {
		case event, ok := <-ec:
			if !ok || event == nil {
				utils.InfoError("subscribeHtlcProposal.event", err)
				close(resultChan)
				return
			}
			contractEvent, ok := event.(*common.ContractEventInfo)
			if !ok {
				utils.Info("event type convert error")
				close(resultChan)
				return
			}
			resultChan <- contractEvent
		case <-ctx.Done():
			fmt.Println("上下文关闭,结束监听")
			close(resultChan)
			return
		}
	}
}
