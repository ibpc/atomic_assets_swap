/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 哈希时间锁合约调用相关函数，包括：
	1. 部署htlc合约
	2. 更新htlc合约
	3. 发起一个新的htlc proposal
	4. 根据proposalID获得这个proposal信息
	5. 根据proposalID对其进行退款操作
	6. 根据proposalID对其进行取款操作
*/
// coding:utf-8
//
package contractinvoke

import (
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"fmt"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
)

// 部署HTLC合约
// @param client 链客户端
// @return error err
func deployHTLC(client *sdk.ChainClient) error {
	// 初始化一个提案的参数列表
	aProposalArgs := []*common.KeyValuePair{}
	resp, err := DeployContract(client,
		config.HTLC_CONTRACT_NAME, config.HTLC_CONTRACT_VERSION,
		config.HTLC_CONTRACT_PATH, config.DOCKER_GO,
		aProposalArgs,
		true,
		config.CREATE_CONTRACT_TIMEOUT,
		config.Chain1Admins...)
	if err != nil {
		info := utils.MyLogRespErr("DeployCOntract", resp, err)
		return fmt.Errorf(info)
	}
	return nil
}

// 更新htlc合约
func upgradeHTLCContract(client *sdk.ChainClient) error {
	// 初始化一个提案的参数列表
	aProposalArgs := []*common.KeyValuePair{}
	resp, err := UpgradeContract(client,
		config.HTLC_CONTRACT_NAME, config.HTLC_CONTRACT_VERSION,
		config.HTLC_CONTRACT_PATH, config.DOCKER_GO,
		aProposalArgs,
		true,
		config.CREATE_CONTRACT_TIMEOUT,
		config.Chain1Admins...)
	if err != nil {
		utils.MyLogRespErr("UpgradeContract", resp, err)
		return err
	}
	return nil
}

// 发起一个proposal
// @param client 这个客户端发起
// @param toAddress 发钱给这个人
// @param amount 发这么多钱过去
// @param hashlock 哈希锁
// @param timelock 时间锁
// @return proposalID,error
func newHTLCProposal(
	client *sdk.ChainClient,
	toAddress,
	amount,
	hashlock,
	timelock string,
) (string, error) {
	args := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("newProposal"),
		},
		{
			Key:   "receiver",
			Value: []byte(toAddress),
		},
		{
			Key:   "amount",
			Value: []byte(amount),
		},
		{
			Key:   "hashlock",
			Value: []byte(hashlock),
		},
		{
			Key:   "timelock",
			Value: []byte(timelock),
		},
	}
	resp, err := InvokeContract(client, config.HTLC_CONTRACT_NAME, "", args, true)
	if err != nil {
		info := utils.MyLogRespErr("InvokeCOntract", resp, err)
		return "", fmt.Errorf(info)
	}
	return string(resp.ContractResult.Result), nil
}

// 查询这个proposal
// @param client
// @proposalID 提案id
// @return proposal字符串, error
func getHTLCProposal(client *sdk.ChainClient, proposalID string) (string, error) {
	args := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("getProposal"),
		},
		{
			Key:   "proposalID",
			Value: []byte(proposalID),
		},
	}
	// 查询链上proposal信息
	resp, err := QueryContract(client, config.HTLC_CONTRACT_NAME, args)
	if err != nil {
		info := utils.MyLogRespErr("QueryContract", resp, err)
		return "", fmt.Errorf(info)
	}
	return string(resp.ContractResult.Result), nil
}

// 取钱
// @param client 通过哪一个客户端来调用这个函数
// @param proposalID 取哪一个proposal的钱
// @param key 原像
// @return 取钱以后的proposal字符串, error
func withdrawHTLC(client *sdk.ChainClient, proposalID, key string) (string, error) {
	args := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("withdraw"),
		},
		{
			Key:   "proposalID",
			Value: []byte(proposalID),
		},
		{
			Key:   "preimage",
			Value: []byte(key),
		},
	}
	// 链上执行withdraw方法
	resp, err := InvokeContract(client, config.HTLC_CONTRACT_NAME, "", args, true)
	if err != nil {
		info := utils.MyLogRespErr("InvokeContract", resp, err)
		return "", fmt.Errorf(info)
	}
	return string(resp.ContractResult.Result), nil
}

// 退钱
// @param client 客户端
// @param proposalID 从这个proposal退钱
func refundHTLC(client *sdk.ChainClient, proposalID string) (string, error) {
	args := []*common.KeyValuePair{
		{
			Key:   "method",
			Value: []byte("refund"),
		},
		{
			Key:   "proposalID",
			Value: []byte(proposalID),
		},
	}
	// 链上执行refund方法
	resp, err := InvokeContract(client, config.HTLC_CONTRACT_NAME, "", args, true)
	if err != nil {
		info := utils.MyLogRespErr("IncokeContract", resp, err)
		return "", fmt.Errorf(info)
	}
	return string(resp.ContractResult.Result), nil
}
