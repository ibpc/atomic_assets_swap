/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 获取链ID
*/
package chainmakerclient

import "fmt"

// 获取chainID
// @return string id
// @return error err
func (c *ChainClient) GetChainID() (string, error) {
	chainConfig, err := c.mtClient.GetChainConfig()
	if err != nil {
		return "", fmt.Errorf("chainclient.GetChainInfo error: %s", err)
	}
	chainID := chainConfig.GetChainId()
	return chainID, nil
}
