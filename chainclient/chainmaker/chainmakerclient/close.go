/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 关闭客户端
*/
package chainmakerclient

// 关闭客户端
// chainmaker不需要，所以什么都不做
func (c *ChainClient) Close() {
}
