/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 根据proposalID以及proposal类型进行取款
*/
package chainmakerclient

import (
	"atomic_assets_swap/chainclient/chainmaker/contractinvoke"
	"atomic_assets_swap/utils"
	"fmt"
)

// 取钱
// @param proposalType proposal类型"htlc","lilac"
// @param proposalID 取这个proposal的钱
// @param preimage 原像
func (c *ChainClient) Withdraw(proposalType, proposlID string, preimage interface{}) error {
	switch proposalType {
	case "htlc":
		return c.withdrawHtlc(proposlID, preimage)
	case "lilac":
		return c.withdrawLilac(proposlID, preimage)
	default:
		return fmt.Errorf("invalid proposalType in chainClient.Withdraw: %s", proposalType)
	}
}

// 取htlc的钱
// @param proposalID 取这个proposal的钱
// @param preimage 原像
func (c *ChainClient) withdrawHtlc(proposlID string, preimage interface{}) error {
	preimageStr, ok := preimage.(string)
	if !ok {
		return fmt.Errorf("htlc preimage must be string")
	}
	str, err := contractinvoke.WithdrawHTLC(c.mtClient, proposlID, preimageStr)
	if err != nil {
		info := utils.InfoError("contractinvoke.WithdrawHTLC", err)
		return fmt.Errorf(info)
	}
	utils.Info(str)
	return nil
}

// 取lilac的钱
// @param proposalID 取这个proposal的钱
// @param preimage 原像
func (c *ChainClient) withdrawLilac(proposlID string, preimage interface{}) error {
	// 原像需要是字符串数组
	preimageStr, err := utils.Any2StringArray(preimage)
	if err != nil {
		return fmt.Errorf("withdrawLilac error: " + err.Error())
	}
	str, err := contractinvoke.WithdrawLilac(c.mtClient, proposlID, preimageStr)
	if err != nil {
		return fmt.Errorf("contractinvoke.WithdrawLilac error: " + err.Error())
	}
	utils.Info(str)
	return nil
}
