/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 测试
*/
package chainmakerclient

import (
	"atomic_assets_swap/chainclient/proposalstruct"
	"atomic_assets_swap/utils"
	"encoding/json"
	"reflect"
	"testing"

	"bou.ke/monkey"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/config"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
)

// 链客户端对象
var client *ChainClient

// 初始化, 打桩
func initFunc() {
	// 给sdk.NewCainClient打桩
	monkey.Patch(sdk.NewChainClient, func(...sdk.ChainClientOption) (*sdk.ChainClient, error) {
		result := sdk.ChainClient{}
		return &result, nil
	})

	// 给enablecerthash打桩
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&sdk.ChainClient{}),
		"EnableCertHash",
		func(*sdk.ChainClient) error {
			return nil
		})

	// 给InvokeContract打桩
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&sdk.ChainClient{}),
		"InvokeContract",
		func(*sdk.ChainClient,
			string, string, string, []*common.KeyValuePair, int64, bool,
		) (*common.TxResponse, error) {
			resp := common.TxResponse{}
			resp.Code = common.TxStatusCode_SUCCESS
			resp.ContractResult = &common.ContractResult{}
			resp.ContractResult.Result = []byte("InvokeContractResult")
			return &resp, nil
		})

	// invoke contract打桩补丁
	monkey.PatchInstanceMethod(reflect.TypeOf(&sdk.ChainClient{}),
		"InvokeContractWithLimit",
		func(*sdk.ChainClient,
			string, string, string, []*common.KeyValuePair, int64, bool,
			*common.Limit) (*common.TxResponse, error) {
			resp := common.TxResponse{}
			resp.Code = common.TxStatusCode_SUCCESS
			resp.ContractResult = &common.ContractResult{}
			resp.ContractResult.Result = []byte("InvokeContractResult")
			return &resp, nil
		})

	// 给QueryContract打桩
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&sdk.ChainClient{}),
		"QueryContract",
		func(*sdk.ChainClient,
			string, string, []*common.KeyValuePair, int64,
		) (*common.TxResponse, error) {
			resp := common.TxResponse{}
			resp.Code = common.TxStatusCode_SUCCESS
			resp.ContractResult = &common.ContractResult{}
			info, _ := json.Marshal(proposalstruct.Proposal{})
			resp.ContractResult.Result = info
			return &resp, nil
		})

	// GetContractInfo
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&sdk.ChainClient{}),
		"GetContractInfo",
		func(*sdk.ChainClient, string) (*common.Contract, error) {
			return &common.Contract{}, nil
		},
	)

	// GetChainConfig
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&sdk.ChainClient{}),
		"GetChainConfig",
		func(*sdk.ChainClient) (*config.ChainConfig, error) {
			result := config.ChainConfig{}
			result.ChainId = "chainid"
			return &result, nil
		},
	)
}

// 测试sdk客户端
func TestNewchainmakerclient(t *testing.T) {
	var err error
	client, err = NewClient("")
	if err != nil {
		panic(err)
	}
}

// 测试获取proposal
func TestGetProposal(t *testing.T) {
	// 类型错误
	if _, err := client.GetProposal("wrong type", "proposalid"); err == nil {
		panic("case wrong type error: err == nil error")
	}

	// 获取htlc合约
	if _, err := client.GetProposal("htlc", "proposalid"); err != nil {
		panic(err)
	}

	// 获取lilac合约
	if _, err := client.GetProposal("lilac", "proposalid"); err != nil {
		panic(err)
	}
}

// 测试发htlc proposal
func TestNewHtlcProposal(t *testing.T) {
	// 测试new htlc proposal
	hashlock := interface{}("hashlock")
	timelock := interface{}("1000")
	proposalID, err := client.NewProposal("htlc", "toaddr", "10", hashlock, timelock)
	if err != nil {
		panic(err)
	}
	utils.Info("proposal id: ", proposalID)

	// 时间锁错误
	if _, err := client.NewProposal("htlc", "toaddr", "10", hashlock, interface{}("timelock")); err == nil {
		panic("case timelock error: err != nil error")
	}

	// 数量错误
	if _, err := client.NewProposal("htlc", "toaddr", "amount", hashlock, timelock); err == nil {
		panic("case amount error: err != nil error")
	}

	// 类型错误
	if _, err := client.NewProposal("lilac", "toaddr", "10", hashlock, timelock); err == nil {
		panic("case amount error: err != nil error")
	}
}

// 测试发lilac proposal
func TestNewLilacProposal(t *testing.T) {
	// 测试new lilac proposal
	hashlock := utils.StringArray2Any([]string{"hash1", "hash2"})
	timelock := interface{}("1000")
	proposalID, err := client.NewProposal("lilac", "toaddr", "10", hashlock, timelock)
	if err != nil {
		panic(err)
	}
	utils.Info("proposal id: ", proposalID)

	// 时间锁错误
	if _, err := client.NewProposal("lilac", "toaddr", "10", hashlock, interface{}("timelock")); err == nil {
		panic("case timelock error: err != nil error")
	}

	// 数量错误
	if _, err := client.NewProposal("lilac", "toaddr", "amount", hashlock, timelock); err == nil {
		panic("case amount error: err != nil error")
	}

	// 类型错误
	if _, err := client.NewProposal("htlc", "toaddr", "10", hashlock, timelock); err == nil {
		panic("case amount error: err != nil error")
	}
}

// 测试解析proposal
func TestEventParse(t *testing.T) {
	// 类型错误
	if _, err := client.ParseEventToProposal("wrong type"); err == nil {
		panic("case wrong type of event error: err != nil error")
	}
	// 空事件
	event := &common.ContractEventInfo{}
	if _, err := client.ParseEventToProposal(event); err == nil {
		panic("case event type error: err != nil error")
	}
	// 测试 newProposal 事件解析
	event.Topic = "newProposal"
	if _, err := client.ParseEventToProposal(event); err == nil {
		panic("case eventData length error: err != nil error")
	}

	// json 错误
	event.EventData = []string{"proposalid", string([]byte("byte"))}
	if _, err := client.ParseEventToProposal(event); err == nil {
		panic("case json marshal error: err != nil error")
	}

	// 正常解析
	proposal := proposalstruct.Proposal{}
	jsonByte, err := json.Marshal(proposal)
	if err != nil {
		panic("json marshal error")
	}
	event.EventData = []string{"proposalID", string(jsonByte)}
	if _, err := client.ParseEventToProposal(event); err != nil {
		panic(err)
	}

	// 测试 withdraw 事件解析
	event.Topic = "withdraw"
	if _, err := client.ParseEventToProposal(event); err != nil {
		panic(err)
	}
}

// 测试 refund
func TestRefund(t *testing.T) {
	// proposal type 错误
	if err := client.Refund("wrong type", "proposalid"); err == nil {
		panic(err)
	}

	// refund htlc
	if err := client.Refund("htlc", "proposalid"); err != nil {
		panic(err)
	}

	// refund lilac
	if err := client.Refund("lilac", "proposalid"); err != nil {
		panic(err)
	}
}

// 测试withdraw
func TestWithdraw(t *testing.T) {
	// proposal type error
	if err := client.Withdraw("wrong type", "proposalid", "preimage"); err == nil {
		panic("case proposal type error: err == nil error")
	}

	// htlc preimage类型错误
	if err := client.Withdraw("htlc", "proposalid", 111); err == nil {
		panic("case htlc preimage error: err == nil error")
	}

	// 正常测试
	if err := client.Withdraw("htlc", "proposalid", "preimage"); err != nil {
		panic(err)
	}

	// lilac preimage错误
	if err := client.Withdraw("lilac", "proposalid", "preimage"); err == nil {
		panic(err)
	}

	// 正常测试
	preimage := utils.StringArray2Any([]string{"pre1", "pre2"})
	if err := client.Withdraw("lilac", "proposalid", preimage); err != nil {
		panic(err)
	}

}

// 其他简单测试
func TestOther(t *testing.T) {
	// 测试关闭客户端
	client.Close()

	// 测试获取地址
	if _, err := client.GetAddress(); err != nil {
		panic(err)
	}

	// 获取余额
	if _, err := client.GetBalance("address"); err != nil {
		panic(err)
	}

	// 获取链id
	if _, err := client.GetChainID(); err != nil {
		panic(err)
	}
}

func TestMain(m *testing.M) {
	utils.InfoTips("开始测试chainmaker SDK")
	// 初始化
	initFunc()
	// 执行
	m.Run()
	utils.InfoTips("chainmaker SDK测试完成")
}
