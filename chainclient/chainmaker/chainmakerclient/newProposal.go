/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 发起一个新的htlc proposal
*/
package chainmakerclient

import (
	"atomic_assets_swap/chainclient/chainmaker/contractinvoke"
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"fmt"
)

// 在链chainName发起一个proposalType的proposal, 参数为params
// @param proposalType 发起的proposal类型
// @param fromAddr 发起者地址
// @param toAddr 接收者地址
// @param amount 发送的数量
// @param hashlock 哈希锁
// @param timelock 时间锁, 单位秒
// @param string proposalID
// @param error err
func (c *ChainClient) NewProposal(proposalType, toAddr, amount string, hashlock, timelock interface{}) (string, error) {
	switch proposalType {
	case config.PROPOSAL_TYPE_HTLC:
		// htlc proposal
		if _, ok := hashlock.(string); !ok {
			return "", fmt.Errorf("hashlock is not string")
		}
		if _, ok := timelock.(string); !ok {
			return "", fmt.Errorf("timelock is not string")
		}
		proposalID, err := contractinvoke.NewHTLCProposal(c.mtClient, toAddr, amount, hashlock.(string), timelock.(string))
		if err != nil {
			return "", fmt.Errorf("ChainClient NewHtlcProposal error: %s", err)
		}
		return proposalID, nil
	case config.PROPOSAL_TYPE_LILAC:
		// lilac proposal
		currHashLock, err := utils.Any2StringArray(hashlock)
		if err != nil {
			return "", fmt.Errorf("ChainClient NewLilacProposal error: %s", err)
		}
		proposalID, err := contractinvoke.NewLilacProposal(c.mtClient, toAddr, amount, currHashLock, timelock.(string))
		if err != nil {
			return "", fmt.Errorf("ChainClient NewLilacProposal error: %s", err)
		}
		return proposalID, nil
	default:
		return "", fmt.Errorf("invalid case \"%s\"", proposalType)
	}
}
