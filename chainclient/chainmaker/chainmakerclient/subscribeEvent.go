/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 订阅合约的事件并加入到通道里面
*/
package chainmakerclient

import (
	"atomic_assets_swap/chainclient/chainmaker/contractinvoke"
)

// 监听一个链的事件
// @param contractName 被监听的合约名
// @param eventName 被监听的事件名称
// @param resultChan 返回监听的结果
func (c *ChainClient) SubscribeEvent(contractName, eventName string, resultChan chan<- interface{}) {
	contractinvoke.SubscribeEvent(c.mtClient, contractName, eventName, resultChan)
}
