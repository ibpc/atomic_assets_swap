/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 解析事件为指定的proposal结构
*/
package chainmakerclient

import (
	"atomic_assets_swap/chainclient/proposalstruct"
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"encoding/json"
	"fmt"

	"chainmaker.org/chainmaker/pb-go/v2/common"
)

// 解析事件信息为一个proposal
// @param eventInfo 对应链的事件信息
// @return proposalStruct 解析的proposal
// @return error 错误
func (c *ChainClient) ParseEventToProposal(eventInfo interface{}) (proposalstruct.Proposal, error) {
	event, ok := eventInfo.(*common.ContractEventInfo)
	if !ok {
		return proposalstruct.Proposal{}, fmt.Errorf("invalid event type")
	}
	switch event.Topic {
	case config.TOPIC_NEW_PROPOSAL:
		// 解析newProposal事件
		return c.parseNewProposalEvent(event)
	case config.TOPIC_WITHDRAW:
		// 解析withdraw事件
		return c.parseWithdrawEvent(event)
	default:
		return proposalstruct.Proposal{}, fmt.Errorf("invalid event topic:" + event.Topic)
	}
}

// 解析withdraw事件
// @param event 合约事件
// @return proposalStruct 解析的合约事件
// @return error 错误
func (c *ChainClient) parseWithdrawEvent(event *common.ContractEventInfo) (proposalstruct.Proposal, error) {
	// htlc和lilac的withdraw事件的处理也和newProposal相同
	return c.parseNewProposalEvent(event)
}

// 解析newProposal事件
// @param event 合约事件
// @return proposalStruct 解析的合约事件
// @return error 错误
func (c *ChainClient) parseNewProposalEvent(event *common.ContractEventInfo) (proposalstruct.Proposal, error) {
	// htlc和lilac的newProposal事件的返回处理都是这样
	var proposal proposalstruct.Proposal
	if len(event.EventData) <= 1 {
		return proposal, fmt.Errorf("length of eventData in chain1 must be greater than 1, get: %d", len(event.EventData))
	}
	proposalID := event.EventData[0]
	proposalJson := event.EventData[1]
	err := json.Unmarshal([]byte(proposalJson), &proposal)
	if err != nil {
		return proposal, fmt.Errorf("dealChainmakerNewHtlcProposalEvent error: " + err.Error())
	}
	proposal.ProposalID = proposalID
	proposal.ContractName = event.ContractName
	// topic表示事件名,合约名就是合约名
	proposal.ChainName, err = c.GetChainID()
	if err != nil {
		info := utils.InfoError("dealChainmakerNewHtlcProposalEvent.GetChainID error", err)
		return proposal, fmt.Errorf(info)
	}
	return proposal, nil
}
