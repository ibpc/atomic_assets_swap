/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 根据proposalID以及类型，执行退款程序
*/
package chainmakerclient

import (
	"atomic_assets_swap/chainclient/chainmaker/contractinvoke"
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"fmt"
)

// 撤销一个proposal
// @param proposalType proposal类型"htlc","lilac"
// @param proposalID 被撤销的id
func (c *ChainClient) Refund(proposalType, proposalID string) error {
	var f func(string) error
	switch proposalType {
	case config.PROPOSAL_TYPE_HTLC:
		f = c.refundHtlc
	case config.PROPOSAL_TYPE_LILAC:
		f = c.refundLilac
	default:
		return fmt.Errorf("invalid proposalType: %s", proposalType)
	}
	// 执行对应的函数
	return f(proposalID)
}

// 撤销一个htlc proposal
// @param proposalID 被撤销的id
func (c *ChainClient) refundHtlc(proposalID string) error {
	str, err := contractinvoke.RefundHTLC(c.mtClient, proposalID)
	if err != nil {
		info := utils.InfoError("contractinvoke.RefundHTLC", err)
		return fmt.Errorf(info)
	}
	utils.Info(str)
	return nil
}

// 撤销一个htlc proposal
// @param proposalID 被撤销的id
func (c *ChainClient) refundLilac(proposalID string) error {
	str, err := contractinvoke.RefundLilac(c.mtClient, proposalID)
	if err != nil {
		info := utils.InfoError("contractinvoke.RefundLilac", err)
		return fmt.Errorf(info)
	}
	utils.Info(str)
	return nil
}
