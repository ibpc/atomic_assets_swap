/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 根据proposalID获取proposal
*/
package chainmakerclient

import (
	"atomic_assets_swap/chainclient/chainmaker/contractinvoke"
	"atomic_assets_swap/chainclient/proposalstruct"
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"encoding/json"
	"fmt"
)

// 获取proposal
// @param proposalType proposal类型
// @param proposalID proposalID
// @return proposal 这个proposalID对应的proposal
// @return error err
func (c *ChainClient) GetProposal(proposalType, proposalID string) (proposalstruct.Proposal, error) {
	var proposalJson string
	var err error
	var contractName string
	switch proposalType {
	case config.HTLC_CONTRACT_NAME:
		// htlc合约
		proposalJson, err = contractinvoke.GetHTLCProposal(c.mtClient, proposalID)
		contractName = config.HTLC_CONTRACT_NAME
	case config.LILAC_CONTRACT_NAME:
		// lilac合约
		proposalJson, err = contractinvoke.GetLilacProposal(c.mtClient, proposalID)
		contractName = config.LILAC_CONTRACT_NAME
	default:
		proposalJson = ""
		err = fmt.Errorf("chainmaker GetProposal invalid proposalType: %s", proposalType)
	}
	if err != nil {
		return proposalstruct.Proposal{}, fmt.Errorf("chainmakerClient GetProposal error: " + err.Error())
	}

	// 解析proposalJson
	proposal := proposalstruct.Proposal{}
	err = json.Unmarshal([]byte(proposalJson), &proposal)
	if err != nil {
		info := utils.InfoError("GetProposal unmarshal", err)
		return proposal, fmt.Errorf(info)
	}
	proposal.ProposalID = proposalID
	proposal.ContractName = contractName
	proposal.ChainName, err = c.GetChainID()
	if err != nil {
		info := utils.InfoError("chainmakerClient.GetChainID", err)
		return proposal, fmt.Errorf(info)
	}
	return proposal, nil
}
