/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 获取当前用户余额
*/
package chainmakerclient

import (
	"atomic_assets_swap/chainclient/chainmaker/contractinvoke"
	"fmt"
)

// 获取余额
// @param address 地址,如果为""则返回自己的余额
// @return string 余额
// @return error err
func (c *ChainClient) GetBalance(address string) (string, error) {
	// 如果为空则返回自己的余额
	if address == "" {
		address = c.msAddress
	}
	balance, err := contractinvoke.GetBalance(c.mtClient, address)
	if err != nil {
		return "", fmt.Errorf("chainmaker.GetBalance: %s", err)
	}
	return balance, nil
}
