/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 获得一个适配长安链的SDK对象
*/
// Package chainmakerclient 长安链上实现客户端接口
package chainmakerclient

import (
	"atomic_assets_swap/chainclient/chainmaker/contractinvoke"
	"atomic_assets_swap/utils"
	"fmt"

	sdk "chainmaker.org/chainmaker/sdk-go/v2"
)

// 链客户端
type ChainClient struct {
	// 当前用户配置文件路径, 确定当前客户端用户
	msConfigPath string
	// 与链交互的客户端
	mtClient *sdk.ChainClient
	// 当前客户端地址
	msAddress string
}

// 创建链客户端
// @param sdkConfPath:客户端sdk路径
func createClient(sdkConfPath string) (*sdk.ChainClient, error) {
	cc, err := sdk.NewChainClient(
		sdk.WithConfPath(sdkConfPath),
		sdk.WithEnableTxResultDispatcher(true),
	)
	if err != nil {
		info := utils.InfoError("sdk.NewChainClient", err)
		return nil, fmt.Errorf(info)
	}
	if cc.GetAuthType() == sdk.PermissionedWithCert {
		if err := cc.EnableCertHash(); err != nil {
			info := utils.InfoError("cc.EnableCertHash", err)
			return nil, fmt.Errorf(info)
		}
	}
	return cc, nil
}

// 一个新的chainmaker与链交互的客户端
// @param configPath 配置文件路径
func NewClient(configPath string) (*ChainClient, error) {
	ptResult := &ChainClient{}
	ptResult.msConfigPath = configPath

	// 建立与链交互的客户端
	currChainClient, err := createClient(configPath)
	if err != nil {
		utils.Info("configPath: " + configPath)
		info := utils.InfoError("chainmakerClient NewClient CreateClient", err)
		return nil, fmt.Errorf(info)
	}
	ptResult.mtClient = currChainClient

	// fmt.Println("重置余额")
	if _, err := contractinvoke.ResetBalance(currChainClient); err != nil {
		info := utils.InfoError("chainmaker reset balance", err)
		return nil, fmt.Errorf(info)
	}

	// 初始化地址值
	ptResult.msAddress = ""
	if _, err := ptResult.GetAddress(); err != nil {
		return nil, fmt.Errorf("chainmakerClient.NewClient.GetAddress error: %s", err)
	}
	return ptResult, nil
}
