/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 获得当前用户的地址
*/
package chainmakerclient

import (
	"atomic_assets_swap/chainclient/chainmaker/contractinvoke"
	"fmt"
)

// 获取地址
// @return string 地址
// @return error err
func (c *ChainClient) GetAddress() (string, error) {
	if c.msAddress == "" {
		// 获取地址
		address, err := contractinvoke.GetAddress(c.mtClient)
		if err != nil {
			return "", fmt.Errorf("chainmakerClient.GetAddress error: %s", err)
		}
		// 赋值
		c.msAddress = address
	}
	return c.msAddress, nil
}
