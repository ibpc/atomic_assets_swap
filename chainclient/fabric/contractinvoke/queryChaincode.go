/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 查询已经安装的链码
*/
package contractinvoke

import (
	"atomic_assets_swap/utils"
	"fmt"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/resmgmt"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
)

// 查询已经安装的链码
func (c *FabricClient) QueryChaincode() ([]string, error) {
	adminContext := c.FabricSdk.Context(fabsdk.WithUser(c.Org.OrgAdminUser), fabsdk.WithOrg(c.Org.OrgName))
	orgResMgmt, err := resmgmt.New(adminContext)
	if err != nil {
		info := utils.InfoError("queryChaincode new resmgmt", err)
		return []string{}, fmt.Errorf(info)
	}

	ls_result := []string{}
	configBackend, err := c.FabricSdk.Config()
	if err != nil {
		info := utils.InfoError("sdk.config", err)
		return []string{}, fmt.Errorf(info)
	}
	// 获取peer
	targets, err := orgTargetPeers([]string{c.Org.OrgName}, configBackend)
	if err != nil {
		info := utils.InfoError("orgTargetPeers", err)
		return []string{}, fmt.Errorf(info)
	}
	if len(targets) == 0 {
		return []string{}, fmt.Errorf("length of peers is 0")
	}
	// 查询链码
	peer := targets[0]
	resp, _ := orgResMgmt.QueryInstalledChaincodes(resmgmt.WithTargetEndpoints(peer))

	for _, ccInfo := range resp.Chaincodes {
		// 保存链码信息
		ls_result = append(ls_result, ccInfo.Name)
	}

	return ls_result, nil
}
