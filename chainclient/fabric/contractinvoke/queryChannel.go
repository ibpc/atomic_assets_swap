/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 查询用户加入的通道
*/
package contractinvoke

import (
	"atomic_assets_swap/utils"
	"fmt"
	"log"
	"strings"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/resmgmt"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/errors/retry"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/core"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/fab"
	"github.com/hyperledger/fabric-sdk-go/pkg/core/config/lookup"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
)

// 通过组织名获取user.yaml配置的组织对应的docker容器配置的peer名字
// @param orgs 组织名
// @param ocnfigBackend ...
// @return 这个组织对应peer名字
func orgTargetPeers(orgs []string, configBackend ...core.ConfigBackend) ([]string, error) {
	networkConfig := fab.NetworkConfig{}
	err := lookup.New(configBackend...).UnmarshalKey("organizations", &networkConfig.Organizations)
	if err != nil {
		return nil, fmt.Errorf("failed to get organizations from config: %s", err)
	}

	var peer []string
	for _, org := range orgs {
		orgConfig, ok := networkConfig.Organizations[strings.ToLower(org)]
		if !ok {
			continue
		}
		peer = append(peer, orgConfig.Peers...)
	}
	return peer, nil
}

// 查询通道
// @return 当前组织加入的通道
func (c *FabricClient) QueryChannel() ([]string, error) {
	configBackend, err := c.FabricSdk.Config()
	if err != nil {
		log.Fatalf("failed to get config backendfrom sdk: %s", err)
	}
	targets, err := orgTargetPeers([]string{c.Org.OrgName}, configBackend)
	if err != nil {
		log.Fatalf("creating peers failed: %s", err)
	}

	resMgmtClient, err := resmgmt.New(
		c.FabricSdk.Context(
			fabsdk.WithUser(c.Org.OrgAdminUser),
			fabsdk.WithOrg(c.Org.OrgName),
		),
	)
	if err != nil {
		info := utils.InfoError("QueryChannel resmgmt.New", err)
		return []string{}, fmt.Errorf(info)
	}

	// 查询通道信息
	channelQueryResp, err := resMgmtClient.QueryChannels(
		resmgmt.WithTargetEndpoints(targets[0]),
		resmgmt.WithRetry(retry.DefaultChannelOpts),
	)
	if err != nil {
		info := utils.InfoError("QueryChannel", err)
		return []string{}, fmt.Errorf(info)
	}

	ls_result := []string{}
	for _, channel := range channelQueryResp.Channels {
		// 保存通道信息
		ls_result = append(ls_result, channel.ChannelId)
	}

	return ls_result, nil
}
