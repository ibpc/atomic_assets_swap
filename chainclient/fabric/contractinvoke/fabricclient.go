/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 实例化一个与fabric交互的sdk对象
*/
package contractinvoke

import (
	myConfig "atomic_assets_swap/config/fabric"
	"atomic_assets_swap/utils"
	"fmt"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/event"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/ledger"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/msp"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/resmgmt"
	"github.com/hyperledger/fabric-sdk-go/pkg/core/config"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
)

// 链客户端
type FabricClient struct {
	ChannelID       string            // 客户端交互的通道名称
	ChannelConfig   string            // 通道配置文件
	Org             *myConfig.OrgInfo // 当前客户端所属组织
	ChaincodeClient *channel.Client   // 链码调用客户端
	LedgerClient    *ledger.Client    // 账本客户端
	EventClient     *event.Client     // 事件客户端

	// fabric sdk
	FabricSdk *fabsdk.FabricSDK

	//排序节点信息
	Orderer myConfig.OrdererInfo
}

// 初始化fabric sdk
// @param configFile 配置文件路径
// @param info 通道信息
func InitFabricSdk(configFile string, info *myConfig.ChannelInfo) (*fabsdk.FabricSDK, error) {
	//根据myConfig.yaml配置sdk
	sdk, err1 := fabsdk.New(config.FromFile(configFile))
	if err1 != nil {
		info := utils.InfoError("InitFabricSdk fabsdk.New FromFile", err1)
		return nil, fmt.Errorf(info)
	}
	//根据不同的组织上下文，配置资源管理客户端(resource management client)
	for _, org := range info.Orgs {
		mspClient, err := msp.New(sdk.Context(), msp.WithOrg(org.OrgName))
		if err != nil {
			info := utils.InfoError("InitFabricSdk msp.New", err)
			return nil, fmt.Errorf(info)
		}
		org.OrgMspClient = mspClient
		orgContext := sdk.Context(fabsdk.WithUser(org.OrgAdminUser), fabsdk.WithOrg(org.OrgName))
		resMgmtclient, err := resmgmt.New(orgContext)
		if err != nil {
			info := utils.InfoError("InitFabricSdk sdk.Context", err)
			return nil, fmt.Errorf(info)
		}
		org.OrgResMgmt = resMgmtclient
	}
	//配置通道客户端
	ordererClientContext := sdk.Context(
		fabsdk.WithUser(info.Orderer.OrdererAdminUser),
		fabsdk.WithOrg(info.Orderer.OrdererOrgName))
	chClient, err := resmgmt.New(ordererClientContext)
	if err != nil {
		return nil, fmt.Errorf("channel management client create error: %v", err)
	}
	info.Orderer.ChMgmtClient = chClient
	return sdk, nil
}

// 建立三个客户端
// 链码客户端
// 账本客户端
// 事件客户端
func (c *FabricClient) InitClient() error {
	// 建立并返回通道上下文
	ccp := c.FabricSdk.ChannelContext(c.ChannelID, fabsdk.WithUser(c.Org.OrgUser), fabsdk.WithOrg(c.Org.OrgName))
	var err error
	// 链码客户端
	c.ChaincodeClient, err = channel.New(ccp)
	if err != nil {
		return fmt.Errorf("创建链码客户端失败： %v", err.Error())
	}
	// 查询区块信息
	c.LedgerClient, err = ledger.New(ccp)
	if err != nil {
		return fmt.Errorf("创建账本客户端失败： %v", err.Error())
	}

	// 初始化事件客户端
	c.EventClient, err = event.New(ccp, event.WithBlockEvents())
	if err != nil {
		return fmt.Errorf("创建事件客户端失败： %v", err.Error())
	}
	return nil
}

// 获取新的客户端对象
// @param configYamlPath yaml配置文件
// @param channelInfo 通道信息
func NewClient(configYamlPath, orgName, userName string) (*FabricClient, error) {
	utils.Info("配置文件:", configYamlPath)
	ptResult := FabricClient{}

	channelInfo := myConfig.GetChannelInfo()

	ptResult.ChannelID = channelInfo.ChannelID
	ptResult.ChannelConfig = channelInfo.ChannelConfig
	ptResult.Org = myConfig.GetOrgFromNameAndUser(orgName, userName)
	if ptResult.Org == nil {
		return nil, fmt.Errorf("get nil Org object, orgName: %s", orgName)
	}

	// 这里会给channelInfo里面的order赋值
	mySdk, err := InitFabricSdk(configYamlPath, channelInfo)
	if err != nil {
		return nil, fmt.Errorf("initFabricSdk error: %v", err)
	}
	ptResult.FabricSdk = mySdk
	return &ptResult, nil
}
