/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 关闭fabric sdk与链的连接
*/
package contractinvoke

// 关闭sdk与链的连接
func (c *FabricClient) Close() {
	c.FabricSdk.Close()
}
