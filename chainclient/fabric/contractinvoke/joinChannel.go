/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 加入通道
*/
package contractinvoke

import (
	config "atomic_assets_swap/config/fabric"
	"atomic_assets_swap/utils"
	"fmt"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/resmgmt"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/errors/retry"
)

// 加入通道
// @parma info 通道信息
func (c *FabricClient) JoinChannel(info *config.ChannelInfo) error {
	for _, org := range info.Orgs {
		// 每个组织都加入通道
		if err := org.OrgResMgmt.JoinChannel(info.ChannelID,
			resmgmt.WithRetry(retry.DefaultResMgmtOpts),
			resmgmt.WithOrdererEndpoint(info.Orderer.OrdererEndpoint)); err != nil {
			return fmt.Errorf("%s peers failed to JoinChannel: %v", org.OrgName, err)
		}
	}
	utils.Info("加入通道成功...")
	return nil
}
