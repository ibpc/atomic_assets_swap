/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 创建和加入通道，包括：
1. 创建通道
2. 获得群签名
3. 创建并加入通道
*/
package contractinvoke

import (
	config "atomic_assets_swap/config/fabric"
	"atomic_assets_swap/utils"
	"fmt"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/resmgmt"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/errors/retry"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/msp"
)

// 创建通道
// @params signalIDs 群签名
// @param info 通道信息
func CreateChannel(signalIDs []msp.SigningIdentity, info *config.ChannelInfo) error {
	var err error
	//创建通道请求
	req := resmgmt.SaveChannelRequest{
		ChannelID:         info.ChannelID,
		ChannelConfigPath: info.ChannelConfig,
		SigningIdentities: signalIDs,
	}
	_, err = info.Orderer.ChMgmtClient.SaveChannel(
		req,
		resmgmt.WithRetry(retry.DefaultResMgmtOpts),
		resmgmt.WithOrdererEndpoint(info.Orderer.OrdererEndpoint))
	if err != nil {
		return fmt.Errorf("error should be nil for SaveChannel of orgchannel: %v", err)
	}
	utils.Info("使用每个org的管理员身份更新锚节点配置...")
	for i, org := range info.Orgs {
		req = resmgmt.SaveChannelRequest{
			ChannelID:         info.ChannelID,
			ChannelConfigPath: org.OrgAnchorFile,
			SigningIdentities: []msp.SigningIdentity{signalIDs[i]},
		}
		_, err = info.Orderer.ChMgmtClient.SaveChannel(req, resmgmt.WithRetry(retry.DefaultResMgmtOpts),
			resmgmt.WithOrdererEndpoint(info.Orderer.OrdererEndpoint))
		if err != nil {
			return fmt.Errorf("saveChannel for anchor org %s error: %v", org.OrgName, err)
		}
	}
	utils.Info("锚节点更新完成...")
	return nil
}

// 群签名
// @param info 通道信息
func GetSignalIdentities(info *config.ChannelInfo) (signalIDs []msp.SigningIdentity, err error) {
	signalIDs = []msp.SigningIdentity{}
	for _, org := range info.Orgs {
		orgSignalID, err := org.OrgMspClient.GetSigningIdentity(org.OrgAdminUser)
		if err != nil {
			return signalIDs, fmt.Errorf("GetSignalIdentities error %v", err)
		}
		signalIDs = append(signalIDs, orgSignalID)
	}
	return signalIDs, nil
}

// 创建并加入通道
// @param channelInfor 通道信息
func (c *FabricClient) CreateAndJoinChannel(info *config.ChannelInfo) error {
	signalIDs, err := GetSignalIdentities(info)
	if err != nil {
		return fmt.Errorf("create and join channle error %v", err)
	}
	// 创建通道
	err = CreateChannel(signalIDs, info)
	if err != nil {
		return fmt.Errorf("create channle error %v", err)
	}
	// 加入通道
	err = c.JoinChannel(info)
	if err != nil {
		return fmt.Errorf("join channle error %v", err)
	}
	return nil
}
