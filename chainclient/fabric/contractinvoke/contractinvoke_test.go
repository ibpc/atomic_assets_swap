/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 测试
*/
package contractinvoke

import (
	"atomic_assets_swap/utils"
	"reflect"
	"testing"

	fabConfig "atomic_assets_swap/config/fabric"

	"github.com/hyperledger/fabric-protos-go/common"
	"github.com/hyperledger/fabric-protos-go/peer"
	mspctx "github.com/hyperledger/fabric-sdk-go/pkg/common/providers/msp"
	"github.com/hyperledger/fabric-sdk-go/pkg/fab/ccpackager/gopackager"
	"github.com/hyperledger/fabric-sdk-go/pkg/fab/resource"
	"github.com/hyperledger/fabric-sdk-go/third_party/github.com/hyperledger/fabric/common/policydsl"

	"bou.ke/monkey"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/event"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/ledger"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/msp"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/resmgmt"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/context"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/core"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
)

// 链SDK
var client *FabricClient

func initFunc() {
	// fabsdk.New
	monkey.Patch(fabsdk.New, func(core.ConfigProvider, ...fabsdk.Option) (*fabsdk.FabricSDK, error) {
		result := fabsdk.FabricSDK{}
		return &result, nil
	})
	// msp.New
	monkey.Patch(msp.New, func(context.ClientProvider, ...msp.ClientOption) (*msp.Client, error) {
		result := msp.Client{}
		return &result, nil
	})
	// resmgmt.New
	monkey.Patch(resmgmt.New, func(context.ClientProvider, ...resmgmt.ClientOption) (*resmgmt.Client, error) {
		result := resmgmt.Client{}
		return &result, nil
	})
	// channel.New
	monkey.Patch(channel.New, func(context.ChannelProvider, ...channel.ClientOption) (*channel.Client, error) {
		result := channel.Client{}
		return &result, nil
	})
	// ledger.New
	monkey.Patch(ledger.New, func(context.ChannelProvider, ...ledger.ClientOption) (*ledger.Client, error) {
		result := ledger.Client{}
		return &result, nil
	})
	// event.New
	monkey.Patch(event.New, func(context.ChannelProvider, ...event.ClientOption) (*event.Client, error) {
		result := event.Client{}
		return &result, nil
	})
	// GetSignalIdentities
	monkey.Patch(GetSignalIdentities, func(*fabConfig.ChannelInfo) ([]mspctx.SigningIdentity, error) {
		signalIDs := []mspctx.SigningIdentity{}
		return signalIDs, nil
	})
	// orgTargetPeers
	monkey.Patch(orgTargetPeers, func([]string, ...core.ConfigBackend) ([]string, error) {
		result := []string{"peer1", "peer2"}
		return result, nil
	})
	// gopackager.NewCCPackage
	monkey.Patch(gopackager.NewCCPackage, func(string, string) (*resource.CCPackage, error) {
		result := resource.CCPackage{}
		return &result, nil
	})
	// policydsl.FromString
	monkey.Patch(policydsl.FromString, func(string) (*common.SignaturePolicyEnvelope, error) {
		result := common.SignaturePolicyEnvelope{}
		return &result, nil
	})

	// FabricSDK.Close
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&fabsdk.FabricSDK{}),
		"Close",
		func(*fabsdk.FabricSDK) {
		})
	// FabricSDK.Close
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&resmgmt.Client{}),
		"SaveChannel",
		func(*resmgmt.Client, resmgmt.SaveChannelRequest, ...resmgmt.RequestOption,
		) (resmgmt.SaveChannelResponse, error) {
			result := resmgmt.SaveChannelResponse{}
			return result, nil
		})
	// channel.client.Execute
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&channel.Client{}),
		"Execute",
		func(*channel.Client, channel.Request, ...channel.RequestOption,
		) (channel.Response, error) {
			result := channel.Response{}
			return result, nil
		})
	// channel.client.Execute
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&fabsdk.FabricSDK{}),
		"Config",
		func(*fabsdk.FabricSDK) (core.ConfigBackend, error) {
			return nil, nil
		})
	// resmgmt.Client.QueryInstalledChaincodes
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&resmgmt.Client{}),
		"QueryInstalledChaincodes",
		func(*resmgmt.Client, ...resmgmt.RequestOption,
		) (*peer.ChaincodeQueryResponse, error) {
			result := peer.ChaincodeQueryResponse{}
			return &result, nil
		})
	// resmgmt.Client.QueryChannels
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&resmgmt.Client{}),
		"QueryChannels",
		func(*resmgmt.Client, ...resmgmt.RequestOption,
		) (*peer.ChannelQueryResponse, error) {
			result := peer.ChannelQueryResponse{}
			result.Channels = []*peer.ChannelInfo{}
			result.Channels = append(result.Channels, func() []*peer.ChannelInfo {
				id1 := &peer.ChannelInfo{}
				id1.ChannelId = "channelid"
				return []*peer.ChannelInfo{id1}
			}()...)
			return &result, nil
		})
	// resmgmt.Client.JoinChannel
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&resmgmt.Client{}),
		"JoinChannel",
		func(*resmgmt.Client, string, ...resmgmt.RequestOption,
		) error {
			return nil
		})
}

// 测试sdk客户端
func TestNewClient(t *testing.T) {
	var err error
	client, err = NewClient("", "orgname", "username")
	if err != nil {
		panic(err)
	}
}

// 测试初始化客户端
func TestInitCLient(t *testing.T) {
	if err := client.InitClient(); err != nil {
		panic(err)
	}
}

// 测试创建加入通道
func TestCreateAndJoinChannel(t *testing.T) {
	if err := client.CreateAndJoinChannel(&fabConfig.ChannelInfo{}); err != nil {
		panic(err)
	}
}

// 测试部署链码
func TestDeployChaincode(t *testing.T) {
	// 空链码信息错误
	info := fabConfig.ChainCodeInfo{}
	if err := client.DeployChaincode(&info); err == nil {
		panic("链码信息为空不报错 error: err == nil error")
	}

	// 空链码信息错误
	if err := client.DeployChaincodeWithArgs(&info, [][]byte{}); err == nil {
		panic("链码信息为空不报错 error: err == nil error")
	}

}

// 测试调用合约
func TestInvokeFunction(t *testing.T) {
	if _, err := client.InvokeFunction("chaincodename",
		"funcname", []string{"arg1"}, "peer1", "pee2"); err != nil {
		panic(err)
	}
}

// 测试加入通道
func TestJoinChannel(t *testing.T) {
	if err := client.JoinChannel(fabConfig.GetChannelInfo()); err != nil {
		panic(err)
	}
}

// 测试查询安装的链码
func TestQueryChaincode(t *testing.T) {
	if _, err := client.QueryChaincode(); err != nil {
		panic(err)
	}
}

// 测试查询通道
func TestQueryChannel(t *testing.T) {
	if _, err := client.QueryChannel(); err != nil {
		panic(err)
	}
}

// 其他功能测试
func TestOtherFunc(t *testing.T) {
	client.Close()
}

func TestMain(m *testing.M) {
	utils.InfoTips("开始测试 fabric 合约调用功能")
	initFunc()
	m.Run()
	utils.InfoTips("fabric 合约调用功能测试完成")
}
