/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 调用合约函数
*/
package contractinvoke

import (
	"atomic_assets_swap/utils"
	"fmt"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
)

// 调用合约函数
// @param chaincodeName 合约名
// @param funcName 函数名
// @param args 参数
// @param peers todo:
func (c *FabricClient) InvokeFunction(
	chaincodeName string,
	funcName string,
	args []string,
	peers ...string,
) (channel.Response, error) {
	utils.Info("call", chaincodeName, funcName)
	arg_bytes := utils.StrArray2ByteArray(args)
	req := channel.Request{
		ChaincodeID: chaincodeName,
		Fcn:         funcName,
		Args:        arg_bytes,
	}
	// 调用合约
	resp, err := c.ChaincodeClient.Execute(req, channel.WithTargetEndpoints(peers...))
	if err != nil {
		return channel.Response{}, fmt.Errorf("invokeFunction:%v error:%v", funcName, err.Error())
	}
	return resp, nil
}
