/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 安装、实例化链码, 包括:
1. 安装链码
2. 实例化链码
3. 安装且实例化链码
*/
package contractinvoke

import (
	config "atomic_assets_swap/config/fabric"
	"atomic_assets_swap/utils"
	"fmt"

	"github.com/hyperledger/fabric-sdk-go/pkg/client/resmgmt"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/errors/retry"
	"github.com/hyperledger/fabric-sdk-go/pkg/fab/ccpackager/gopackager"
	"github.com/hyperledger/fabric-sdk-go/third_party/github.com/hyperledger/fabric/common/policydsl"
)

// 安装链码
func InstallChaincode(info *config.ChainCodeInfo) error {
	// 打包链码
	ccPkg, err := gopackager.NewCCPackage(
		info.ChaincodePath,
		info.ChaincodeGoPath)
	if err != nil {
		return fmt.Errorf("chaincode 打包失败:%v", err)
	}
	utils.Info("链码打包成功...")
	// 安装链码
	req := resmgmt.InstallCCRequest{
		Name:    info.ChaincodeID,
		Version: info.ChaincodeVersion,
		Path:    info.ChaincodePath,
		Package: ccPkg,
	}
	for _, org := range info.Orgs {
		// 链码安装到这些组织下的这些peer里面
		_, err := org.OrgResMgmt.InstallCC(
			req,
			resmgmt.WithTargetEndpoints(org.Peers...),
			resmgmt.WithRetry(retry.DefaultResMgmtOpts))
		if err != nil {
			return fmt.Errorf("链码安装失败: %v", err)
		}
		utils.Info("链码安装成功", org.OrgName)
	}
	return nil
}

// @param 实例化一个链码信息
func InstantiateChaincodeWithArgs(info *config.ChainCodeInfo, args [][]byte) error {
	ccPolicy, err := policydsl.FromString(info.ChaincodePolicy)
	if err != nil {
		return fmt.Errorf("背书策略生成失败")
	}
	instantiateReq := resmgmt.InstantiateCCRequest{
		Name:    info.ChaincodeID,
		Path:    info.ChaincodePath,
		Version: info.ChaincodeVersion,
		Policy:  ccPolicy,
		Args:    args,
	}
	if len(info.Orgs) == 0 {
		return fmt.Errorf("链码组织信息为空错误")
	}
	org := info.Orgs[0]
	_, err = org.OrgResMgmt.InstantiateCC(info.ChannelID, instantiateReq, resmgmt.WithRetry(retry.DefaultResMgmtOpts))
	if err != nil {
		return fmt.Errorf("实例化失败:%v", err)
	}
	utils.Info("链码实例化成功!\n")
	return nil
}

// 链码实例化
func InstantiateChaincode(info *config.ChainCodeInfo) error {
	//指定背书策略
	ccPolicy, err := policydsl.FromString(info.ChaincodePolicy)
	if err != nil {
		return fmt.Errorf("背书策略生成失败")
	}
	instantiateReq := resmgmt.InstantiateCCRequest{
		Name:    info.ChaincodeID,
		Path:    info.ChaincodePath,
		Version: info.ChaincodeVersion,
		Policy:  ccPolicy,
		Args:    [][]byte{[]byte("init")},
	}
	if len(info.Orgs) == 0 {
		return fmt.Errorf("组织信息为空错误")
	}
	org := info.Orgs[0]
	_, err = org.OrgResMgmt.InstantiateCC(info.ChannelID, instantiateReq, resmgmt.WithRetry(retry.DefaultResMgmtOpts))
	if err != nil {
		return fmt.Errorf("实例化失败:%v", err)
	}
	utils.Info("链码实例化成功!")
	return nil
}

// 部署链码
// @param info 链码信息
func (c *FabricClient) DeployChaincode(info *config.ChainCodeInfo) error {
	if err := InstallChaincode(info); err != nil {
		return fmt.Errorf("InstallChaincode: %s", err)
	}
	if err := InstantiateChaincode(info); err != nil {
		return fmt.Errorf("InstantiateChaincode: %s", err)
	}
	return nil
}

// 部署链码
// @param info 链码信息
func (c *FabricClient) DeployChaincodeWithArgs(info *config.ChainCodeInfo, args [][]byte) error {
	// 部署链码
	if err := InstallChaincode(info); err != nil {
		return fmt.Errorf("InstallChaincode: %s", err)
	}
	// 实例化链码
	if err := InstantiateChaincodeWithArgs(info, args); err != nil {
		return fmt.Errorf("InstantiateChaincodeWithArgs: %s", err)
	}
	return nil
}
