/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 封装fabric sdk对象
*/
// Package fabricclient 实现用户客户端接口的fabric sdk对象
package fabricclient

import (
	"atomic_assets_swap/chainclient/fabric/contractinvoke"
	fabConfig "atomic_assets_swap/config/fabric"
	"atomic_assets_swap/utils"
	"fmt"
	"strings"
)

// 链客户端
type ChainClient struct {
	// 当前客户端的用户名
	msUserName string
	// 客户端所属组织
	msOrgName string
	// 当前用户配置文件路径, 确定当前客户端用户
	msConfigPath string
	// 当前用户客户端地址
	msAddress string
	// 与fabric链交互的客户端
	mptFabricClient *contractinvoke.FabricClient
	// 客户端对应的endorser
	masEndorserPeer []string
}

// 获取新的客户端对象
// @param configYamlPath yaml配置文件
func NewClient(configInfos []string) (*ChainClient, error) {
	if len(configInfos) != 3 {
		info := utils.InfoError("fabric client NewClient",
			fmt.Errorf("length of fabric configInfos must equal to 3, but get %d", len(configInfos)),
		)
		return nil, fmt.Errorf(info)
	}
	ptResult := &ChainClient{}
	ptResult.msConfigPath = configInfos[0]
	ptResult.msOrgName = configInfos[1]
	ptResult.msUserName = strings.ToUpper(string(configInfos[2][0])) + configInfos[2][1:]
	ptResult.masEndorserPeer = []string{
		"peer1.org1.raw.com",
		"peer1.org2.raw.com",
	}

	// 建立与链交互的客户端
	currChainClient, err := contractinvoke.NewClient(ptResult.msConfigPath, ptResult.msOrgName, ptResult.msUserName)
	if err != nil {
		info := utils.InfoError("fabric NewClient", err)
		return nil, fmt.Errorf("%s", info)
	}
	ptResult.mptFabricClient = currChainClient

	// 查询通道，确定有指定的通道
	flag := false
	ls_channel, err := ptResult.mptFabricClient.QueryChannel()
	if err != nil {
		return nil, fmt.Errorf("query channel error: " + err.Error())
	}
	for _, id := range ls_channel {
		if id == fabConfig.GetChannelInfo().ChannelID {
			flag = true
			break
		}
	}

	// 如果通道不存在就创建并加入通道
	if !flag {
		// 创建并加入通道
		if err := currChainClient.CreateAndJoinChannel(fabConfig.GetChannelInfo()); err != nil {
			return nil, fmt.Errorf("create channel error:" + err.Error())
		}
	}

	// 初始化fabric sdk客户端
	if err := ptResult.mptFabricClient.InitClient(); err != nil {
		info := utils.InfoError("init fabric client", err)
		return nil, fmt.Errorf(info)
	}

	// 初始化地址
	ptResult.msAddress = ""
	if _, err := ptResult.GetAddress(); err != nil {
		return nil, fmt.Errorf("fabricClient.NewClient.GetAddress error: " + err.Error())
	}

	// 重置余额
	if err := ptResult.ResetBalance(); err != nil {
		return nil, fmt.Errorf("fabricClient.NewClient.ResetBalance error: " + err.Error())
	}

	return ptResult, nil
}
