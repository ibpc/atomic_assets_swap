/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 测试
*/
package fabricclient

import (
	"atomic_assets_swap/chainclient/fabric/contractinvoke"
	"atomic_assets_swap/chainclient/proposalstruct"
	"atomic_assets_swap/utils"
	"encoding/json"
	"fmt"
	"reflect"
	"testing"

	fabConfig "atomic_assets_swap/config/fabric"

	"github.com/hyperledger/fabric-protos-go/common"
	"github.com/hyperledger/fabric-sdk-go/pkg/fab/ccpackager/gopackager"
	"github.com/hyperledger/fabric-sdk-go/pkg/fab/resource"
	"github.com/hyperledger/fabric-sdk-go/third_party/github.com/hyperledger/fabric/common/policydsl"

	"bou.ke/monkey"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/event"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/ledger"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/msp"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/resmgmt"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/context"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/core"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/fab"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
)

// fabric sdk
var client *ChainClient

func initFunc() {
	// fabsdk.New
	monkey.Patch(fabsdk.New, func(core.ConfigProvider, ...fabsdk.Option) (*fabsdk.FabricSDK, error) {
		result := fabsdk.FabricSDK{}
		return &result, nil
	})
	// msp.New
	monkey.Patch(msp.New, func(context.ClientProvider, ...msp.ClientOption) (*msp.Client, error) {
		result := msp.Client{}
		return &result, nil
	})
	// resmgmt.New
	monkey.Patch(resmgmt.New, func(context.ClientProvider, ...resmgmt.ClientOption) (*resmgmt.Client, error) {
		result := resmgmt.Client{}
		return &result, nil
	})
	// channel.New
	monkey.Patch(channel.New, func(context.ChannelProvider, ...channel.ClientOption) (*channel.Client, error) {
		result := channel.Client{}
		return &result, nil
	})
	// ledger.New
	monkey.Patch(ledger.New, func(context.ChannelProvider, ...ledger.ClientOption) (*ledger.Client, error) {
		result := ledger.Client{}
		return &result, nil
	})
	// event.New
	monkey.Patch(event.New, func(context.ChannelProvider, ...event.ClientOption) (*event.Client, error) {
		result := event.Client{}
		return &result, nil
	})
	// gopackager.NewCCPackage
	monkey.Patch(gopackager.NewCCPackage, func(string, string) (*resource.CCPackage, error) {
		result := resource.CCPackage{}
		return &result, nil
	})
	// policydsl.FromString
	monkey.Patch(policydsl.FromString, func(string) (*common.SignaturePolicyEnvelope, error) {
		result := common.SignaturePolicyEnvelope{}
		return &result, nil
	})

	// contractinvoke.FabricClient.QueryChannel
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&contractinvoke.FabricClient{}),
		"QueryChannel",
		func(*contractinvoke.FabricClient,
		) ([]string, error) {
			result := []string{"channel1", "channel2"}
			return result, nil
		})
	// contractinvoke.FabricClient
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&contractinvoke.FabricClient{}),
		"CreateAndJoinChannel",
		func(*contractinvoke.FabricClient, *fabConfig.ChannelInfo,
		) error {
			return nil
		})
	// contractinvoke.FabricClient
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&contractinvoke.FabricClient{}),
		"QueryChaincode",
		func(*contractinvoke.FabricClient) ([]string, error) {
			return []string{}, nil
		})
	// contractinvoke.FabricClient
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&contractinvoke.FabricClient{}),
		"DeployChaincodeWithArgs",
		func(*contractinvoke.FabricClient, *fabConfig.ChainCodeInfo, [][]byte) error {
			return nil
		})
	// contractinvoke.FabricClient
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&contractinvoke.FabricClient{}),
		"DeployChaincode",
		func(*contractinvoke.FabricClient, *fabConfig.ChainCodeInfo) error {
			return nil
		})
	// contractinvoke.FabricClient
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&contractinvoke.FabricClient{}),
		"InvokeFunction",
		func(*contractinvoke.FabricClient, string, string, []string, ...string,
		) (channel.Response, error) {
			result := channel.Response{}
			p := proposalstruct.Proposal{}
			pByte, err := json.Marshal(p)
			if err != nil {
				return result, fmt.Errorf("monkey invoke function error: %s", err)
			}
			result.Payload = pByte
			return result, nil
		})
	// contractinvoke.FabricClient
	monkey.PatchInstanceMethod(
		reflect.TypeOf(&contractinvoke.FabricClient{}),
		"Close",
		func(*contractinvoke.FabricClient) {
		})
}

// new client
func TestNewClient(t *testing.T) {
	var err error
	client, err = NewClient([]string{"config.yml", "orgname", "username"})
	if err != nil {
		panic(err)
	}
}

// get proposal
func TestGetProposal(t *testing.T) {
	// 获取htlc proposal
	if _, err := client.GetProposal("htlc", "proposalid"); err != nil {
		panic(err)
	}

	// 获取lilac proposal
	if _, err := client.GetProposal("lilac", "proposalid"); err != nil {
		panic(err)
	}

	// 类型错误
	if _, err := client.GetProposal("errorType", "proposalid"); err == nil {
		panic("get proposal error: errorType illegal")
	}
}

// new proposal
func TestNewProposal(t *testing.T) {
	// 时间锁错误
	if _, err := client.NewProposal("htlc", "toaddr", "1", "hashlock", "timelock"); err == nil {
		panic("timelock error: err==nil error")
	}

	// 类型错误
	if _, err := client.NewProposal("wrongtype", "toaddr", "1", "hashlock", "600"); err == nil {
		panic("proposal type error: err == nil error")
	}

	// htlc amount错误
	if _, err := client.NewProposal("htlc", "toaddr", "amount", "hash", "600"); err == nil {
		panic("amount error: err == nil error")
	}

	// htlc hashlock 错误
	if _, err := client.NewProposal("htlc", "toaddr", "1", 1, "600"); err == nil {
		panic("hash lock error: err == nil error")
	}

	// 正常proposal
	if _, err := client.NewProposal("htlc", "toaddr", "1", "hashlock", "600"); err != nil {
		panic(err)
	}

	// lilac amount错误
	if _, err := client.NewProposal("lilac", "toaddr", "amount", "hashlock", "600"); err == nil {
		panic("amount error: err == nil error")
	}

	// lilac hashlock 错误
	if _, err := client.NewProposal("lilac", "toaddr", "1", 2, "600"); err == nil {
		panic("hashlock error: err == nil error")
	}

	// 正常proposal
	lilacHashlock := utils.StringArray2Any([]string{"hash1", "hash2"})
	if _, err := client.NewProposal("lilac", "toaddr", "1", lilacHashlock, "600"); err != nil {
		panic(err)
	}
}

// 事件解析测试
func TestEventParse(t *testing.T) {
	// 事件类型错误
	if _, err := client.ParseEventToProposal("eventinfo"); err == nil {
		panic("event type error: err == nil error")
	}

	eventInfo := &fab.CCEvent{}
	eventInfo.EventName = "wrongName"

	// 事件名错误
	if _, err := client.ParseEventToProposal(eventInfo); err == nil {
		panic("event name error: err == nil error")
	}

	// newProposal
	eventInfo.EventName = "newProposal"

	// 空payload错误
	if _, err := client.ParseEventToProposal(eventInfo); err == nil {
		panic("event payload error: err == nil error")
	}

	// payload解析错误
	eventInfo.Payload = []byte("error payload")
	if _, err := client.ParseEventToProposal(eventInfo); err == nil {
		panic("event payload unmarshal error: err == nil error")
	}

	// payload数量错误
	var payload []byte
	var err error
	payload, err = json.Marshal([]string{"11"})
	if err != nil {
		panic("json.marshal error: " + err.Error())
	}
	eventInfo.Payload = payload
	if _, err = client.ParseEventToProposal(eventInfo); err == nil {
		panic("length of event payload data error: err == nil error")
	}

	// proposal json 错误
	payload, err = json.Marshal([]string{"proposalid", "proposaljson"})
	if err != nil {
		panic("json.marshal error: " + err.Error())
	}
	eventInfo.Payload = payload
	if _, err = client.ParseEventToProposal(eventInfo); err == nil {
		panic("event payload of proposaljson error: err == nil error")
	}

	// 正常解析
	proposalJson, err := json.Marshal(proposalstruct.Proposal{})
	if err != nil {
		panic("json.marshal proposal error: " + err.Error())
	}
	payload, err = json.Marshal([]string{"proposalid", string(proposalJson)})
	if err != nil {
		panic("json.marshal error: " + err.Error())
	}
	eventInfo.Payload = payload
	if _, err := client.ParseEventToProposal(eventInfo); err != nil {
		panic(err)
	}

	// withdraw
	eventInfo.EventName = "withdraw"
	if _, err := client.ParseEventToProposal(eventInfo); err != nil {
		panic(err)
	}
}

// 测试refund
func TestRefund(t *testing.T) {
	// 类型错误
	if err := client.Refund("wrongtype", "proposalid"); err == nil {
		panic("refund wrong proposal type error: err == nil error")
	}

	// htlc退款
	if err := client.Refund("htlc", "proposalid"); err != nil {
		panic(err)
	}

	// lilac退款
	if err := client.Refund("lilac", "proposalid"); err != nil {
		panic(err)
	}
}

// 测试取款
func TestWithdraw(t *testing.T) {
	// 类型错误
	if err := client.Withdraw("wrong type", "proposalid", "preimage"); err == nil {
		panic("withdraw wrong type error: err == nil error")
	}

	// htlc 原像类型错误
	if err := client.Withdraw("htlc", "proposalid", 122); err == nil {
		panic("withdraw preimage type error: err == nil error")
	}

	// htlc正常withdraw
	if err := client.Withdraw("htlc", "proposalid", "preimage"); err != nil {
		panic(err)
	}

	// lilac 原像类型错误
	if err := client.Withdraw("lilac", "proposalid", 122); err == nil {
		panic("withdraw preimage type error: err == nil error")
	}

	// lilac正常withdraw
	preimage := utils.StringArray2Any([]string{"preimage1", "preimage2"})
	if err := client.Withdraw("lilac", "proposalid", preimage); err != nil {
		panic(err)
	}
}

// 测试其他功能
func TestOtherFunc(t *testing.T) {
	if _, err := client.GetAddress(); err != nil {
		panic(err)
	}

	if _, err := client.GetBalance("address"); err != nil {
		panic(err)
	}

	if _, err := client.GetChainID(); err != nil {
		panic(err)
	}

	// 重置余额
	if err := client.ResetBalance(); err != nil {
		panic(err)
	}

	client.Close()
}

func TestMain(m *testing.M) {
	utils.InfoTips("开始测试fabric sdk")
	initFunc()
	m.Run()
	utils.InfoTips("fabric sdk测试完成")
}
