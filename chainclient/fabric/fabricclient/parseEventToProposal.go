/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 解析事件为指定的proposal结构, 包括:
1. 解析lilac的newProposal事件为proposal结构
2. 解析lilac的withdraw事件为proposal结构
3. 解析htlc的newProposal事件为proposal结构
4. 解析htlc的withdraw事件为proposal结构
*/
package fabricclient

import (
	"atomic_assets_swap/chainclient/proposalstruct"
	"atomic_assets_swap/config"
	"encoding/json"
	"fmt"

	"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/fab"
)

// 解析事件信息为一个proposal
// @param eventInfo 对应链的事件信息
// @return proposalStruct 解析的proposal
// @return error 错误
func (c *ChainClient) ParseEventToProposal(eventInfo interface{}) (proposalstruct.Proposal, error) {
	event, ok := eventInfo.(*fab.CCEvent)
	if !ok {
		return proposalstruct.Proposal{}, fmt.Errorf("invalid type of event")
	}
	switch event.EventName {
	case config.TOPIC_NEW_PROPOSAL:
		return c.parseNewProposalEvent(event)
	case config.TOPIC_WITHDRAW:
		return c.parseWithdrawEvent(event)
	}
	return proposalstruct.Proposal{}, fmt.Errorf("invalid event name:" + event.EventName)
}

// 解析withdraw事件
// @param event 合约事件
// @return proposalStruct 解析的合约事件
// @return error 错误
func (c *ChainClient) parseWithdrawEvent(event *fab.CCEvent) (proposalstruct.Proposal, error) {
	// htlc和lilac的withdraw事件的处理也和newProposal相同
	return c.parseNewProposalEvent(event)
}

// 解析newProposal事件
// @param event 合约事件
// @return proposalStruct 解析的合约事件
// @return error 错误
func (c *ChainClient) parseNewProposalEvent(event *fab.CCEvent) (proposalstruct.Proposal, error) {
	// htlc和lilac的newProposal事件的返回处理都是这样
	var proposal proposalstruct.Proposal
	if len(event.Payload) < 1 {
		return proposal, fmt.Errorf("fabric event payload size illegal. get %d", len(event.Payload))
	}
	// decode proposal
	eventData := []string{}
	if err := json.Unmarshal(event.Payload, &eventData); err != nil {
		return proposal, fmt.Errorf("unmarshal eventData error: " + err.Error())
	}
	if len(eventData) != 2 {
		return proposal, fmt.Errorf("length of event data must be equal to 2")
	}
	proposalID := eventData[0]
	proposalJson := eventData[1]
	err := json.Unmarshal([]byte(proposalJson), &proposal)
	if err != nil {
		return proposal, fmt.Errorf("fabric parse event error: %s", err)
	}

	proposal.ProposalID = proposalID
	proposal.ContractName = event.ChaincodeID
	proposal.ChainName, err = c.GetChainID()
	if err != nil {
		return proposal, fmt.Errorf("fabric parse event getChainID error: %s", err)
	}
	return proposal, nil
}
