/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 关闭链客户端
*/
package fabricclient

// 关闭链客户端
// defer的时候调用
func (c *ChainClient) Close() {
	c.mptFabricClient.Close()
}
