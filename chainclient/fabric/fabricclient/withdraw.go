/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 给定proposalID、proposal类型以及proposal原像，对其进行取款操作, 包括:
	1. 取款htlc proposal
	2. 取款lilac proposal
*/
// coding:utf-8
// 取钱
package fabricclient

import (
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"encoding/json"
	"fmt"
)

// 取钱
// @param proposalType proposal类型"htlc","lilac"
// @param proposalID 取这个proposal的钱
// @param preimage 原像
func (c *ChainClient) Withdraw(proposalType, proposlID string, preimage interface{}) error {
	switch proposalType {
	case "htlc":
		return c.withdrawHtlc(proposlID, preimage)
	case "lilac":
		return c.withdrawLilac(proposlID, preimage)
	default:
		return fmt.Errorf("invalid proposalType in chainClient.Withdraw: %s", proposalType)
	}
}

// 取htlc的钱
// @param proposalID 取这个proposal的钱
// @param preimage 原像
func (c *ChainClient) withdrawHtlc(proposlID string, preimage interface{}) error {
	preimageStr, ok := preimage.(string)
	if !ok {
		return fmt.Errorf("preimage is not string error")
	}
	args := []string{
		proposlID,
		preimageStr,
	}
	resp, err := c.mptFabricClient.InvokeFunction(config.HTLC_CONTRACT_NAME, "withdraw", args, c.masEndorserPeer...)
	if err != nil {
		return fmt.Errorf("withdrawHtlc invoke fabric function error: %s", err)
	}
	utils.Info("withdraw htlc", string(resp.Payload))
	return nil
}

// 取lilac的钱
// @param proposalID 取这个proposal的钱
// @param preimage 原像
func (c *ChainClient) withdrawLilac(proposlID string, preimage interface{}) error {
	preimageStrList, err := utils.Any2StringArray(preimage)
	if err != nil {
		return fmt.Errorf("fabric withdrawLilac preimage error: %s", err)
	}
	preimageJson, err := json.Marshal(preimageStrList)
	if err != nil {
		return fmt.Errorf("fabric withdrawLilac json.marshal error: %s", err)
	}
	args := []string{
		proposlID,
		string(preimageJson),
	}
	resp, err := c.mptFabricClient.InvokeFunction(config.LILAC_CONTRACT_NAME, "withdraw", args, c.masEndorserPeer...)
	if err != nil {
		info := utils.InfoFabricChannelRespError("fabric withdrawLilac InvokeFunction", &resp, err)
		return fmt.Errorf(info)
	}
	return nil
}
