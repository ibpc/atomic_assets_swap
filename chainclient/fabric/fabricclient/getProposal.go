/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 获取proposal, 包括：
1. 获取Lilac proposal信息
2. 获取htlc proposal信息
*/
package fabricclient

import (
	"atomic_assets_swap/chainclient/proposalstruct"
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"encoding/json"
	"fmt"
)

// 获取HTLC proposal
// @param proposalID id
// @return string proposalJson
// @return error err
func (c *ChainClient) getHTLCProposal(proposalID string) (string, error) {
	args := []string{proposalID}
	resp, err := c.mptFabricClient.InvokeFunction(config.HTLC_CONTRACT_NAME, "getProposal", args, c.masEndorserPeer...)
	if err != nil {
		info := utils.InfoFabricChannelRespError("fabric getHtlcProposal InvokeFuncton", &resp, err)
		return "", fmt.Errorf(info)
	}
	return string(resp.Payload), nil
}

// 获取LILAC proposal
// @param proposalID id
// @return string proposalJson
// @return error err
func (c *ChainClient) getLILACProposal(proposalID string) (string, error) {
	args := []string{proposalID}
	resp, err := c.mptFabricClient.InvokeFunction(config.LILAC_CONTRACT_NAME, "getProposal", args, c.masEndorserPeer...)
	if err != nil {
		info := utils.InfoFabricChannelRespError("fabric getLilacProposal InvokeFuncton", &resp, err)
		return "", fmt.Errorf(info)
	}
	return string(resp.Payload), nil
}

// 获取proposal
// @param proposalType proposal类型
// @param proposalID proposalID
// @return proposal 这个proposalID对应的proposal
// @return error err
func (c *ChainClient) GetProposal(proposalType, proposalID string) (proposalstruct.Proposal, error) {
	var proposalJson string
	var err error
	var contractName string
	switch proposalType {
	case config.HTLC_CONTRACT_NAME:
		// htlc
		proposalJson, err = c.getHTLCProposal(proposalID)
		contractName = config.HTLC_CONTRACT_NAME
	case config.LILAC_CONTRACT_NAME:
		// lilac
		proposalJson, err = c.getLILACProposal(proposalID)
		contractName = config.LILAC_CONTRACT_NAME
	default:
		// 暂时不支持其他合约名
		proposalJson = ""
		err = fmt.Errorf("fabric GetProposal invalid proposalType: %s", proposalType)
	}
	if err != nil {
		return proposalstruct.Proposal{}, fmt.Errorf("fabric GetProposal error: %s", err)
	}

	// 解析proposalJson
	proposal := proposalstruct.Proposal{}
	err = json.Unmarshal([]byte(proposalJson), &proposal)
	if err != nil {
		info := utils.InfoError("GetProposal unmarshal", err)
		return proposal, fmt.Errorf(info)
	}
	proposal.ProposalID = proposalID
	proposal.ContractName = contractName
	proposal.ChainName, err = c.GetChainID()
	if err != nil {
		info := utils.InfoError("fabricClient.GetChainID", err)
		return proposal, fmt.Errorf(info)
	}
	return proposal, nil
}
