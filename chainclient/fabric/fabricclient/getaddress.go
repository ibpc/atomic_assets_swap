/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 获取账户地址
*/
package fabricclient

import (
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"fmt"
)

// 获取地址
// @return string 地址
// @return error err
func (c *ChainClient) GetAddress() (string, error) {
	if c.msAddress == "" {
		// 获取地址
		resp, err := c.mptFabricClient.InvokeFunction(
			config.ASSETS_CONTRACT_NAME,
			"getAddress",
			[]string{},
			c.masEndorserPeer...)
		if err != nil {
			info := utils.InfoError("InvokeFunction GetAddress", err)
			return "", fmt.Errorf(info)
		}
		c.msAddress = string(resp.Payload)
	}
	return c.msAddress, nil
}
