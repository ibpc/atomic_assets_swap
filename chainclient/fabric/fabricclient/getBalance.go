/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 获取当前用户余额
*/
package fabricclient

import (
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"fmt"
)

// 获取余额
// @param address 地址,如果为""则返回自己的余额
// @return string 余额
// @return error err
func (c *ChainClient) GetBalance(address string) (string, error) {
	// 如果为空则返回自己的余额
	if address == "" {
		address = c.msAddress
	}
	args := []string{
		address,
	}
	resp, err := c.mptFabricClient.InvokeFunction(config.ASSETS_CONTRACT_NAME, "balanceOf", args, c.masEndorserPeer...)
	if err != nil {
		info := utils.InfoFabricChannelRespError("GetBalance", &resp, err)
		return "", fmt.Errorf(info)
	}
	return string(resp.Payload), nil
}
