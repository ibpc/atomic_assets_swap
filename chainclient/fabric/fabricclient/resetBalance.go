/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 重置余额
*/
package fabricclient

import (
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"fmt"
)

// 重置自己的余额
// ! 后续考虑删除
func (c *ChainClient) ResetBalance() error {
	args := []string{}
	resp, err := c.mptFabricClient.InvokeFunction(config.ASSETS_CONTRACT_NAME, "reset", args, c.masEndorserPeer...)
	if err != nil {
		info := utils.InfoFabricChannelRespError("fabric resetBalance", &resp, err)
		return fmt.Errorf(info)
	}
	return nil
}
