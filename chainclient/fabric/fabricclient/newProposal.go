/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 发起一个新的proposal, 包括:
1. 发起一个新的htlc proposal
2. 发起一个新的lilac proposal
*/
package fabricclient

import (
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"encoding/json"
	"fmt"
)

// 发起一个新的htlc proposal
// @param toAddr 接收者地址
// @param amount 发送的数量
// @param hashlock 哈希锁
// @param timelock 时间锁，单位秒
// @return string propsal id
// @return error err
func (c *ChainClient) newLilacProposal(
	toAddr,
	amount string,
	hashlock []string,
	timelock string,
) (string, error) {
	// 必须是整数
	if !utils.IsNumber(amount) {
		info := utils.Info("fabric newLilacProposal params \"amount\" is not a number: " + amount)
		return "", fmt.Errorf(info)
	}
	if !utils.IsNumber(timelock) {
		info := utils.Info("fabric newLilacProposal params \"timelock\" is not a number: " + timelock)
		return "", fmt.Errorf(info)
	}

	// 哈希锁序列化
	hashJson, err := json.Marshal(hashlock)
	if err != nil {
		info := utils.InfoError("fabric newLilacProposal json marshal", err)
		return "", fmt.Errorf(info)
	}
	args := []string{
		toAddr,
		amount,
		string(hashJson),
		timelock,
	}
	resp, err := c.mptFabricClient.InvokeFunction(config.LILAC_CONTRACT_NAME, "newProposal", args, c.masEndorserPeer...)
	if err != nil {
		info := utils.InfoFabricChannelRespError("fabric newLilacProposal InvokeFuncton", &resp, err)
		return "", fmt.Errorf(info)
	}
	return string(resp.Payload), nil
}

// 发起一个新的htlc proposal
// @param toAddr 接收者地址
// @param amount 发送的数量
// @param hashlock 哈希锁
// @param timelock 时间锁，单位秒
// @return string propsal id
// @return error err
func (c *ChainClient) newHtlcProposal(toAddr, amount, hashlock, timelock string) (string, error) {
	// 必须是整数
	if !utils.IsNumber(amount) {
		info := utils.Info("fabric newHtlcProposal params \"amount\" is not a number: " + amount)
		return "", fmt.Errorf(info)
	}
	if !utils.IsNumber(timelock) {
		info := utils.Info("fabric newHTLCProposal params \"timelock\" is not a number: " + timelock)
		return "", fmt.Errorf(info)
	}
	args := []string{
		toAddr,
		amount,
		hashlock,
		timelock,
	}
	resp, err := c.mptFabricClient.InvokeFunction(config.HTLC_CONTRACT_NAME, "newProposal", args, c.masEndorserPeer...)
	if err != nil {
		info := utils.InfoFabricChannelRespError("fabric newHtlcProposal InvokeFuncton", &resp, err)
		return "", fmt.Errorf(info)
	}
	return string(resp.Payload), nil
}

// 在链chainName发起一个proposalType的proposal, 参数为params
// @param proposalType 发起的proposal类型
// @param fromAddr 发起者地址
// @param toAddr 接收者地址
// @param amount 发送的数量
// @param hashlock 哈希锁
// @param timelock 时间锁, 单位秒
// @return string proposalID
// @return error err
func (c *ChainClient) NewProposal(proposalType, toAddr, amount string, hashlock, timelock interface{}) (string, error) {
	// 时间锁判定
	if _, ok := timelock.(string); !ok {
		return "", fmt.Errorf("fabric NewProposal error: timelock is not string")
	}

	switch proposalType {
	case config.PROPOSAL_TYPE_HTLC:
		// htlc
		if _, ok := hashlock.(string); !ok {
			return "", fmt.Errorf("fabric NewProposal error: hashlock is not string")
		}
		proposalID, err := c.newHtlcProposal(toAddr, amount, hashlock.(string), timelock.(string))
		if err != nil {
			return "", fmt.Errorf("fabric ChainClient NewProposla htlc error: %s", err)
		}
		return proposalID, nil
	case config.PROPOSAL_TYPE_LILAC:
		// lilac
		currHashLock, err := utils.Any2StringArray(hashlock)
		if err != nil {
			return "", fmt.Errorf("fabric ChainClient NewLilacProposal error: %s", err)
		}
		proposalID, err := c.newLilacProposal(toAddr, amount, currHashLock, timelock.(string))
		if err != nil {
			return "", fmt.Errorf("fabric ChainClient NewProposal lilac error: %s", err)
		}
		return proposalID, nil
	default:
		return "", fmt.Errorf("invalid case \"%s\"", proposalType)
	}
}
