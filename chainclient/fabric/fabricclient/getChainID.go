/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 获取链ID
*/
package fabricclient

import "atomic_assets_swap/config"

// 获取chainID
// @return string id
// @return error err
func (c *ChainClient) GetChainID() (string, error) {
	return config.FABRIC, nil
}
