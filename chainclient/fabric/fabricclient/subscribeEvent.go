/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 订阅合约的某个事件并以通道的形式返回
*/
package fabricclient

import (
	"atomic_assets_swap/utils"
	"context"
)

// 监听一个链的事件
// @param contractName 被监听的合约名
// @param eventName 被监听的事件名称
// @param resultChan 返回监听的结果
func (c *ChainClient) SubscribeEvent(contractName, eventName string, resultChan chan<- interface{}) {
	utils.Info("订阅" + contractName + "的" + eventName)
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	eventClient := c.mptFabricClient.EventClient
	// 订阅事件
	reg, eventCh, err := eventClient.RegisterChaincodeEvent(contractName, eventName)
	if err != nil {
		utils.InfoError("fabric subscribeEvent "+contractName+" "+eventName, err)
		return
	}
	defer eventClient.Unregister(reg)

	for {
		select {
		case eventData := <-eventCh:
			resultChan <- eventData
		case <-ctx.Done():
			close(resultChan)
			return
		}
	}

}
