/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 给定proposalID以及类型，对其进行退款操作, 包括:
1. 退款htlc proposal
2. 退款lilac proposal
*/
package fabricclient

import (
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"fmt"
)

// 撤销一个proposal
// @param proposalType proposal类型"htlc","lilac"
// @param proposalID 被撤销的id
func (c *ChainClient) Refund(proposalType, proposalID string) error {
	// 指定proposal类型，如果类型和id不匹配也会执行失败
	switch proposalType {
	case config.PROPOSAL_TYPE_HTLC:
		return c.refundHtlc(proposalID)
	case config.PROPOSAL_TYPE_LILAC:
		return c.refundLilac(proposalID)
	default:
		return fmt.Errorf("invalid proposalType: %s", proposalType)
	}
}

// 撤销一个htlc proposal
// @param proposalID 被撤销的id
func (c *ChainClient) refundHtlc(proposalID string) error {
	args := []string{proposalID}
	resp, err := c.mptFabricClient.InvokeFunction(config.HTLC_CONTRACT_NAME, "refund", args, c.masEndorserPeer...)
	if err != nil {
		info := utils.InfoFabricChannelRespError("fabric refundHtlc", &resp, err)
		return fmt.Errorf(info)
	}
	return nil
}

// 撤销一个htlc proposal
// @param proposalID 被撤销的id
func (c *ChainClient) refundLilac(proposalID string) error {
	args := []string{proposalID}
	resp, err := c.mptFabricClient.InvokeFunction(config.LILAC_CONTRACT_NAME, "refund", args, c.masEndorserPeer...)
	if err != nil {
		info := utils.InfoFabricChannelRespError("fabric refundLilac", &resp, err)
		return fmt.Errorf(info)
	}
	return nil
}
