# bin/bash
#! 首先初始化余额
echo "初始化用户资产"
for chainid in 1 2;
do
    for index in {1..4}
    # for index in {1..2}
    do
        echo "初始化 链${chainid} 用户${index} 链上资产"
        ./bin/cmc client contract user invoke --contract-name=assets --method=reset --sdk-conf-path=./config_files/sdkconfigs/chain${chainid}_sdkconfig${index}.yml --params="{}" --sync-result=true --result-to-string=true
    done
done

echo "开始测试合约功能..."

# 测试合约功能
echo "计算config的测试覆盖率"
go test -v -coverprofile=config.out ./config >>config.log
go tool cover -html=config.out
tail  config.log | grep -E "coverage|===|---"

echo "计算contractinvoke测试覆盖率"
go test -v -coverprofile=contractinvoke.out ./http_server_process/contractinvoke/ >>contractinvoke.log
go tool cover -html=contractinvoke.out
tail  contractinvoke.log | grep -E "coverage|===|---"

echo "计算notice测试覆盖率"
go test -v -coverprofile=notice.out ./http_server_process/notice/ >>notice.log
go tool cover -html=notice.out
tail  notice.log | grep -E "coverage|===|---"

echo "计算http_server测试覆盖率"
go test -coverprofile=server.out -v ./http_server_process/http_server/ >>server.log
go tool cover -html=server.out
tail  server.log | grep -E "coverage|===|---"

# 注释覆盖率
echo "注释覆盖率"
gocloc --include-lang=Go --output-type=json --not-match=_test.go --not-match-d=example . | jq '(.total.comment-.total.files*6)/(.total.code+.total.comment)*100'

