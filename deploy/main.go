// coding:utf-8
// 部署合约
package main

import (
	"atomic_assets_swap/chainclient/chainmaker/contractinvoke"
	fabContractInvoke "atomic_assets_swap/chainclient/fabric/contractinvoke"
	"atomic_assets_swap/config"
	fabConfig "atomic_assets_swap/config/fabric"
	"atomic_assets_swap/utils"
	"fmt"
	"os"
	"sync"
	"time"

	sdk "chainmaker.org/chainmaker/sdk-go/v2"
)

// ################## 部署fabric合约 ##################

// 部署Lilac链码
func deployLilacChaincode(client *fabContractInvoke.FabricClient, ls_installedChaincode []string) error {
	// 判断是否要部署lilac
	flag := false
	for _, name := range ls_installedChaincode {
		if name == config.LILAC_CONTRACT_NAME {
			flag = true
			utils.Info("chaincode lilac already exist.")
			break
		}
	}
	if flag {
		return nil
	}

	chaincodeInfo := fabConfig.NewChaincodeInfo(config.LILAC_CONTRACT_NAME, "lilac/")
	// 部署
	if err := client.DeployChaincode(&chaincodeInfo); err != nil {
		info := utils.InfoError("deploy lilac chaincode", err)
		return fmt.Errorf(info)
	}
	return nil
}

// 部署htlc链码
func deployHtlcChaincode(client *fabContractInvoke.FabricClient, ls_installedChaincode []string) error {
	flag := false
	for _, name := range ls_installedChaincode {
		if name == config.HTLC_CONTRACT_NAME {
			flag = true
			utils.Info("chaincode htlc already exist.")
			break
		}
	}
	if flag {
		return nil
	}
	chaincodeInfo := fabConfig.NewChaincodeInfo(config.HTLC_CONTRACT_NAME, "htlc/")
	// 部署
	if err := client.DeployChaincode(&chaincodeInfo); err != nil {
		info := utils.InfoError("deploy htlc chaincode", err)
		return fmt.Errorf(info)
	}
	return nil
}

// 部署assets合约
func deployAssetsChaincode(client *fabContractInvoke.FabricClient, ls_installedChaincode []string) error {
	flag := false
	for _, name := range ls_installedChaincode {
		if name == config.ASSETS_CONTRACT_NAME {
			flag = true
			utils.Info("chaincode assets already exist.")
			break
		}
	}
	if flag {
		return nil
	}
	chaincodeInfo := fabConfig.NewChaincodeInfo(config.ASSETS_CONTRACT_NAME, "assets")
	args := [][]byte{
		[]byte("100000000"),
		[]byte("ict_coin"),
		[]byte("0.0.1"),
		[]byte("a,b"),
	}
	// 部署
	if err := client.DeployChaincodeWithArgs(&chaincodeInfo, args); err != nil {
		info := utils.InfoError("deploy assests chaincode", err)
		return fmt.Errorf(info)
	}
	return nil
}

// 部署必要的链码
func deployNecessaryChaincode(client *fabContractInvoke.FabricClient) error {
	// 查询链码
	ls_chaincode, err := client.QueryChaincode()
	if err != nil {
		info := utils.InfoError("init fabric querychaincode", err)
		return fmt.Errorf(info)
	}

	// 部署合约
	var wg sync.WaitGroup
	wg.Add(3)
	go func() {
		// 部署assets合约
		if err := deployAssetsChaincode(client, ls_chaincode); err != nil {
			utils.InfoError("deploy assets chaincode", err)
			return
		}
		wg.Done()
	}()

	go func() {
		// 部署htlc链码
		if err := deployHtlcChaincode(client, ls_chaincode); err != nil {
			utils.InfoError("deploy htlc chaincode", err)
			return
		}
		wg.Done()
	}()

	go func() {
		// 部署lilac链码
		if err := deployLilacChaincode(client, ls_chaincode); err != nil {
			utils.InfoError("deploy lilac chaincode", err)
			return
		}
		wg.Done()
	}()
	wg.Wait()

	return nil
}

// 确保通道存在
func testChannel(client *fabContractInvoke.FabricClient) error {
	// 查询通道，确定有指定的通道
	flag := false
	ls_channel, err := client.QueryChannel()
	if err != nil {
		return fmt.Errorf("query channel error: " + err.Error())
	}
	for _, id := range ls_channel {
		if id == fabConfig.GetChannelInfo().ChannelID {
			flag = true
			break
		}
	}

	// 如果通道不存在就创建并加入通道
	if !flag {
		utils.InfoTips("通道不存在, 创建并加入通道")
		// 创建并加入通道
		if err := client.CreateAndJoinChannel(fabConfig.GetChannelInfo()); err != nil {
			return fmt.Errorf("create channel error:" + err.Error())
		}
	}

	return nil
}

func deployFabricContract() {
	configPath := "./config_files/fabric/user.yaml"
	orgName := "Org1"
	userName := "user1"
	// 建立与链交互的客户端
	fabricChainClient, err := fabContractInvoke.NewClient(configPath, orgName, userName)
	if err != nil {
		utils.InfoError("fabric NewClient", err)
		os.Exit(1)
	}

	// 查询通道
	if err := testChannel(fabricChainClient); err != nil {
		utils.InfoError("fabric channel", err)
		os.Exit(1)
	}

	// 部署合约
	if err := deployNecessaryChaincode(fabricChainClient); err != nil {
		utils.InfoError("deploy fabric contract", err)
		os.Exit(1)
	}

	// 等待两秒
	time.Sleep(2 * time.Second)
}

// ############### 部署chainmaker合约 ###############

// assets链码存在
// @param c chainClient
func testAssetsContract(c *sdk.ChainClient) error {
	contractInfo, err := c.GetContractInfo(config.ASSETS_CONTRACT_NAME)
	if err == nil {
		utils.InfoTips("assets合约存在:")
		utils.Info(contractInfo)
		return nil
	}
	// 有错表示合约不存在就部署
	utils.InfoTips("assets合约不存在,开始部署")
	if err := contractinvoke.DeployAssetsContract(c); err != nil {
		info := utils.InfoError("test assets contract", err)
		return fmt.Errorf(info)
	}
	utils.InfoTips("assets合约部署成功")
	return nil
}

// htlc链码存在
// @param c chainClient
func testHtlcContract(c *sdk.ChainClient) error {
	contractInfo, err := c.GetContractInfo(config.HTLC_CONTRACT_NAME)
	if err == nil {
		utils.InfoTips("htlc合约存在:")
		utils.Info(contractInfo)
		return nil
	}
	// 有错表示合约不存在就部署
	utils.InfoTips("htlc合约不存在,开始部署")
	if err := contractinvoke.DeployHTLCContract(c); err != nil {
		info := utils.InfoError("test htlc contract", err)
		return fmt.Errorf(info)
	}
	utils.InfoTips("htlc合约部署成功")
	return nil
}

// lilac链码存在
// @param c chainClient
func testLilacContract(c *sdk.ChainClient) error {
	contractInfo, err := c.GetContractInfo(config.LILAC_CONTRACT_NAME)
	if err == nil {
		utils.InfoTips("lilac合约存在:")
		utils.Info(contractInfo)
		return nil
	}
	// 有错表示合约不存在就部署
	utils.InfoTips("lilac合约不存在,开始部署")
	if err := contractinvoke.DeployLilacContract(c); err != nil {
		info := utils.InfoError("test lilac contract", err)
		return fmt.Errorf(info)
	}
	utils.InfoTips("lilac合约部署成功")
	return nil
}

// 测试合约
// @param c chainClient
func testContract(c *sdk.ChainClient) error {
	// 测试资产合约存在性
	if err := testAssetsContract(c); err != nil {
		return err
	}
	// 测试htlc合约存在性
	if err := testHtlcContract(c); err != nil {
		return err
	}
	// 测试lilac合约存在性
	return testLilacContract(c)
}

// 创建链客户端
// @param sdkConfPath:客户端sdk路径
func createChainmakerClient(sdkConfPath string) (*sdk.ChainClient, error) {
	cc, err := sdk.NewChainClient(
		sdk.WithConfPath(sdkConfPath),
		sdk.WithEnableTxResultDispatcher(true),
	)
	if err != nil {
		info := utils.InfoError("sdk.NewChainClient", err)
		return nil, fmt.Errorf(info)
	}
	if cc.GetAuthType() == sdk.PermissionedWithCert {
		if err := cc.EnableCertHash(); err != nil {
			info := utils.InfoError("cc.EnableCertHash", err)
			return nil, fmt.Errorf(info)
		}
	}
	return cc, nil
}

// 部署长安链合约
func deployChainmakerContract(configPath string) {
	utils.InfoTips("开始部署chainmaker合约")
	chainmaker_client, err := createChainmakerClient(configPath)
	if err != nil {
		utils.InfoError("deploy chainmaker client", err)
		os.Exit(1)
	}
	// 部署chainmaker合约
	if err := testContract(chainmaker_client); err != nil {
		utils.InfoError("test deploy contract", err)
		os.Exit(1)
	}
}

func main() {
	// 部署chainmaker合约
	chain1UserConfigPath := "./config_files/sdkconfigs/chain1_sdkconfig1.yml"
	deployChainmakerContract(chain1UserConfigPath)
	chain2UserConfigPath := "./config_files/sdkconfigs/chain2_sdkconfig1.yml"
	deployChainmakerContract(chain2UserConfigPath)
	// 部署fabric合约
	deployFabricContract()
}
