/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 订阅withdraw事件, 包括:
1. htlc withdraw事件
2. lilac withdraw事件
*/
package client

import (
	"atomic_assets_swap/chainclient/proposalstruct"
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"fmt"
	"strings"
)

// 订阅新的htlc 取钱事件
// @param chainName 链名
// @param userName 用户名
// @param contractName 合约名
// @param eventName 事件名
// @param resultChan 获取结果的channel
func (c *Client) subscribeWithdraw(chainName, userName, contractName string) {
	keyName, err := c.makeClientMapKey(chainName, userName)
	if err != nil {
		utils.Info("subscribeWithdraw chainName and userName error: " + err.Error())
		return
	}

	// 通过通道获取链客户端返回的消息
	resultChan := make(chan interface{}, 100)
	// 处理withdraw
	go c.dealWithdrawEvent(chainName, userName, resultChan)
	// 这是一个死循环,不会出来
	c.mmptClient[keyName].SubscribeEvent(contractName, "withdraw", resultChan)
}

// 处理chainmaker的withdraw事件
// @param chainName 链名
// @param userName 用户名
// @param resultChan 获取结果的channel
func (c *Client) dealWithdrawEvent(chainName, userName string, resultChan chan interface{}) {
	keyName, err := c.makeClientMapKey(chainName, userName)
	if err != nil {
		utils.Info("dealWithdrawEvent chainName and userName error: " + err.Error())
		return
	}
	currClient := c.mmptClient[keyName]
	if currClient == nil {
		utils.Info("dealWithdrawEvent get a nil client: invalid keyName")
		return
	}

	// 以自己的地址作为过滤标准
	selfAddress, err := currClient.GetAddress()
	if err != nil {
		utils.InfoError("c.dealWithdrawEvent", err)
		return
	}

	// 读取通道信息
	for {
		eventInfo := <-resultChan
		proposal, err := currClient.ParseEventToProposal(eventInfo)
		if err != nil {
			utils.InfoError("c.dealChainmakerWithdrawEvent", err)
			continue
		}

		// 发现一个withdraw的proposal的sender不是自己就跳过
		if proposal.Sender != selfAddress {
			continue
		}
		// 是自己就表示自己的proposal被取钱了

		// 确定是哪一种proposal
		if strings.Contains(proposal.ContractName, config.PROPOSAL_TYPE_HTLC) {
			proposal.ProposalType = config.PROPOSAL_TYPE_HTLC
		} else if strings.Contains(proposal.ContractName, config.PROPOSAL_TYPE_LILAC) {
			proposal.ProposalType = config.PROPOSAL_TYPE_LILAC
		} else {
			utils.Info("invalid contractName: " + proposal.ContractName)
			return
		}
		// 保存的proposal的具体信息
		proposal.ChainName = chainName
		proposal.UserName = userName

		// 输出
		utils.Info("======= " + chainName + " =======")
		utils.Info("自己的proposal被取钱了")
		utils.Info(proposal)

		// 从对端链取钱
		go c.withdrawPeerProposal(proposal, userName)
	}
}

// 检查自己被取款的proposal与发给自己哪一个proposal匹配
// @param proposal 接收到的proposal
// @return 在自己收到的proposal里面查找匹配的那个proposal, 用于后续取钱
func (c *Client) marchReceiveProposal(proposal proposalstruct.Proposal) *proposalstruct.Proposal {
	// 监听到一个自己的proposal被取钱了,就要在receive里面找符合条件的那个proposal并取钱
	for _, receiveProposal := range c.maReceiveProposal {
		// todo:要判断收到的这个proposal在自己的send里面吗,是为了确定chainName对得上
		// 合约名也要一样
		if proposal.ContractName != receiveProposal.ContractName {
			continue
		}
		// proposalType要一样
		if proposal.ProposalType != receiveProposal.ProposalType {
			continue
		}
		// lilac和htlc的hashlock格式不同
		if proposal.ProposalType == config.PROPOSAL_TYPE_HTLC {
			// 哈希锁要和我一开始设定的一样
			if proposal.HashLock != receiveProposal.HashLock {
				continue
			}
		} else if proposal.ProposalType == config.PROPOSAL_TYPE_LILAC {
			hash1, err := utils.Any2StringArray(proposal.HashLock)
			if err != nil {
				continue
			}
			hash2, err := utils.Any2StringArray(receiveProposal.HashLock)
			if err != nil {
				continue
			}
			// 哈希锁要和我一开始设定的一样
			if !utils.EqualStringArray(hash1, hash2) {
				continue
			}
		} else {
			panic("invalid proposal type")
		}
		// todo:判断金额
		// todo:时间锁判断???其他还需要吗

		return &receiveProposal
	}
	return nil
}

// 监听到自己的proposal被取钱了
// @param proposal 收到的proposal
// @param userName 当前用户名
func (c *Client) withdrawPeerProposal(proposal proposalstruct.Proposal, userName string) {
	receiveProposal := c.marchReceiveProposal(proposal)
	if receiveProposal == nil {
		utils.InfoWarning("监听到属于自己的Propsal, 但是没在本地找到匹配的proposal异常")
		return
	}

	utils.Info("withdraw切换到对端链取钱")
	currKeyName, err := c.makeClientMapKey(receiveProposal.ChainName, userName)
	if err != nil {
		utils.Info("makeClientMapKey error: " + err.Error())
		return
	}

	correspondingClient := c.mmptClient[currKeyName]
	if correspondingClient == nil {
		utils.Info("dealWithdrawEvent nil client error")
		return
	}

	if err := correspondingClient.Withdraw(
		proposal.ProposalType,
		receiveProposal.ProposalID,
		proposal.Preimage,
	); err != nil {
		utils.Info("取钱失败了 type: " + proposal.ProposalType)
		info := utils.InfoError("correspondingClient.WithDraw", err)
		fmt.Printf("监听到自己的proposal被取钱了:%+v", proposal)
		fmt.Printf("在本地找到与之匹配的对方发给我的proposal:%+v", receiveProposal)
		utils.InfoTips("查询是否重复")
		p, err := c.getProposal(receiveProposal.ChainName, userName, proposal.ProposalType, receiveProposal.ProposalID)
		if err != nil {
			info = utils.InfoError("withdraw失败, 且c.getProposal失败", err)
			panic(info)
		}
		if p.Unlocked {
			utils.InfoTips("proposal已经被取过, 跳过")
		} else {
			// 取失败
			panic(info)
		}
	}
	// 从本地proposal池删除已经处理过的proposal
	c.deleteNewReceiveProposal(*receiveProposal)
	c.deleteNewSendProposalByParams(proposal)
}
