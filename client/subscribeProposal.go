/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 订阅new proposal事件并处理, 包括:
1. htlc new proposal 事件
2. lilac new proposal 事件
*/
package client

import (
	"atomic_assets_swap/chainclient/proposalstruct"
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"fmt"
	"time"
)

// 订阅新的htlc proposal事件
// @param chainName 链名
// @param userName 用户名
// @param contractName 合约名
// @param eventName 事件名
// @param resultChan 获取结果的channel
func (c *Client) subscribeNewProposal(chainName, userName, contractName string) {
	// todo:判断keyName是否合法
	keyName, err := c.makeClientMapKey(chainName, userName)
	if err != nil {
		utils.Info("subscribeNewProposal chainName and userName error: " + err.Error())
		return
	}

	resultChan := make(chan interface{}, 100)
	// 处理发现一个新的proposal事件
	go c.dealNewProposalEvent(chainName, userName, contractName, resultChan)
	// 这是一个死循环,不会出来
	c.mmptClient[keyName].SubscribeEvent(contractName, "newProposal", resultChan)
}

// 处理监听到新proposal事件
// @param chainName 链名
// @param userName 用户名
// @param contractName 合约名
// @param resultChan 获取结果的通道
func (c *Client) dealNewProposalEvent(chainName, userName, contractName string, resultChan chan interface{}) {
	keyName, err := c.makeClientMapKey(chainName, userName)
	if err != nil {
		utils.Info("dealNewProposalEvent chainName and userName error: " + err.Error())
		return
	}
	currClient := c.mmptClient[keyName]
	if currClient == nil {
		utils.Info("dealNewProposalEvent get a nil client: invalid keyName")
		return
	}

	// 以当前用户地址为接收者地址作为过滤器
	receiverAddress, err := currClient.GetAddress()
	if err != nil {
		utils.InfoError("c.subscribeNewHtlcProposal", err)
		return
	}
	for {
		event := <-resultChan
		proposal, err := currClient.ParseEventToProposal(event)
		if err != nil {
			utils.InfoError("c.dealChainmakerNewHtlcProposalEvent", err)
			continue
		}
		var proposalType string
		switch contractName {
		case config.PROPOSAL_TYPE_HTLC:
			proposalType = config.PROPOSAL_TYPE_HTLC
		case config.PROPOSAL_TYPE_LILAC:
			proposalType = config.PROPOSAL_TYPE_LILAC
		default:
			utils.Info("invalid proposalType: " + proposalType)
			return
		}
		// 发现一个receiver是自己的就加入到receive里面
		if proposal.Receiver != receiverAddress {
			continue
		}
		// 加入收到的proposal的具体信息
		proposal.ChainName = chainName
		proposal.UserName = userName
		proposal.ContractName = contractName
		proposal.ProposalType = proposalType
		if err := c.addNewReceiveProposal(proposal); err != nil {
			utils.InfoError("c.dealChainmakerNewProposalEvent", err)
			continue
		}
		// 输出
		utils.Info(fmt.Sprintf("===== %s 监听到proposal ===== %s", chainName, proposal.ProposalID))
		utils.Info(fmt.Sprint(proposal))

		go c.dealReceiveProposal(proposal, chainName, userName)
	}
}

// 监听到发给自己的Proposal
// @param proposal 收到的Proposal
// @param chainName 收到这个proposal的chain name
// @param userName 收到这个proposal的user name
func (c *Client) dealReceiveProposal(proposal proposalstruct.Proposal, chainName, userName string) {
	// 休眠一秒，不然fabric可能会找不到proposal
	utils.InfoTips("等待1秒")
	time.Sleep(1 * time.Second)
	// 监听到一个发给自己的proposal, 就在自己发过的proposal里面找对应的proposal并取钱
	sendProposal := c.marchSendProposal(proposal)
	if sendProposal == nil {
		// 为空就是没找到, 说明不是我发起的, 返回
		return
	}

	// 找到了就取对端链取钱
	// 在发送的里面找到了符合条件的proposal就表示对端已经发了钱过来,应该切换到对端链取钱
	utils.Info("===== new proposal切换到对端链取钱 =====")
	currKeyName, err := c.makeClientMapKey(chainName, userName)
	if err != nil {
		utils.InfoError("makeClientMapKey", err)
		return
	}
	correspondingClient := c.mmptClient[currKeyName]
	if correspondingClient == nil {
		utils.InfoError("dealNewProposalEvent", fmt.Errorf("nil client error"))
		return
	}
	if err := correspondingClient.Withdraw(proposal.ProposalType, proposal.ProposalID, sendProposal.Preimage); err != nil {
		fmt.Printf("本地发送的proposal: %+v", sendProposal)
		fmt.Printf("监听到发给自己的proposal并取这个proposal: %+v", proposal)
		utils.InfoTips("取钱失败了")
		info := utils.InfoError("correspondingClient.WithDraw", err)

		utils.InfoTips("查询是否重复")
		p, err := c.getProposal(chainName, userName, proposal.ProposalType, proposal.ProposalID)
		if err != nil {
			info = utils.InfoError("切换对端链withdraw失败, 且c.getProposal失败", err)
			panic(info)
		}
		if p.Unlocked {
			utils.InfoTips("proposal已经被取过, 跳过")
		} else {
			// 取失败
			panic(info)
		}
	}

	utils.Info("====== 取钱成功 ======:", sendProposal.ProposalID)
	// 取钱成功,则当前交易完成,从sendproposal和receiveproposal中删除
	c.deleteNewSendProposal(*sendProposal)
	c.deleteNewReceiveProposal(proposal)
}

// 与自己发送的proposal进行匹配
// @param proposal 收到的proposal
// @return 匹配的Proposal指针
func (c *Client) marchSendProposal(proposal proposalstruct.Proposal) *proposalstruct.Proposal {
	// 监听到一个发给自己的proposal, 就在自己发过的proposal里面找对应的proposal并取钱
	for _, sendProposal := range c.maSendProposal {
		// 收到的chainName应该和发送的对应chainName一样
		if proposal.ChainName != sendProposal.CorrespondingChainName {
			utils.InfoTips(proposal.ChainName, sendProposal.CorrespondingChainName)
			continue
		}
		// 用户名要一样
		if proposal.UserName != sendProposal.UserName {
			utils.InfoTips(proposal.UserName, sendProposal.UserName)
			continue
		}
		// 合约名也要一样
		if proposal.ContractName != sendProposal.ContractName {
			utils.InfoTips(proposal.ContractName, sendProposal.ContractName)
			continue
		}
		// proposal类型要一样
		if proposal.ProposalType != sendProposal.ProposalType {
			utils.InfoTips(proposal.ProposalType != sendProposal.ProposalType)
			continue
		}
		// 钱要符合我一开始设定的钱
		if proposal.Amount != sendProposal.CorrespondingAmount {
			utils.InfoTips(proposal.Amount, sendProposal.CorrespondingAmount)
			continue
		}
		// 哈希锁要和我一开始设定的一样
		if proposal.ProposalType == config.PROPOSAL_TYPE_HTLC {
			if proposal.HashLock != sendProposal.HashLock {
				utils.InfoTips(proposal.HashLock, sendProposal.HashLock)
				continue
			}
			// 发送的preimage要存在表示要取对端的钱
			if sendProposal.Preimage == "" || sendProposal.Preimage == nil {
				continue
			}
		} else if proposal.ProposalType == config.PROPOSAL_TYPE_LILAC {
			hash1, err := utils.Any2StringArray(proposal.HashLock)
			if err != nil {
				continue
			}
			hash2, err := utils.Any2StringArray(sendProposal.HashLock)
			if err != nil {
				continue
			}
			if !utils.EqualStringArray(hash1, hash2) {
				utils.InfoTips(proposal.HashLock, sendProposal.HashLock)
				continue
			}
			// 发送的preimage要存在表示要取对端的钱
			preimage, err := utils.Any2StringArray(sendProposal.Preimage)
			if err != nil {
				continue
			}
			if utils.EqualStringArray(preimage, []string{}) {
				continue
			}
		} else {
			utils.Info("invalid proposal type: " + proposal.ProposalType)
			continue
		}
		// todo:时间锁判断???
		// ! 对于发起方取钱来说,只需要判断哈希锁,amount,chainName,contractName,preimage是不是就够了??

		// 找到就返回
		return &sendProposal
	}
	return nil
}
