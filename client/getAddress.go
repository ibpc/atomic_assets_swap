/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 获取用户在特定链上的地址信息
*/
package client

import "fmt"

// 获取链客户端的地址
// @param chainName 链名
// @param userName 用户名
// @return string address
// @param error err
func (c *Client) getAddress(chainName, userName string) (string, error) {
	keyName, err := c.makeClientMapKey(chainName, userName)
	if err != nil {
		return "", fmt.Errorf("getAddress chainName and userName error: %s", err)
	}

	currClient := c.mmptClient[keyName]
	if currClient == nil {
		return "", fmt.Errorf("invalid keyName: %s", keyName)
	}
	return c.mmptClient[keyName].GetAddress()
}
