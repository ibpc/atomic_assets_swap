/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 订阅new proposal和withdraw事件
*/
package client

import (
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
)

// 自动订阅需要的事件并处理
func (c *Client) autoSubscribe() {
	// 订阅proposal事件
	for keyName := range c.mmptClient {
		chainName, userName, err := c.splitClientMapKey(keyName)
		if err != nil {
			utils.InfoError("autoSubscribe splitClientMapKey", err)
			continue
		}

		// 订阅这条链的htlc new proposal事件
		go c.subscribeNewProposal(chainName, userName, config.HTLC_CONTRACT_NAME)
		// 订阅这条链的lilac new proposal事件
		go c.subscribeNewProposal(chainName, userName, config.LILAC_CONTRACT_NAME)
		// 订阅这条链的htlc withdraw事件
		go c.subscribeWithdraw(chainName, userName, config.HTLC_CONTRACT_NAME)
		// 订阅这条链的lilac withdraw事件
		go c.subscribeWithdraw(chainName, userName, config.LILAC_CONTRACT_NAME)
	}
}
