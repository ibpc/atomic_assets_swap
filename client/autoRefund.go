/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 定期检查超时proposal并执行自动退款
*/
package client

import (
	"atomic_assets_swap/utils"
	"fmt"
	"time"
)

// 超时自动退款
func (c *Client) autoRefund() {
	go func() {
		for {
			c.autoRefundAction()
			// 休眠2秒
			time.Sleep(2 * time.Second)
		}
	}()
}

// 自动退款行动
func (c *Client) autoRefundAction() {
	// 根据send列表的时间来取钱
	for _, proposal := range c.maSendProposal {
		currTime := utils.GetTimestamp()
		if currTime-proposal.PosalTime > (2 + proposal.TimeLock) {
			// 时间超过就退款
			utils.Info("proposa超时,进入退款程序:" + fmt.Sprint(proposal))
			if err := c.refund(proposal.ChainName, proposal.UserName, proposal.ContractName, proposal.ProposalID); err != nil {
				utils.InfoError("c.autoRefund", err)
			}
		}
	}
}
