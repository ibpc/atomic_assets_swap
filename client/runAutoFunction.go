/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 执行自动订阅以及自动退款程序
*/
package client

// 运行订阅处理以及自动退款
func (c *Client) RunAutoFunction() {
	// 客户端一旦执行就开一个订阅处理
	c.autoSubscribe()
	// 客户端一旦执行就开启超时自动退款
	c.autoRefund()
}
