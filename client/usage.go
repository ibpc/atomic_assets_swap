/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 打印使用方法
*/
package client

import (
	"atomic_assets_swap/utils"
	"fmt"
)

// 打印usage
func printUsage() {
	utils.InfoTips("###### usage ######")
	fmt.Print(
		"newHtlcProposal chainName userName toAddr amount Preimage hashlock timelock chain2 amount\n",
		"newLilacProposal chainName userName toAddr amount Preimage hashlock timelock chain2 amount\n",
		"refund chainName userName contractName proposalID\n",
		"withdraw chainName userName contractName proposalID preimage\n",
		"getProposal chainName userName proposalType proposalID\n",
		"getBalance chainName userName\n",
		"getAddress chainName userName\n",
		"break\n",
		"exit\n",
	)
	utils.InfoTips("###### end ######")
	fmt.Println("")
}
