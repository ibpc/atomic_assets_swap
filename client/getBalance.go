/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 获取指定链上的用户余额
*/
package client

import "fmt"

// 获取地址余额
// @param chainName 哪一条链
// @param userName 用户名
// @param address 地址
// @return string 余额
// @return error 错误信息
func (c *Client) getBalance(chainName, userName, address string) (string, error) {
	keyName, err := c.makeClientMapKey(chainName, userName)
	if err != nil {
		return "", fmt.Errorf("getBalance chainName and userName error: %s", err)
	}

	currClient := c.mmptClient[keyName]
	if currClient == nil {
		return "", fmt.Errorf("invalid keyName: %s", keyName)
	}
	balance, err := currClient.GetBalance(address)
	if err != nil {
		return "", fmt.Errorf("client.getBalance error: %s", err)
	}
	return balance, nil
}
