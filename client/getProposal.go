/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 获取链上proposal信息
*/
package client

import (
	"atomic_assets_swap/chainclient/proposalstruct"
	"atomic_assets_swap/utils"
	"fmt"
)

// 获取proposal
// @param chainName 哪一条链
// @param userName 用户名
// @param proposalID proposalID
// @return proposal proposal
// @return error 错误信息
func (c *Client) getProposal(chainName, userName, proposalType, proposalID string) (*proposalstruct.Proposal, error) {
	keyName, err := c.makeClientMapKey(chainName, userName)
	if err != nil {
		info := utils.InfoError("GetProposal chainName and userName", err)
		return nil, fmt.Errorf(info)
	}

	currClient := c.mmptClient[keyName]
	if currClient == nil {
		return nil, fmt.Errorf("invalid keyName: %s", keyName)
	}

	proposal, err := currClient.GetProposal(proposalType, proposalID)
	if err != nil {
		info := utils.InfoError("client.GetProposal", err)
		return nil, fmt.Errorf(info)
	}
	return &proposal, nil
}
