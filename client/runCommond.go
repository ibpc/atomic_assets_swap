/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 解析用户输入的指令并执行
*/
package client

import (
	"atomic_assets_swap/chainclient/proposalstruct"
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"fmt"
	"strconv"
	"strings"
)

// 发起一个新的htlc proposal
// @param c 客户端
// @param lsCmd 命令
// @return string result of proposal
// @return error err
func newHtlcProposal(c *Client, lsCmd []string) (string, error) {
	// 判断参数个数
	if len(lsCmd) != 10 {
		info := utils.Info(fmt.Sprintf("newHtlcProposal命令行参数个数错误, expect %d, get %d", 10, len(lsCmd)))
		printUsage()
		return "", fmt.Errorf(info)
	}

	// newHtlcProposal chainName userName toAddr
	// amount Preimage hashlock timelock correspondingChainName correspondingAmountStr
	chainName := lsCmd[1] //"chain1"
	userName := lsCmd[2]
	toAddr := lsCmd[3]    //chain1user2Address
	amountStr := lsCmd[4] //10
	amount, err := strconv.Atoi(amountStr)
	if err != nil {
		info := utils.Info("amount is not a number: " + amountStr)
		return "", fmt.Errorf(info)
	}

	preimage := lsCmd[5] //"preimage"
	var hashlock string
	if preimage == config.NIL {
		// 如果preimage为nil表示没有preimage
		preimage = ""
		hashlock = lsCmd[6]
	} else {
		// 有preimage就不管hashlock
		hashlock = utils.GetSha256Hash([]byte(preimage))
	}
	utils.Info("preimage", preimage)
	utils.Info("hashlock", hashlock)

	timelockStr := lsCmd[7] //600 //
	timelock, err := strconv.Atoi(timelockStr)
	if err != nil {
		info := utils.Info("timelock is not a number: " + timelockStr)
		return "", fmt.Errorf(info)
	}
	utils.Info("timelock", timelock)

	// 对端链的消息
	correspondingChainName := lsCmd[8] //"chain2"
	correspondingAmountStr := lsCmd[9] //1
	correspondingAmount, err := strconv.Atoi(correspondingAmountStr)
	if err != nil {
		info := utils.Info("correspondingAmountStr is not a number: " + correspondingAmountStr)
		return "", fmt.Errorf(info)
	}
	// newHtlcProposal chainName userName toAddr amount
	// Preimage hashlock timelock correspondingChainName correspondingAmountStr
	proposalID, err := c.newProposal(
		chainName,
		userName,
		"htlc",
		toAddr,
		amount,
		preimage,
		hashlock,
		timelock,
		correspondingChainName,
		correspondingAmount,
	)
	if err != nil {
		info := utils.InfoError("c.newHtlcProposal", err)
		printUsage()
		return "", fmt.Errorf(info)
	}
	return proposalID, nil
}

// 发起一个lilac proposal
// @param c 客户端对象
// @param lsCmd 命令
// @return string result of proposal
// @return error err
func newLilacProposal(c *Client, lsCmd []string) (string, error) {
	if len(lsCmd) != 10 {
		info := utils.Info("commond length of lilac error, expect 10, but get " + fmt.Sprint(len(lsCmd)))
		printUsage()
		return "", fmt.Errorf(info)
	}

	// newHtlcProposal chainName userName toAddr
	// amount Preimage hashlock timelock correspondingChainName correspondingAmountStr
	chainName := lsCmd[1] //"chain1"
	userName := lsCmd[2]
	toAddr := lsCmd[3]    //chain1user2Address
	amountStr := lsCmd[4] //10
	amount, err := strconv.Atoi(amountStr)
	if err != nil {
		info := utils.Info("amount is not a number: " + amountStr)
		return "", fmt.Errorf(info)
	}

	preimage := lsCmd[5] //"preimage"
	hashlock := lsCmd[6]
	var vPreimage []string
	var vHashLock []string
	if preimage == config.NIL {
		// 如果preimage为nil表示没有preimage
		vHashLock_tmp, err2 := utils.SplitStr(hashlock, ",")
		if err2 != nil {
			info := utils.Info("hashlock error: " + hashlock)
			return "", fmt.Errorf(info)
		}
		vHashLock = vHashLock_tmp
	} else {
		// 有preimage就不管hashlock,计算hashlock
		var err1 error
		// 分割preimage
		vPreimage, err1 = utils.SplitStr(preimage, ",")
		if err1 != nil {
			info := utils.Info("preimage error: " + preimage)
			return "", fmt.Errorf(info)
		}
		// 计算hash
		for _, currPreimage := range vPreimage {
			currHash := utils.GetSha256Hash([]byte(currPreimage))
			vHashLock = append(vHashLock, currHash)
		}
	}

	utils.Info("vPreimage:", vPreimage)
	utils.Info("vHashlock:", vHashLock)
	timelockStr := lsCmd[7] //600
	timelock, err := strconv.Atoi(timelockStr)
	if err != nil {
		info := utils.Info("timelock is not a number: " + timelockStr)
		return "", fmt.Errorf(info)
	}
	utils.Info("timelock", timelock)
	// 对端链的消息
	correspondingChainName := lsCmd[8] //"chain2"
	correspondingAmountStr := lsCmd[9] //1
	correspondingAmount, err := strconv.Atoi(correspondingAmountStr)
	if err != nil {
		info := utils.Info("correspondingAmountStr is not a number: " + correspondingAmountStr)
		return "", fmt.Errorf(info)
	}
	// newHtlcProposal chainName userName toAddr amount
	// Preimage hashlock timelock correspondingChainName correspondingAmountStr
	anyPreimage := utils.StringArray2Any(vPreimage)
	anyHashlock := utils.StringArray2Any(vHashLock)
	proposalID, err := c.newProposal(
		chainName,
		userName,
		"lilac",
		toAddr,
		amount,
		anyPreimage,
		anyHashlock,
		timelock,
		correspondingChainName,
		correspondingAmount,
	)
	if err != nil {
		info := utils.InfoError("c.newLilacProposal", err)
		printUsage()
		return "", fmt.Errorf(info)
	}
	return proposalID, nil
}

// 获取指定对象的余额
// @param c 客户端对象
// @param lsCmd 命令
// @return string balance of user
// @return error err
func getBalance(c *Client, lsCmd []string) (string, error) {
	// todo:是可以指定address的
	if len(lsCmd) != 3 {
		info := utils.Info("length of commond error, expect 3, but get " + fmt.Sprint(len(lsCmd)))
		printUsage()
		return "", fmt.Errorf(info)
	}

	// getBalance chainName userName
	chainName := lsCmd[1]
	userName := lsCmd[2]
	balance, err := c.getBalance(chainName, userName, "")
	if err != nil {
		info := utils.InfoError("c.getbalance", err)
		printUsage()
		return "", fmt.Errorf(info)
	}
	utils.Info(chainName, userName, "balance: ", balance)
	return balance, nil
}

// 退钱
// @param c 客户端对象
// @param lsCmd 命令
// @return string propsalID
// @return error err
func refund(c *Client, lsCmd []string) (string, error) {
	if len(lsCmd) != 5 {
		info := utils.Info("length of commond error, expect 5, but get " + fmt.Sprint(len(lsCmd)))
		printUsage()
		return "", fmt.Errorf(info)
	}

	// refund chainName userName contractName proposalID
	chainName := lsCmd[1]
	userName := lsCmd[2]
	contractName := lsCmd[3]
	proposalID := lsCmd[4]
	// 执行退钱命令
	if err := c.refund(chainName, userName, contractName, proposalID); err != nil {
		info := utils.InfoError("c.refund", err)
		return "", fmt.Errorf(info)
	}
	return proposalID, nil
}

// 获取自己的地址
// @param c 客户端对象
// @param lsCmd 命令
// @return string address
// @return error err
func getAddress(c *Client, lsCmd []string) (string, error) {
	if len(lsCmd) != 3 {
		info := utils.Info("length of commond error, expect 3, but get " + fmt.Sprint(len(lsCmd)))
		printUsage()
		return "", fmt.Errorf(info)
	}

	chainName := lsCmd[1]
	userName := lsCmd[2]
	// 执行
	address, err := c.getAddress(chainName, userName)
	if err != nil {
		info := utils.InfoError("c.getAddress", err)
		return "", fmt.Errorf(info)
	}
	utils.Info("address:" + address)
	return address, nil
}

// 取钱
// @param c 客户端对象
// @param lsCmd 命令
// @return string propsalID
// @return error err
func withdraw(c *Client, lsCmd []string) (string, error) {
	if len(lsCmd) != 6 {
		info := utils.Info("length of commond error, expect 6, but get " + fmt.Sprint(len(lsCmd)))
		printUsage()
		return "", fmt.Errorf(info)
	}

	// withdraw chainName userName contractName proposalID preimage
	chainName := lsCmd[1]
	userName := lsCmd[2]
	contractName := lsCmd[3]
	proposalID := lsCmd[4]
	preimageStr := lsCmd[5]
	var preimage interface{}
	// lilac的特殊处理
	if strings.Contains(contractName, "lilac") {
		// 分割preimage
		vPreimage, err := utils.SplitStr(preimageStr, ",")
		if err != nil {
			info := utils.InfoError("splitStr preimage", err)
			return "", fmt.Errorf(info)
		}
		preimage = utils.StringArray2Any(vPreimage)
	} else {
		preimage = preimageStr
	}

	if err := c.withdraw(chainName, userName, contractName, proposalID, preimage); err != nil {
		info := utils.InfoError("c.refund", err)
		return "", fmt.Errorf(info)
	}
	return proposalID, nil
}

// 获取proposal
// @param c 客户端对象
// @param lsCmd 命令
// @return proposal proposal
// @return error err
func getProposal(c *Client, lsCmd []string) (proposalstruct.Proposal, error) {
	// getProposal chainName userName proposalType proposalID
	if len(lsCmd) != 5 {
		info := utils.Info("length of commond error, expect 5, but get " + fmt.Sprint(len(lsCmd)))
		printUsage()
		return proposalstruct.Proposal{}, fmt.Errorf(info)
	}

	chainName := lsCmd[1]
	userName := lsCmd[2]
	proposalType := lsCmd[3]
	proposalID := lsCmd[4]
	// 读取一个proposal
	proposal, err := c.getProposal(chainName, userName, proposalType, proposalID)
	if err != nil {
		info := utils.InfoError("c.getProposal", err)
		return proposalstruct.Proposal{}, fmt.Errorf(info)
	}
	utils.Info("proposal:", *proposal)
	return *proposal, nil
}

// 执行客户端程序
// @param lsCmd 命令行, 一行一个命令
func (c *Client) RunCommond(lsCmd []string) (interface{}, error) {
	if len(lsCmd) < 1 {
		info := utils.Info("命令行参数错误")
		return "", fmt.Errorf(info)
	}

	switch lsCmd[0] {
	case "break":
		// 退出
		return config.EXIT, nil
	case config.EXIT:
		// 退出
		return config.EXIT, nil
	case "newHtlcProposal":
		// 新的htlc proposal
		return newHtlcProposal(c, lsCmd)
	case "newLilacProposal":
		// 新的lilac proposal
		return newLilacProposal(c, lsCmd)
	case "getBalance":
		// 获取自己的地址
		return getBalance(c, lsCmd)
	case "refund":
		// 退钱
		return refund(c, lsCmd)
	case "withdraw":
		// 取钱
		return withdraw(c, lsCmd)
	case "getAddress":
		// 获取当前客户端地址
		return getAddress(c, lsCmd)
	case "getProposal":
		// 获取proposal
		return getProposal(c, lsCmd)
	default:
		printUsage()
	}
	return "", fmt.Errorf("invalid comand")
}
