/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 等待用户输入指定并执行
*/
package client

import (
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
)

// 执行客户端程序
func (c *Client) Run() {
	// 开启自动订阅以及退款
	c.RunAutoFunction()

	for {
		// 读取输入
		lsCmd, err := utils.ReadInputs()
		if err != nil {
			utils.InfoError("utils.ReadInputs", err)
			continue
		}
		if len(lsCmd) == 0 {
			utils.Info("invalid command")
			continue
		}

		// 执行命令
		result, err := c.RunCommond(lsCmd)
		if err != nil {
			utils.Info("run commond: ", lsCmd)
			utils.InfoError("run commond", err)
			continue
		}
		if result == config.EXIT {
			break
		}

	} //for
	utils.Info("login out client.")
}
