/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 执行退款程序, 包括:
	1. htlc退款
	2. lilac退款
*/
// coding:utf-8
// 取钱
package client

import (
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"fmt"
	"strings"
)

// 撤回指定链上指定合约的钱
// @param chainName 链名
// @param userName 用户名
// @param contractName 合约名
// @param proposalID id
// @return error
func (c *Client) withdraw(chainName, userName, contractName, proposalID string, preimage interface{}) error {
	keyName, err := c.makeClientMapKey(chainName, userName)
	if err != nil {
		return fmt.Errorf("withdraw chainName and userName error: %s", err)
	}

	// 当前客户端
	currClient := c.mmptClient[keyName]
	if currClient == nil {
		return fmt.Errorf("can not find client in chain : %s", keyName)
	}

	// 判断合约类型
	var proposalType string
	if strings.Contains(contractName, config.PROPOSAL_TYPE_HTLC) {
		proposalType = config.PROPOSAL_TYPE_HTLC
	} else if strings.Contains(contractName, config.PROPOSAL_TYPE_LILAC) {
		proposalType = config.PROPOSAL_TYPE_LILAC
	} else {
		err := utils.Info("invalid contractName: " + contractName)
		return fmt.Errorf(err)
	}
	// 取钱并处理错误信息
	if err := currClient.Withdraw(proposalType, proposalID, preimage); dealWithdrawError(err) != nil {
		panic("当前客户端withdraw错误:" + err.Error())
		// return fmt.Errorf("c.Withdraw error: %s", err)
	}
	// 取钱成功就从proposal里面删掉刚刚加入进去的propsal
	for index, p := range c.maSendProposal {
		if p.ContractName == contractName && p.ProposalID == proposalID &&
			p.ChainName == chainName && p.UserName == userName {
			// 找到就删除
			c.deleteNewSendProposalByIndex(index)
			break
		}
	}
	return nil
}

// 处理退钱失败的情况
// @param err 返回的错误信息
func dealWithdrawError(err error) error {
	if err == nil {
		return nil
	}
	msg := err.Error()
	// 没有锁或已经解锁
	if strings.Contains(msg, "locked") {
		utils.InfoWarning(err)
		return nil
	}
	// 已经退回
	if strings.Contains(msg, "rolledback") {
		utils.InfoWarning(err)
		return nil
	}
	return err
}
