/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 获取用户当前接收到的proposal
*/
package client

import "atomic_assets_swap/chainclient/proposalstruct"

// 获取当前节点接收到的proposal
func (c *Client) GetReceiveProposal() []proposalstruct.Proposal {
	return c.maReceiveProposal
}
