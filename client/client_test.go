/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 实例化一个用户客户端对象
*/
package client

import (
	"atomic_assets_swap/chainclient/proposalstruct"
	"atomic_assets_swap/config"
	"atomic_assets_swap/projectinterface"
	"atomic_assets_swap/utils"
	"testing"
	"time"

	"bou.ke/monkey"
	"chainmaker.org/chainmaker/pb-go/v2/common"
)

// 用户客户端对象
var client *Client

// chainmaker test sdk
type cChainmaker struct {
}

func (s *cChainmaker) NewProposal(propsalType, toAddr, amount string, hashlock, timelock interface{}) (string, error) {
	return "chain1proposalid", nil
}
func (s *cChainmaker) Refund(proposalType, proposalID string) error {
	return nil
}
func (s *cChainmaker) Withdraw(proposalType, proposlID string, preimage interface{}) error {
	return nil
}
func (s *cChainmaker) SubscribeEvent(contractName, eventName string, resultChan chan<- interface{}) {
	// todo:发送一个chainmaker事件
	event := &common.ContractEventInfo{}
	resultChan <- event
}
func (s *cChainmaker) ParseEventToProposal(eventInfo interface{}) (proposalstruct.Proposal, error) {
	result := &proposalstruct.Proposal{}
	result.Receiver = config.TEST_CHAIN1_ADDRESS
	result.ProposalType = "htlc"
	return *result, nil
}
func (s *cChainmaker) GetBalance(address string) (string, error) {
	return "balance", nil
}
func (s *cChainmaker) GetProposal(proposalType, proposalID string) (proposalstruct.Proposal, error) {
	return proposalstruct.Proposal{}, nil
}
func (s *cChainmaker) GetAddress() (string, error) {
	return config.TEST_CHAIN1_ADDRESS, nil
}
func (s *cChainmaker) GetChainID() (string, error) {
	return config.CHAIN1, nil
}
func (s *cChainmaker) Close() {}

// fabric test sdk
type cFabric struct {
}

func (s *cFabric) NewProposal(propsalType, toAddr, amount string, hashlock, timelock interface{}) (string, error) {
	return "fabricproposalid", nil
}
func (s *cFabric) Refund(proposalType, proposalID string) error {
	return nil
}
func (s *cFabric) Withdraw(proposalType, proposlID string, preimage interface{}) error {
	return nil
}
func (s *cFabric) SubscribeEvent(contractName, eventName string, resultChan chan<- interface{}) {
	// todo:发送一个fabric事件
}
func (s *cFabric) ParseEventToProposal(eventInfo interface{}) (proposalstruct.Proposal, error) {
	return proposalstruct.Proposal{}, nil
}
func (s *cFabric) GetBalance(address string) (string, error) {
	return "900", nil
}
func (s *cFabric) GetProposal(proposalType, proposalID string) (proposalstruct.Proposal, error) {
	return proposalstruct.Proposal{}, nil
}
func (s *cFabric) GetAddress() (string, error) {
	return "fabricaddress", nil
}
func (s *cFabric) GetChainID() (string, error) {
	return config.FABRIC, nil
}
func (s *cFabric) Close() {}

// 初始化，打桩
func initFunc() {
	monkey.Patch(newChainmakerClient, func(interface{}) (projectinterface.ChainClientInterface, error) {
		cc := &cChainmaker{}
		return cc, nil
	})
	monkey.Patch(newFabricClient, func(interface{}) (projectinterface.ChainClientInterface, error) {
		cc := &cFabric{}
		return cc, nil
	})
}

// 测试NewClient
func TestNewClient(t *testing.T) {
	mpConfigPath := make(map[string]interface{})
	// mpConfigPath["chain1_user1"] = config.TEST_USER_CERT_PATH
	// mpConfigPath["fabric_user1"] = utils.StringArray2Any([]string{"../config_files/fabric/user.yaml", "Org1"})

	// 测试不存在的客户端类型
	if _, err := NewClient("invalidType", mpConfigPath); err == nil {
		panic("invalid type of client error: err == nil error")
	}

	// 测试用户名格式错误
	mpConfigPath["wronguser"] = config.TEST_USER_CERT_PATH
	if _, err := NewClient("cmdClient", mpConfigPath); err == nil {
		panic("wrong user name error: err == nil error")
	}
	delete(mpConfigPath, "wronguser")
	mpConfigPath["chain1_user1_something"] = config.TEST_USER_CERT_PATH
	if _, err := NewClient("cmdClient", mpConfigPath); err == nil {
		panic("wrong chain name error: err == nil error")
	}
	delete(mpConfigPath, "chain1_user1_something")

	// 测试链名错误
	mpConfigPath["wrongchainname_user1"] = config.TEST_USER_CERT_PATH
	if _, err := NewClient("cmdClient", mpConfigPath); err == nil {
		panic("wrong chain name error: err == nil error")
	}
	delete(mpConfigPath, "wrongchainname_user1")

	// 测试长安链
	mpConfigPath["chain1_user1"] = config.TEST_USER_CERT_PATH
	if _, err := NewClient("cmdClient", mpConfigPath); err != nil {
		panic(err)
	}
	delete(mpConfigPath, "chain1_user1")

	// 测试fabric
	mpConfigPath["fabric_user1"] = utils.StringArray2Any([]string{"../config_files/fabric/user.yaml", "Org1"})
	if _, err := NewClient("cmdClient", mpConfigPath); err != nil {
		panic(err)
	}
	delete(mpConfigPath, "fabric_user1")

	// 获取客户端对象
	mpConfigPath["chain1_user1"] = config.TEST_USER_CERT_PATH
	mpConfigPath["fabric_user1"] = utils.StringArray2Any([]string{"../config_files/fabric/user.yaml", "Org1"})
	var err error
	client, err = NewClient("cmdClient", mpConfigPath)
	if err != nil {
		panic(err)
	}
}

// 测试新的htlc proposal
func TestNewHtlcProposal(t *testing.T) {
	ls_cmd := []string{
		"newHtlcProposal", // 0
		config.CHAIN1,     // 1
		"user1",           // 2
		"fabricaddress",   // 3
		"1",               // 4
		"preimage",        // 5
		"nil",             // 6
		"600",             // 7
		config.CHAIN1,     // 8
		"1",               // 9
	}
	// 命令个数错误
	if _, err := client.RunCommond([]string{"newHtlcProposal h a a"}); err == nil {
		panic("length of commond error: err == nil error")
	}

	// amount错误
	curr_cmd := [10]string{}
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[4] = "amount1"
	if _, err := client.RunCommond(curr_cmd[:]); err == nil {
		panic("length of commond error: err == nil error")
	}
	// 时间锁错误
	curr_cmd = [10]string{}
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[7] = "timelock1"
	if _, err := client.RunCommond(curr_cmd[:]); err == nil {
		panic("timelock error: err == nil error")
	}
	// 对端链收钱数错误
	curr_cmd = [10]string{}
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[9] = "wrong number1"
	if _, err := client.RunCommond(curr_cmd[:]); err == nil {
		panic("corresponding number error: err == nil error")
	}
	// chainname或者username字段错误
	curr_cmd = [10]string{}
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[1] = config.TEST_ERR_CHAIN_NAME
	curr_cmd[2] = config.TEST_ERR_USER_NAME
	if _, err := client.RunCommond(curr_cmd[:]); err == nil {
		panic("chainname or username include illegal char error: err == nil error")
	}
	// 链名或用户名不存在
	curr_cmd = [10]string{}
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[1] = config.CHAIN1
	curr_cmd[2] = config.TEST_USER_NAME
	if _, err := client.RunCommond(curr_cmd[:]); err == nil {
		panic("chainname or username not exists error: err == nil error")
	}

	// 正常发htlc proposal
	if _, err := client.RunCommond(ls_cmd); err != nil {
		panic(err)
	}
}

// 测试新的lilac proposal
func TestNewLilacProposal(t *testing.T) {
	ls_cmd := []string{
		"newLilacProposal",    // 0
		config.CHAIN1,         // 1
		"user1",               // 2
		"fabricaddress",       // 3
		"1",                   // 4
		"preimage1,preimage2", // 5
		"nil",                 // 6
		"600",                 // 7
		config.CHAIN1,         // 8
		"1",                   // 9
	}
	// 命令个数错误
	if _, err := client.RunCommond([]string{"newLilacProposal h a a"}); err == nil {
		panic("length of commond error: err == nil error")
	}

	// amount错误
	curr_cmd := [10]string{}
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[4] = "amount"
	if _, err := client.RunCommond(curr_cmd[:]); err == nil {
		panic("length of commond error: err == nil error")
	}
	// 时间锁错误
	curr_cmd = [10]string{}
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[7] = "timelock"
	if _, err := client.RunCommond(curr_cmd[:]); err == nil {
		panic("timelock error: err == nil error")
	}
	// 对端链收钱数错误
	curr_cmd = [10]string{}
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[9] = "wrong number"
	if _, err := client.RunCommond(curr_cmd[:]); err == nil {
		panic("corresponding number error: err == nil error")
	}
	// chainname或者username字段错误
	curr_cmd = [10]string{}
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[1] = config.TEST_ERR_CHAIN_NAME
	curr_cmd[2] = config.TEST_ERR_USER_NAME
	if _, err := client.RunCommond(curr_cmd[:]); err == nil {
		panic("chainname or username include illegal char error: err == nil error")
	}
	// 链名或用户名不存在
	curr_cmd = [10]string{}
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[1] = config.CHAIN1
	curr_cmd[2] = config.TEST_USER_NAME
	if _, err := client.RunCommond(curr_cmd[:]); err == nil {
		panic("chainname or username not exists error: err == nil error")
	}

	// 正常发
	if _, err := client.RunCommond(ls_cmd); err != nil {
		panic(err)
	}
}

// 测试get balance
func TestGetBalance(t *testing.T) {
	ls_cmd := []string{
		"getBalance",  // 0
		config.CHAIN1, // 1
		"user1",       // 2
	}
	// 命令个数错误
	if _, err := client.RunCommond([]string{"getBalance h a a"}); err == nil {
		panic("length of commond error: err == nil error")
	}

	// chainname或者username字段错误
	curr_cmd := [3]string{}
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[1] = config.TEST_ERR_CHAIN_NAME
	curr_cmd[2] = config.TEST_ERR_USER_NAME
	if _, err := client.RunCommond(curr_cmd[:]); err == nil {
		panic("chainname or username include illegal char error: err == nil error")
	}
	// 链名或用户名不存在
	curr_cmd = [3]string{}
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[1] = config.CHAIN1
	curr_cmd[2] = config.TEST_USER_NAME
	if _, err := client.RunCommond(curr_cmd[:]); err == nil {
		panic("chainname or username not exists error: err == nil error")
	}

	// 正常发
	if _, err := client.RunCommond(ls_cmd); err != nil {
		panic(err)
	}
}

// 测试refund
func TestRefund(t *testing.T) {
	ls_cmd := []string{
		"refund",           // 0
		config.CHAIN1,      // 1
		"user1",            // 2
		"htlc",             //3
		"chain1proposalid", //4
	}
	// 命令个数错误
	if _, err := client.RunCommond([]string{"refund a aa aa h a a"}); err == nil {
		panic("length of commond error: err == nil error")
	}

	// chainname或者username字段错误
	curr_cmd := [5]string{}
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[1] = config.TEST_ERR_CHAIN_NAME
	curr_cmd[2] = config.TEST_ERR_USER_NAME
	if _, err := client.RunCommond(curr_cmd[:]); err == nil {
		panic("chainname or username include illegal char error: err == nil error")
	}
	// 链名或用户名不存在
	curr_cmd = [5]string{}
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[1] = config.CHAIN1
	curr_cmd[2] = config.TEST_USER_NAME
	if _, err := client.RunCommond(curr_cmd[:]); err == nil {
		panic("chainname or username not exists error: err == nil error")
	}

	// 正常发htlc
	if _, err := client.RunCommond(ls_cmd); err != nil {
		panic(err)
	}

	// 正常发lilac
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[3] = config.PROPOSAL_TYPE_LILAC
	if _, err := client.RunCommond(ls_cmd); err != nil {
		panic(err)
	}
}

// 测试withdraw
func TestWithdraw(t *testing.T) {
	ls_cmd := []string{
		"withdraw",         // 0
		config.CHAIN1,      // 1
		"user1",            // 2
		"htlc",             //3
		"chain1proposalid", //4
		"preimage",         //5
	}
	// 命令个数错误
	if _, err := client.RunCommond([]string{"refund a aa aa h a a"}); err == nil {
		panic("length of commond error: err == nil error")
	}

	// chainname或者username字段错误
	curr_cmd := [6]string{}
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[1] = config.TEST_ERR_CHAIN_NAME
	curr_cmd[2] = config.TEST_ERR_USER_NAME
	if _, err := client.RunCommond(curr_cmd[:]); err == nil {
		panic("chainname or username include illegal char error: err == nil error")
	}
	// 链名或用户名不存在
	curr_cmd = [6]string{}
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[1] = config.CHAIN1
	curr_cmd[2] = config.TEST_USER_NAME
	if _, err := client.RunCommond(curr_cmd[:]); err == nil {
		panic("chainname or username not exists error: err == nil error")
	}

	// 正常发
	if _, err := client.RunCommond(ls_cmd); err != nil {
		panic(err)
	}

	// 正常发lilac
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[3] = config.PROPOSAL_TYPE_LILAC
	if _, err := client.RunCommond(ls_cmd); err != nil {
		panic(err)
	}
}

// 测试get address
func TestGetAddress(t *testing.T) {
	ls_cmd := []string{
		"getAddress",  // 0
		config.CHAIN1, // 1
		"user1",       // 2
	}
	// 命令个数错误
	if _, err := client.RunCommond([]string{"getAddress b bb h a a"}); err == nil {
		panic("length of commond error: err == nil error")
	}

	// chainname或者username字段错误
	curr_cmd := [3]string{}
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[1] = config.TEST_ERR_CHAIN_NAME
	curr_cmd[2] = config.TEST_ERR_USER_NAME
	if _, err := client.RunCommond(curr_cmd[:]); err == nil {
		panic("chainname or username include illegal char error: err == nil error")
	}
	// 链名或用户名不存在
	curr_cmd = [3]string{}
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[1] = config.CHAIN1
	curr_cmd[2] = config.TEST_USER_NAME
	if _, err := client.RunCommond(curr_cmd[:]); err == nil {
		panic("chainname or username not exists error: err == nil error")
	}

	// 正常发
	if _, err := client.RunCommond(ls_cmd); err != nil {
		panic(err)
	}
}

// 测试refund
func TestGetProposal(t *testing.T) {
	ls_cmd := []string{
		"getProposal",      // 0
		config.CHAIN1,      // 1
		"user1",            // 2
		"htlc",             //3
		"chain1proposalid", //4
	}
	// 命令个数错误
	if _, err := client.RunCommond([]string{"refund a aa aa h a a"}); err == nil {
		panic("length of commond error: err == nil error")
	}

	// chainname或者username字段错误
	curr_cmd := [5]string{}
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[1] = config.TEST_ERR_CHAIN_NAME
	curr_cmd[2] = config.TEST_ERR_USER_NAME
	if _, err := client.RunCommond(curr_cmd[:]); err == nil {
		panic("chainname or username include illegal char error: err == nil error")
	}
	// 链名或用户名不存在
	curr_cmd = [5]string{}
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[1] = config.CHAIN1
	curr_cmd[2] = config.TEST_USER_NAME
	if _, err := client.RunCommond(curr_cmd[:]); err == nil {
		panic("chainname or username not exists error: err == nil error")
	}

	// 正常发htlc
	if _, err := client.RunCommond(ls_cmd); err != nil {
		panic(err)
	}

	// 正常发lilac
	copy(curr_cmd[:], ls_cmd)
	curr_cmd[3] = config.PROPOSAL_TYPE_LILAC
	if _, err := client.RunCommond(ls_cmd); err != nil {
		panic(err)
	}
}

// 其他功能测试
func TestOtherFunc(t *testing.T) {
	// 测试命令个数错误
	if _, err := client.RunCommond([]string{}); err == nil {
		panic("nil commond error: err==nil error")
	}

	// 开启订阅
	client.autoSubscribe()
	time.Sleep(2 * time.Second)

	// 测试退出
	if _, err := client.RunCommond([]string{"break"}); err != nil {
		panic(err)
	}
	if _, err := client.RunCommond([]string{"exit"}); err != nil {
		panic(err)
	}

	// 测试get proposal
	client.GetReceiveProposal()
	client.GetSendProposal()
	client.RunAutoFunction()
}

func TestMain(m *testing.M) {
	utils.InfoTips("开始测试用户客户端")
	initFunc()
	m.Run()
	utils.InfoTips("用户客户端测试完成")
}
