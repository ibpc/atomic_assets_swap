/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 获取适配的链SDK对象, 包括:
1. chain1(chainmaker)的sdk对象
2. chain2(chainmaker)的sdk对象
3. fabric的sdk对象
*/
package client

import (
	"atomic_assets_swap/chainclient/chainmaker/chainmakerclient"
	"atomic_assets_swap/chainclient/fabric/fabricclient"
	"atomic_assets_swap/projectinterface"
	"atomic_assets_swap/utils"
	"fmt"
)

// 新建一个chainmaker链客户端
// @param filePath 用户配置文件
// @return chainclient 链客户端
// @return error err
func newChainmakerClient(filePath interface{}) (projectinterface.ChainClientInterface, error) {
	userConfigPath, ok := filePath.(string)
	if !ok {
		return nil, fmt.Errorf("invalid type of filePath")
	}
	// 判断文件是否存在
	if !utils.FileExist(userConfigPath) {
		return nil, fmt.Errorf("newChainmakerClient file not exist: %s", userConfigPath)
	}
	// 链客户端
	currClient, err := chainmakerclient.NewClient(userConfigPath)
	if err != nil {
		return nil, fmt.Errorf("newChainmakerClient.NewClient error: %s", err)
	}
	return currClient, nil
}

// 新建一个fabric链客户端
// @param filePath_orgName []string{用户配置文件路径, 组织名, 用户名}
func newFabricClient(filePath_orgName interface{}) (projectinterface.ChainClientInterface, error) {
	asConfig, err := utils.Any2StringArray(filePath_orgName)
	if err != nil {
		return nil, fmt.Errorf("newFabricClient error: %s", err)
	}
	if len(asConfig) != 3 {
		info := utils.InfoError(
			"newFabricClient",
			fmt.Errorf("length of input config must be equal to 3, but get %d", len(asConfig)),
		)
		return nil, fmt.Errorf(info)
	}
	// 配置文件路径
	configYamlPath := asConfig[0]

	// 判断文件是否存在
	if !utils.FileExist(configYamlPath) {
		return nil, fmt.Errorf("newFabricClient file not exist: %s", configYamlPath)
	}
	// 链客户端
	currClient, err := fabricclient.NewClient(asConfig)
	if err != nil {
		return nil, fmt.Errorf("newFabricClient.NewClient error: %s", err)
	}
	return currClient, nil
}
