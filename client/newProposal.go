/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 在指定链上发起一个指定类型的proposal, 包括:
1. 发起htlc proposal
2. 发起lilac proposal
*/
package client

import (
	"atomic_assets_swap/chainclient/proposalstruct"
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"fmt"
)

// 发起一个新的htlc proposal
// @param chainName 链名
// @param userName 用户名
// @param toAddr 接收者地址
// @param amount 发送的数量
// @param preimage 原像
// @param timelock 时间锁
// @param correspondingChainName 对端链的名字
// @param correspondingAmount 对端链给我的金额
// @return string str
// @return error
func (c *Client) newProposal(
	chainName,
	userName,
	proposalType,
	toAddr string,
	amount int,
	preimage,
	hashlock interface{},
	timelock int,
	correspondingChainName string,
	correspondingAmount int,
) (string, error) {
	keyName, err := c.makeClientMapKey(chainName, userName)
	if err != nil {
		info := utils.InfoError("newProposal chainName and userName", err)
		return "", fmt.Errorf(info)
	}

	// 当前客户端
	currClient := c.mmptClient[keyName]
	if currClient == nil {
		return "", fmt.Errorf("can not find client in chain : %s", keyName)
	}

	// 合约名
	var contractName string
	switch proposalType {
	case config.PROPOSAL_TYPE_HTLC:
		contractName = config.HTLC_CONTRACT_NAME
	case config.PROPOSAL_TYPE_LILAC:
		contractName = config.LILAC_CONTRACT_NAME
	default:
		info := utils.Info("proposalType is illegal: " + proposalType)
		return "", fmt.Errorf(info)
	}
	// 发起proposal
	utils.InfoTips("发起时的hashlock", hashlock)
	proposalID, err := currClient.NewProposal(proposalType, toAddr, fmt.Sprint(amount), hashlock, fmt.Sprint(timelock))
	if err != nil {
		info := utils.InfoError("Client newHtlcProposal", err)
		return "", fmt.Errorf(info)
	}
	utils.Info("newProposal:" + proposalID)

	// 把proposal加入到send proposal里面
	sender, _ := currClient.GetAddress()
	proposal := proposalstruct.Proposal{
		ChainName:              chainName,
		UserName:               userName,
		ContractName:           contractName,
		ProposalType:           proposalType,
		CorrespondingAmount:    int64(correspondingAmount),
		CorrespondingChainName: correspondingChainName,
		Sender:                 sender,
		Receiver:               toAddr,
		Amount:                 int64(amount),
		Preimage:               preimage,
		HashLock:               hashlock,
		TimeLock:               int64(timelock),
		ProposalID:             proposalID,
		PosalTime:              utils.GetTimestamp(),
	}
	if err := c.addNewSendProposal(proposal); err != nil {
		info := utils.InfoError("c.addNewSendProposal", err)
		return "", fmt.Errorf(info)
	}
	utils.InfoTips("proposal加入完成", proposal.ProposalID)
	utils.Info(proposal)

	return proposalID, nil
}
