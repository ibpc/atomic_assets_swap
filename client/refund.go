/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 对给定的proposal进行取款操作, 包括:
1. 退款htlc proposal
2. 退款lilac proposal
*/
package client

import (
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"fmt"
	"strings"
)

// 在指定链上退指定合约上的proposal的钱
// @param chainName 链名
// @param userName 用户名
// @param contractName 合约名
// @param proposalID proposal id
// @return error
func (c *Client) refund(chainName, userName, contractName, proposalID string) error {
	keyName, err := c.makeClientMapKey(chainName, userName)
	if err != nil {
		return fmt.Errorf("refund chainName and userName error: %s", err)
	}

	// 当前客户端
	currClient := c.mmptClient[keyName]
	if currClient == nil {
		return fmt.Errorf("can not find client in chain : %s", keyName)
	}

	// 判断合约类型
	var proposalType string
	if strings.Contains(contractName, config.PROPOSAL_TYPE_HTLC) {
		proposalType = config.PROPOSAL_TYPE_HTLC
	} else if strings.Contains(contractName, config.PROPOSAL_TYPE_LILAC) {
		proposalType = config.PROPOSAL_TYPE_LILAC
	} else {
		err := utils.Info("invalid contractName: " + contractName)
		return fmt.Errorf(err)
	}
	// 退钱并处理可能的退钱失败场景
	if err = currClient.Refund(proposalType, proposalID); dealRefundError(err) != nil {
		// 判断具体的错误情况
		panic("当前客户端退钱失败:" + err.Error())
		// return fmt.Errorf("c.Refund error: %s", err)
	}
	// 退钱成功就从proposal里面删掉刚刚加入进去的propsal
	for index, p := range c.maSendProposal {
		if p.ContractName == contractName && p.ProposalID == proposalID &&
			p.ChainName == chainName && p.UserName == userName {
			// 找到就删除
			c.deleteNewSendProposalByIndex(index)
			break
		}
	}
	return nil
}

// 处理退钱失败的情况
// @param err 返回的错误信息
func dealRefundError(err error) error {
	if err == nil {
		return nil
	}
	msg := err.Error()
	// 没有锁或者已经解锁
	if strings.Contains(msg, "locked") {
		utils.InfoWarning(err)
		return nil
	}
	// 已经退回
	if strings.Contains(msg, "proposal's amount has") {
		utils.InfoWarning(err)
		return nil
	}
	return err
}
