/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 保存和删除本地提案信息的互斥操作
*/
package client

import (
	"atomic_assets_swap/chainclient/proposalstruct"
	"atomic_assets_swap/config"
	"atomic_assets_swap/utils"
	"fmt"
)

// 往接收的htlc proposal里面增加一个proposal
// @param proposal 增加的proposal
// @return error 如果这个proposal存在,则返回错误
func (c *Client) addNewReceiveProposal(proposal proposalstruct.Proposal) error {
	c.mtMutex.Lock()
	// todo:判重
	c.maReceiveProposal = append(c.maReceiveProposal, proposal)
	c.mtMutex.Unlock()
	return nil
}

// 往接收的htlc proposal里面删除指定index的proposal
// @param proposal 删除的proposal
func (c *Client) deleteNewReceiveProposal(proposal proposalstruct.Proposal) {
	c.mtMutex.Lock()
	for index, p := range c.maReceiveProposal {
		if utils.EqualProposal(p, proposal) {
			c.maReceiveProposal = append(c.maReceiveProposal[:index], c.maReceiveProposal[index+1:]...)
			break
		}
	}
	c.mtMutex.Unlock()
}

// 往发送的htlc proposal里面增加一个proposal
// @param proposal 增加的proposal
// @return error 如果这个proposal存在,则返回错误
func (c *Client) addNewSendProposal(proposal proposalstruct.Proposal) error {
	c.mtMutex.Lock()
	// todo:判重
	c.maSendProposal = append(c.maSendProposal, proposal)
	c.mtMutex.Unlock()
	return nil
}

// 往发送的htlc proposal里面删除指定index的proposal
// @param index 删除的index
func (c *Client) deleteNewSendProposalByIndex(index int) {
	c.mtMutex.Lock()
	c.maSendProposal = append(c.maSendProposal[:index], c.maSendProposal[index+1:]...)
	c.mtMutex.Unlock()
}

// 往接收的htlc proposal里面删除指定index的proposal
// @param proposal 删除的proposal
func (c *Client) deleteNewSendProposal(proposal proposalstruct.Proposal) {
	c.mtMutex.Lock()
	for index, p := range c.maSendProposal {
		if utils.EqualProposal(p, proposal) {
			c.maSendProposal = append(c.maSendProposal[:index], c.maSendProposal[index+1:]...)
			break
		}
	}
	c.mtMutex.Unlock()
}

// 往接收的htlc proposal里面删除指定index的proposal
// @param proposal 删除的proposal
func (c *Client) deleteNewSendProposalByParams(proposal proposalstruct.Proposal) {
	c.mtMutex.Lock()
	for index, p := range c.maSendProposal {
		if p.Sender != proposal.Sender {
			continue
		}
		if p.Receiver != proposal.Receiver {
			continue
		}
		if p.TimeLock != proposal.TimeLock {
			continue
		}
		if p.ProposalID != proposal.ProposalID {
			continue
		}
		if p.ChainName != proposal.ChainName {
			continue
		}
		if p.ContractName != proposal.ContractName {
			continue
		}
		if p.ProposalType != proposal.ProposalType {
			continue
		}
		// hashlock, proposalID, chainName, contractName, proposalName
		if p.ProposalType == config.PROPOSAL_TYPE_HTLC {
			if p.HashLock != proposal.HashLock {
				continue
			}
		} else if p.ProposalType == config.PROPOSAL_TYPE_LILAC {
			hash1, err := utils.Any2StringArray(p.HashLock)
			if err != nil {
				continue
			}
			hash2, err := utils.Any2StringArray(proposal.HashLock)
			if err != nil {
				continue
			}
			if !utils.EqualStringArray(hash1, hash2) {
				continue
			}
		} else {
			utils.InfoError("c.deleteNewSendProposalByParams", fmt.Errorf("%s", "invalid proposal type:"+p.ProposalType))
			continue
		}
		c.maSendProposal = append(c.maSendProposal[:index], c.maSendProposal[index+1:]...)
		break
	}
	c.mtMutex.Unlock()
}
