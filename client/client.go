/*
Copyright (C) ICT(Institute of Computing Technology, Chinese Academy of Sciences). All rights reserved.

SPDX-License-Identifier: Apache-2.0

@Author randongchuan,jch
@Description 实例化一个用户客户端对象
*/
// Package client 用户客户端实现
package client

import (
	"atomic_assets_swap/chainclient/proposalstruct"
	"atomic_assets_swap/config"
	"atomic_assets_swap/projectinterface"
	"atomic_assets_swap/utils"
	"fmt"
	"strings"
	"sync"
)

type Client struct {
	// 当前用户在每一条链上发起交易与订阅的客户端
	mmptClient map[string]projectinterface.ChainClientInterface
	// 全部链接收的 proposal
	maReceiveProposal []proposalstruct.Proposal
	// 全部链发送的 proposal
	maSendProposal []proposalstruct.Proposal
	// 互斥锁
	mtMutex sync.Mutex
}

// 组合client的key
// @param chainName 链名
// @param userName 用户名
// @return 链名_用户名
func (c *Client) makeClientMapKey(chainName, userName string) (string, error) {
	// 用户名和链名不允许出现下划线
	if strings.Contains(chainName, "_") {
		return "", fmt.Errorf("chainName contains \"_\" prohibited")
	}
	if strings.Contains(userName, "_") {
		return "", fmt.Errorf("userName contains \"_\" prohibited")
	}
	return chainName + "_" + userName, nil
}

// 分割client的key为chainName和userName
// @param keyName chainName_userName
// @return chainName, userName, error
func (c *Client) splitClientMapKey(keyName string) (string, string, error) {
	ls_result, err := utils.SplitStr(keyName, "_")
	if err != nil {
		return "", "", fmt.Errorf("splitClientMapKey error: %s", err)
	}
	if len(ls_result) != 2 {
		return "", "", fmt.Errorf("length of keyName must be equal to 2")
	}
	// chainName, userName, error
	return ls_result[0], ls_result[1], nil
}

// 初始化一个cmd客户端
// @param mpChain2UserConfigPath 用户在每一条链上的配置文件路径
// @return 客户端, error
func newCmdClient(mpChain2UserConfigPath map[string]interface{}) (*Client, error) {
	ptClient := &Client{}
	ptClient.mmptClient = make(map[string]projectinterface.ChainClientInterface)

	// 初始化每一个客户端
	for keyName, userConfigPath := range mpChain2UserConfigPath {
		chainName, userName, err := ptClient.splitClientMapKey(keyName)
		if err != nil {
			return nil, fmt.Errorf("newCmdClient.splitClientMapKey error: %s", err)
		}
		var currClient projectinterface.ChainClientInterface

		switch chainName {
		case config.CHAIN1:
			// chain1是chainmaker
			currClient, err = newChainmakerClient(userConfigPath)
			if err != nil {
				return nil, fmt.Errorf("newCmdClient.newChain1Client error: %s", err)
			}
		case config.CHAIN2:
			// chain2也是chainmaker
			currClient, err = newChainmakerClient(userConfigPath)
			if err != nil {
				return nil, fmt.Errorf("newCmdClient.newChain2Client error: %s", err)
			}
		case config.FABRIC:
			// fabric需要加入用户名到第三个参数
			asConfig, err1 := utils.Any2StringArray(userConfigPath)
			if err1 != nil {
				return nil, fmt.Errorf("newCmdClient.newFabricClient error: %s", err1)
			}
			asConfig = append(asConfig, userName)
			fabric_config := utils.StringArray2Any(asConfig)
			currClient, err = newFabricClient(fabric_config)
			if err != nil {
				return nil, fmt.Errorf("newCmdClient.newFabricClient error: %s", err)
			}
		default:
			return nil, fmt.Errorf("invalid chain name: %s", chainName)
		}

		// 链ID
		currChainName, err := currClient.GetChainID()
		if err != nil {
			return nil, fmt.Errorf("newCmdClient.currClient.GetChainID error: %s", err)
		}

		// 链ID必须包含在chainName里面
		if currChainName != chainName {
			return nil, fmt.Errorf("chainName must be equal to chainClient ID")
		}

		// 赋值
		ptClient.mmptClient[keyName] = currClient

		// 输出地址
		currAddress, err := currClient.GetAddress()
		if err != nil {
			utils.InfoError("client GetAddress", err)
		}
		utils.Info(keyName, "address: ", currAddress)
	}

	return ptClient, nil
}

// 获取一个新的客户端对象
// @param clientType
// @param mpUserConfigPath 每条链上对应的用户配置文件路径
func NewClient(clientType string, mpUserConfigPath map[string]interface{}) (*Client, error) {
	// 目前只有cmd客户端一种形式
	if clientType == "cmdClient" {
		return newCmdClient(mpUserConfigPath)
	}
	return nil, fmt.Errorf("invalid client type")
}
