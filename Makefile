CHAINMAKER_CRYPTOGEN_PATH = $(shell dirname ${PWD})/chainmaker-cryptogen
CHAINMAKER_GO_PATH = $(shell dirname ${PWD})/chainmaker-go
CURR_PATH = $(shell pwd)

chainmaker: environment-check chainmaker-go-build build_link execution_permission add_hosts ut lint
# chainmaker: build_link

environment-check:
	@./scripts/env_check.sh
	@./scripts/download_repo.sh

chainmaker-go-build: chainmaker-cryptogen-build
	@echo "build chainmaker-go..."
	@cd $(CHAINMAKER_GO_PATH) && git checkout v2.3.1
	@cd $(CHAINMAKER_GO_PATH)/tools && ln -fs ../../chainmaker-cryptogen/ .
	@cd $(CHAINMAKER_GO_PATH)/scripts && echo -e "1\nINFO\nYES\n\n" | ./prepare.sh 4 2 
	@cd $(CHAINMAKER_GO_PATH)/scripts && ./build_release.sh

chainmaker-cryptogen-build:
	@echo "build chainmaker-cryptogen..."
	@cd $(CHAINMAKER_CRYPTOGEN_PATH) && make
	@cp -r $(CHAINMAKER_CRYPTOGEN_PATH)/bin .

build_link:
	@echo "build link..."
	@cd $(CURR_PATH)/config_files/chain1 && if [ -d crypto-config ]; then echo "chain1 crypto-config exist"; else ln -s ../../../chainmaker-go/build/crypto-config .; fi
	@cd $(CURR_PATH)/config_files/chain2 && if [ -d crypto-config ]; then echo "chain2 crypto-conrig exist"; else ln -s ../../../chainmaker-go/build/crypto-config .; fi

execution_permission:
	@echo "execution permission..."
	@cd $(CURR_PATH)/bin && chmod 777 *
	@cd $(CURR_PATH)/scripts && chmod 777 *

add_hosts:
	@echo "add node name in fabric to hosts..."
	@cd $(CURR_PATH) && ./scripts/add_fabric_hosts.sh

ut:
	@echo "ut cover test..."
	@./ut_cover.sh

lint:
	@echo "static code analysis..."
	@cd chainclient && golangci-lint run ./...
	@cd client && golangci-lint run ./...
	@cd config && golangci-lint run ./...
	@cd contracts && golangci-lint run ./assets/... ./htlc/... ./lilac/...
	@cd deploy && golangci-lint run ./...
	@cd presentation && golangci-lint run ./...
	@cd projectinterface && golangci-lint run ./...
	@cd test && golangci-lint run ./...
	@cd utils && golangci-lint run ./...
	@golangci-lint run .

fileExist = $(shell if [ -d ${CHAINMAKER_GO_PATH}/build ]; then echo "exist"; else echo "noexist"; fi)
.PHONY:clean
clean:
ifeq ("$(fileExist)", "exist")
	@cd $(CHAINMAKER_GO_PATH)/scripts && ./cluster_quick_stop.sh
	@echo "y" | docker container prune
	@cd $(CHAINMAKER_GO_PATH) && sudo rm -rf $(CHAINMAKER_GO_PATH)/build/release/chainmaker*
endif
