# 命令指导

## 测试命令
### htlc测试命令
##### htlc chainmaker同构
* user1
```bash
getBalance chain1 user1
getBalance chain2 user1
									# # 链1用户2的地址
newHtlcProposal chain1 user1 27ff545972325a8da7007ea164a8caa348b6ff3b6e7a80249980e0912586498e 10 Preimage nil 600 chain2 1
getBalance chain1 user1
getBalance chain2 user1
```
* user2
```bash
getBalance chain1 user2
getBalance chain2 user2
									# 链2用户1的地址
newHtlcProposal chain2 user2 31ffd37bd5ca3f5c5a8f59a17bc2b35cb599d705 1 nil 2f2428e63d61518a8516ec19c68b5ee2140a5e73ab02220f1304f95a12597a77 300 chain1 10
getBalance chain1 user2
getBalance chain2 user2
```
##### htlc chainmaer 2 fabric 异构链
* user1
```bash
getBalance chain1 user1
getBalance fabric user1
									# 链1用户2的地址
newHtlcProposal chain1 user1 27ff545972325a8da7007ea164a8caa348b6ff3b6e7a80249980e0912586498e 10 Preimage nil 600 fabric 1
getBalance chain1 user1
getBalance fabric user1
```
* user2
```bash
getBalance chain1 user2
getBalance fabric user2
									# 链2用户1的地址
newHtlcProposal fabric user2 508037ae1b1d2eb13dd83baf652765e6974ce0e51557e2c85e48de2a6f7cf991 1 nil 2f2428e63d61518a8516ec19c68b5ee2140a5e73ab02220f1304f95a12597a77 300 chain1 10
getBalance chain1 user2
getBalance fabric user2
```

### lilac测试命令
* lilac测试命令, 加一个initial(leader), preimage由leader手动取款,其他人自动取
##### lilac 同构链
* user1
```bash
getBalance chain1 user1
getBalance chain2 user1
									# 链1用户2的地址
newLilacProposal chain1 user1 27ff545972325a8da7007ea164a8caa348b6ff3b6e7a80249980e0912586498e 10 Preimage,preimage nil 600 chain2 1
getBalance chain1 user1
getBalance chain2 user1
```
* user2
```bash
getBalance chain1 user2
getBalance chain2 user2
									# 链2用户1的地址
newLilacProposal chain2 user2 508037ae1b1d2eb13dd83baf652765e6974ce0e51557e2c85e48de2a6f7cf991 1 nil 2f2428e63d61518a8516ec19c68b5ee2140a5e73ab02220f1304f95a12597a77,107661134f21fc7c02223d50ab9eb3600bc3ffc3712423a1e47bb1f9a9dbf55f 300 chain1 10
getBalance chain1 user2
getBalance chain2 user2
```
##### lilac异构链 chamaker 2 fabric
* user1
```bash
getBalance chain1 user1
getBalance fabric user1
									# 链1用户2的地址
newLilacProposal chain1 user1 27ff545972325a8da7007ea164a8caa348b6ff3b6e7a80249980e0912586498e 10 Preimage,preimage nil 600 fabric 1
getBalance chain1 user1
getBalance fabric user1
```
* user2
```bash
getBalance chain1 user2
getBalance fabric user2
									# 链2用户1的地址
newLilacProposal fabric user2 508037ae1b1d2eb13dd83baf652765e6974ce0e51557e2c85e48de2a6f7cf991 1 nil 2f2428e63d61518a8516ec19c68b5ee2140a5e73ab02220f1304f95a12597a77,107661134f21fc7c02223d50ab9eb3600bc3ffc3712423a1e47bb1f9a9dbf55f 300 chain1 10
getBalance chain1 user2
getBalance fabric user2
```

### 超时自动退款命令
##### htlc
* user1
```bash
getBalance chain1 user1
									# 链1用户2的地址
newHtlcProposal chain1 user1 27ff545972325a8da7007ea164a8caa348b6ff3b6e7a80249980e0912586498e 10 Preimage nil 1 chain2 1
# 会进入自动退款程序
getBalance chain1 user1
```
##### lilac
* user1
```bash
getBalance chain1 user1
									# 链1用户2的地址
newLilacProposal chain1 user1 27ff545972325a8da7007ea164a8caa348b6ff3b6e7a80249980e0912586498e 10 Preimage,preimage nil 1 chain2 1
# 会进入自动退款程序
getBalance chain1 user1
```
### refund测试命令, 要提前关闭自动退款
##### htlc
* user1
```bash
getBalance chain1 user1
									# 链1用户2的地址
newHtlcProposal chain1 user1 27ff545972325a8da7007ea164a8caa348b6ff3b6e7a80249980e0912586498e 10 Preimage nil 5 chain2 1
getBalance chain1 user1
# 等待5秒
refund chain1 user1 htlc proposalID
getBalance chain1 user1
```
##### lilac
* user1
```bash
getBalance chain1 user1
									# 链1用户2的地址
newLilacProposal chain1 user1 27ff545972325a8da7007ea164a8caa348b6ff3b6e7a80249980e0912586498e 10 Preimage,preimage nil 5 chain2 1
getBalance chain1 user1
# 等待5秒
refund chain1 user1 lilac proposalID
getBalance chain1 user1
```
